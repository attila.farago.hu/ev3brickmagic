﻿using EV3ModelLib;
using EV3TreeVisLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest1
    {
        static UnitTest1()
        {
            // register converters
            EV3DecompilerLib.Recognize.PatternMatcher.RegisterConversion();
            EV3TreeVisLib.Ev3ZipTraverser.RegisterConversion();
            EV3ModelLib.Import.ImportEV3GB.RegisterConversion();
        }

        //[ClassInitialize()]
        //public static void ClassInit(TestContext context)
        //{
        //var ev3files = new List<string>() { "filename" };
        //var rootfolder = Path.Combine(context.TestRunDirectory, @"..\..\");
        //foreach (var file in Directory.GetFiles(rootfolder + @"TestFiles\"))
        //{
        //    if (!file.EndsWith(@".ev3")) continue;

        //    ev3files.Add(file);
        //}
        //File.WriteAllLines(rootfolder + "evfiles.csv", ev3files.ToArray());
        //}

        private TestContext testContextInstance;
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        //readonly List<string> ev3files = new List<string>() {
        //    "brickbutton.ev3",
        //    "colorsensor1.ev3",
        //    "demo_ev3treevis.ev3",
        //    "DINOR3X1.ev3"};
        string TEST_FILES_ROOT = @"..\..\TestFiles"; //EV3s_mindsensors\

        //[DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\ev3files.csv", "ev3files#csv",
        //    DataAccessMethod.Sequential),
        //    DeploymentItem("ev3files.csv")]
        [TestMethod]
        public void TestFilesRoot()
        {
            Test1InDirectory(TEST_FILES_ROOT, false, false);
        }
        [TestMethod]
        public void TestFilesRootAndRecursive()
        {
            Test1InDirectory(TEST_FILES_ROOT, true, false);
        }

        [TestMethod]
        public void TestFilesRootParallel()
        {
            Test1InDirectory(TEST_FILES_ROOT, false, true);
        }

        /// <summary>
        /// Test Files in a directory
        /// For now check only if json serialization and deserialization can be done without exception
        /// </summary>
        /// <param name="path"></param>
        /// <param name="recursive"></param>
        private void Test1InDirectory(string path, bool recursive, bool parallel)
        {
            Ev3ZipTraverser.Options.Value.HandleAndReportErrors = false;
            Node.PrintOptions.TargetColorIndex = PrintOptionsClass.TargetColorEnum.None;
            Node.PrintOptions.ApplyEV3GBasicFormat();

            TestContext.WriteLine("TEST: Check file conversion and serialization can be done without exception\n");
            TestContext.WriteLine($"Testing directory: {path}\n");
            var ev3files = Directory.GetFiles(path).Where(fn => Path.GetExtension(fn).In(".ev3", ".ev3m", ".rbf", ".ev3gb")).ToList();

            if (!parallel)
            {
                foreach (var ev3file in ev3files)
                    TestOneFile(ev3file);
            }
            else
            {
                List<Task> tasks = new List<Task>();
                foreach (var ev3file in ev3files)
                    tasks.Add(Task.Run(() => { TestOneFile(ev3file); }));
                Task.WaitAll(tasks.ToArray());
            }

            // recurse directories
            if (recursive)
                Directory.GetDirectories(path).ToList().ForEach(dir => Test1InDirectory(dir, recursive, parallel));
        }

        /// <summary>
        /// Test one single file
        /// </summary>
        /// <param name="ev3file"></param>
        private void TestOneFile(string ev3file)
        {
            //var filename = TestContext.DataRow["filename"].ToString();
            TestContext.WriteLine($"Testing {Path.GetFileName(ev3file)}");
            //Console.WriteLine($"Testing {Path.GetFileName(ev3file)}");

            var filename = ev3file; // rootfolder + ev3file;

            var dt = DateTime.Now;
            string str1;
            string str2;
            string[] programs;
            using (var fs = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                // convert to nodes
                ProjectResultData projresult = ProjectConversion.Convert(fs, filename);
                programs = projresult.Nodes.Keys.ToArray();

                // print to string
                str1 = projresult.Nodes.GetFormattedOutput();

                // convert back from EV3T
                EV3ModelLib.Import.ImportEV3GB.Convert(str1, filename);
                str2 = projresult.Nodes.GetFormattedOutput();
            }

            Assert.AreEqual(str1, str2);

            // test passed
            var elapsed = (DateTime.Now - dt).TotalMilliseconds;
            TestContext.WriteLine($"Tests passed in {elapsed} ms for files: {Path.GetFileName(ev3file)} | " + string.Join(", ", programs) + "\n");
        }
    }
}
