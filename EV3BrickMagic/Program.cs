﻿using EV3MonoBrickLib;
using MonoBrick.EV3;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static EV3BrickMagic.CommandOptions;

namespace EV3BrickMagic
{
    /// <summary>
    /// Main program class
    /// </summary>
    class Program
    {
        [STAThread]
        static int Main(string[] args)
        {
            List<CommandOptions> lopts = new List<CommandOptions>();

            string[] args1 = args;
            while (args1.Length > 0)
            {
                string[] args2 = args1.TakeWhile(elem => elem != "@@").ToArray();
                args1 = args1.Skip(args2.Length + 1).ToArray();
                CommandOptions opts = new CommandOptions(args2);
                lopts.Add(opts);
            }

            return new MagicMain().StartTheMagic(lopts);
        }
    }
}
