﻿using EV3BrickMagic.Command;
using EV3MonoBrickLib;
using EV3MonoBrickLib.Helper;
using MonoBrick.EV3;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using static EV3BrickMagic.CommandOptions;

namespace EV3BrickMagic
{
    /// <summary>
    /// Main program class for EV3 brick communication command line tool
    /// </summary>
    internal class MagicMain
    {
        /// <summary>
        /// Execute EV3BrickMagic main loop based on options with error handling
        /// </summary>
        /// <param name="opts"></param>
        internal int StartTheMagic(List<CommandOptions> lopts)
        {
            //-- if started from explorer (& no arguments) start the interactive window - otherwise (no args) display help
            if (lopts.Count == 0)
            {
                if (!StartedFromWindowsExplorer())
                    lopts.Add(new CommandOptions(Commands.Help));
                else
                    lopts.Add(new CommandOptions(Commands.InteractiveWindow));
            }

            // execute the brick logic
            int retval = StartTheMagicSub(lopts);

            // check if process was started from windows explorer directly
            // if was started from explorer (not cmd prompt), not in quiet mode, not in interactive win form
            // -> then add a wait at the end, if not quiet mode explicitely
            if (retval != 0 || (lopts.Count > 0 && !lopts.First().QuietMode && lopts.First().Command != Commands.InteractiveWindow))
            {
                if (StartedFromWindowsExplorer())
                {
                    // https://stackoverflow.com/questions/36272223/how-to-determine-whether-an-console-application-started-from-within-a-console-wi
                    // http://pinvoke.net/default.aspx/kernel32.GetConsoleProcessList
                    Console.WriteLine("Press any key to continue . . .");
                    Console.ReadKey();
                }
            }
            return retval;
        }

        /// <summary>
        /// Check if app was started from windows explorer
        /// </summary>
        /// <returns></returns>
        private static bool StartedFromWindowsExplorer()
        {
            const int MAXPROCESSNUM = 8;
            uint[] procIDs = new uint[MAXPROCESSNUM];
            var numOfProcesses = NativeMethods.GetConsoleProcessList(procIDs, MAXPROCESSNUM);
            return (numOfProcesses <= 1);
        }

        /// <summary>
        /// Execute EV3BrickMagic main loop based on options
        /// </summary>
        /// <param name="opts"></param>
        internal int StartTheMagicSub(List<CommandOptions> lopts)
        {
            //-- select the connection starter
            CommandOptions optsConn = lopts.FirstOrDefault(opts => opts.Command >= Commands._CONNECTION_STARTERS);

            //-- print header before execution
            if (optsConn != null && optsConn.Command != Commands.InteractiveWindow && !optsConn.QuietMode)
                ConsoleExt.WriteLine(GetHeaderLine(), ConsoleColor.DarkGray);

            try
            {
                //-- Connect to the selected port
                try
                {
                    string sUSBSerial = null;
                    if (optsConn != null)
                    {
                        if (optsConn.Port != null && !Regex.IsMatch(optsConn.Port, @"USB|COM\d+"))
                        {
                            // check if port belongs to an associated COM sserial port 0 if yes, use it
                            var btport = CommandListPorts._GetBTPortsInternal().FirstOrDefault(elem => elem.DeviceName == optsConn.Port);
                            if (btport.Port != null)
                            {
                                optsConn.Port = btport.Port;
                                sUSBSerial = null;
                            }
                            else if (Regex.IsMatch(optsConn.Port, "[0-9a-fA-F]{12}"))
                            {
                                //  check usb serials as well - has a 12 char hexa format
                                sUSBSerial = optsConn.Port;
                                optsConn.Port = "USB";
                            }
                        }

                        if (optsConn.DelayMs > 0) _WaitMillisec(optsConn.DelayMs);

                        EV3.Connect(optsConn.Port, sUSBSerial);
                    }
                }
                catch (Exception ex)
                {
                    if (optsConn.Command != Commands.InteractiveWindow)
                    {
                        Console.WriteLine($"Error opening connection to {optsConn.Port} => {ex.Message}");
                        //Console.WriteLine(ex.StackTrace);
                        return 1;
                    }
                }

                //-- execute the selected command(s)
                int retval = 0;
                foreach (var opts in lopts)
                {
                    if (opts != optsConn && opts.DelayMs > 0) _WaitMillisec(opts.DelayMs);

                    retval = StartTheMagicExecute(opts);
                }
                return retval;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error occurred: {(ex.InnerException != null ? ex.InnerException.Message : ex.Message)}");
                if (System.Diagnostics.Debugger.IsAttached)
                {
                    Console.WriteLine(ex.StackTrace);
                    System.Diagnostics.Debugger.Break();
                }
                else
                {
                    var sFirstMyCode = ex.StackTrace.Split('\n').FirstOrDefault(elem => elem.Contains("EV3"));
                    if (sFirstMyCode != null) Console.WriteLine(sFirstMyCode);
                }
                //ex.StackTrace

                //-- indicate failure
                if (optsConn.ConfirmBeep)
                    _MakeBeeps(new ushort[] { 500, 450, 400 });

                return 05;
            }
            finally
            {
                if (EV3.IsConnected)
                {
                    EV3.Disconnect();
                    Thread.Sleep(300);
                }
            }
        }

        /// <summary>
        /// Execute the selected command
        /// </summary>
        /// <param name="opts"></param>
        /// <returns></returns>
        private static int StartTheMagicExecute(CommandOptions opts)
        {
            //-- indicate start
            if (opts.ConfirmBeep)
                _MakeBeeps(new ushort[] { 400, 450, 500 });

            //var fCommandMethod = this.GetType()
            //       .GetMethods(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy)
            //      .FirstOrDefault(m =>
            //        m.GetCustomAttributes(typeof(TriggeredByCommandAttribute), false)
            //        .Any(elem => (elem as TriggeredByCommandAttribute).Trigger == opts.Command)
            //      );
            //if (fCommandMethod != null) fCommandMethod.Invoke(this, new[] { opts });
            var cmdClass = Assembly.GetExecutingAssembly().GetTypes()
                .Where(t => String.Equals(t.Namespace, "EV3BrickMagic.Command", StringComparison.Ordinal))
                .FirstOrDefault(t => t.GetCustomAttributes(typeof(TriggeredByCommandAttribute), false)
                    .Any(elem => (elem as TriggeredByCommandAttribute).Trigger == opts.Command));

            int retval = 0;
            if (cmdClass != null) retval = ((ICommand)Activator.CreateInstance(cmdClass)).Execute(opts);
            else new Command.CommandHelp().Execute(opts);

            //-- indicate success
            if (opts.ConfirmBeep)
            {
                Thread.Sleep(100);
                _MakeBeeps(new ushort[] { 500, 550, 600 });
            }

            return retval;
        }

        /// <summary>
        /// Make confirmation beeps
        /// </summary>
        /// <param name="freqs"></param>
        private static void _MakeBeeps(ushort[] freqs)
        {
            if (EV3.Brick.Connection.IsConnected)
            {
                foreach (var freq in freqs) { EV3.Brick.PlayTone(5, freq, 25); Thread.Sleep(25); }
                Thread.Sleep(25);
            }
        }

        /// <summary>
        /// Delay before starting a command
        /// </summary>
        /// <param name="optsConn"></param>
        private static void _WaitMillisec(int millisecondsTimeout)
        {
            Console.WriteLine($"... Waiting {millisecondsTimeout / 1000.0} seconds before executing the command.");
            Thread.Sleep(millisecondsTimeout);
        }
    }
}
