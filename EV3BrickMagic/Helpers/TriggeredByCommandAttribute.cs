﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV3BrickMagic
{
    [AttributeUsage(AttributeTargets.Class)]
    public class TriggeredByCommandAttribute : Attribute
    {
        public TriggeredByCommandAttribute(Commands trigger)
        {
            this.Trigger = trigger;
        }
        public Commands Trigger { get; set; }
    }
}
