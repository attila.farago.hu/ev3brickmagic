﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EV3BrickMagic
{

    public static class StringExtensions
    {
        /// <summary>
        /// Compares the string against a given pattern.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <param name="pattern">The pattern to match, where "*" means any sequence of characters, and "?" means any single character.</param>
        /// <returns><c>true</c> if the string matches the given pattern; otherwise <c>false</c>.</returns>
        public static bool Like(this string str, string pattern)
        {
            return new Regex(
                "^" + Regex.Escape(pattern).Replace(@"\*", ".*").Replace(@"\?", ".") + "$",
                RegexOptions.IgnoreCase | RegexOptions.Singleline
            ).IsMatch(str);
        }
        public static readonly char[] WILDCHARS = { '*', '?' };

        public static bool ContainsAny(this string str, char[] anyOf)
        {
            return str.IndexOfAny(anyOf) >= 0;
        }

        public static string CapitalizeFirstLetter(this string str)
        {
            return str.Length > 0 ? str.Substring(0, 1).ToUpper() +
                (str.Length > 1 ? str.Substring(1) : null)
                : null;
        }
    }
}
