﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV3BrickMagic
{
    internal static class ColorConverter
    {
        static readonly Dictionary<ConsoleColor, Color> ConsoleColorsRGB = new Dictionary<ConsoleColor, Color>
        {
            [ConsoleColor.Black] = Color.FromArgb(0, 0, 0),
            [ConsoleColor.DarkBlue] = Color.FromArgb(0, 55, 218),
            [ConsoleColor.DarkGreen] = Color.FromArgb(19, 161, 14),
            [ConsoleColor.DarkCyan] = Color.FromArgb(58, 150, 221),
            [ConsoleColor.DarkRed] = Color.FromArgb(197, 15, 31),
            [ConsoleColor.DarkMagenta] = Color.FromArgb(136, 23, 152),
            [ConsoleColor.DarkYellow] = Color.FromArgb(193, 156, 0),
            [ConsoleColor.Gray] = Color.FromArgb(204, 204, 204),
            [ConsoleColor.DarkGray] = Color.FromArgb(118, 118, 118),
            [ConsoleColor.Blue] = Color.FromArgb(59, 120, 255),
            [ConsoleColor.Green] = Color.FromArgb(22, 198, 12),
            [ConsoleColor.Cyan] = Color.FromArgb(97, 214, 214),
            [ConsoleColor.Red] = Color.FromArgb(231, 72, 86),
            [ConsoleColor.Magenta] = Color.FromArgb(180, 0, 158),
            [ConsoleColor.Yellow] = Color.FromArgb(249, 241, 165),
            [ConsoleColor.White] = Color.FromArgb(242, 242, 242),
        };

        /// <summary>
        /// Converts the specified <see cref="Color"/> to it's nearest <see cref="Color"/> equivalent.
        /// </summary>
        /// <remarks>Original code taken from ColorExtensions package - added Hue bias</remarks>
        public static ConsoleColor ToNearestConsoleColor(this Color color)
        {
            ConsoleColor closestColor = 0;
            double delta = double.MaxValue;

            foreach (ConsoleColor consoleColor in Enum.GetValues(typeof(ConsoleColor)))
            {
                //string consoleColorName = Enum.GetName(typeof(ConsoleColor), consoleColor);
                //consoleColorName = string.Equals(consoleColorName, nameof(ConsoleColor.DarkYellow), StringComparison.Ordinal) ?
                //    nameof(Color.Orange) : consoleColorName;
                //Color rgbColor = Color.FromName(consoleColorName);
                Color rgbColor = ConsoleColorsRGB[consoleColor];
                double sum =
                    Math.Abs(rgbColor.GetBrightness() - color.GetBrightness()) * 100.0
                + Math.Abs(rgbColor.GetSaturation() - color.GetSaturation()) * 100.0;
                if (color.GetBrightness() > 0.1 && color.GetBrightness() < 0.5)
                    sum += Math.Abs(rgbColor.GetHue() - color.GetHue()) * 100.0 / 360.0; // 0.0 through 360.0

                if (sum == 0.0)
                {
                    return consoleColor;
                }
                else if (sum < delta)
                {
                    delta = sum;
                    closestColor = consoleColor;
                }
            }

            return closestColor;
        }

        // weighed distance using hue, saturation and brightness
        public static Color ToClosestKnownColor(this Color color)
        {
            var knownColorList = Enum.GetValues(typeof(KnownColor)).Cast<KnownColor>()
                .Select(elem => Color.FromKnownColor(elem))
                .Where(elem => !elem.IsSystemColor)
                //.SkipWhile(elem => elem != KnownColor.AliceBlue)
                //.TakeWhile(elem => elem != KnownColor.ButtonFace)
                //.Select(elem => Color.FromKnownColor(elem))
                .ToArray();
            var colors = knownColorList;

            float hue1 = color.GetHue();
            var num1 = ColorNum(color);

            var diffs = colors.Select(n => Math.Abs(ColorNum(n) - num1) +
                                           getHueDistance(n.GetHue(), hue1));
            var diffMin = diffs.Min(x => x);
            int idxmin = diffs.ToList().FindIndex(n => n == diffMin);
            return colors[idxmin];
        }

        // color brightness as perceived:
        static float getBrightness(Color c)
        { return (c.R * 0.299f + c.G * 0.587f + c.B * 0.114f) / 256f; }

        // distance between two hues:
        static float getHueDistance(float hue1, float hue2)
        {
            float d = Math.Abs(hue1 - hue2); return d > 180 ? 360 - d : d;
        }

        //  weighed only by saturation and brightness (from my trackbars)
        static float ColorNum(Color c)
        {
            const float factorHue = 0.5F;
            const float factorSat = 0.5F;
            const float factorBri = 0.5F;
            return
                c.GetSaturation() * factorSat +
                        getBrightness(c) * factorBri;
        }

        // distance in RGB space
        static int ColorDiff(Color c1, Color c2)
        {
            return (int)Math.Sqrt((c1.R - c2.R) * (c1.R - c2.R)
                                   + (c1.G - c2.G) * (c1.G - c2.G)
                                   + (c1.B - c2.B) * (c1.B - c2.B));
        }
    }
}
