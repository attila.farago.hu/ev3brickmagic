﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using EV3ModelLib;

namespace EV3BrickMagic
{
    /// <summary>
    /// Available commands
    /// </summary>
    public enum Commands
    {
        Help, ListPorts, ConvertFile,
        _CONNECTION_STARTERS,
        Info, Copy,
        StartProgram, StopProgram,
        ListFiles, CreateDirectory,
        SendMail, Sound, InstallAppFromZip, 
        InteractiveWindow
    };

    /// <summary>
    /// Command options helper class
    /// </summary>
    public class CommandOptions
    {
        internal const string TXT_USAGE = @"Usage: EV3BrickMagic [-p=<port>] command [parameters...] [-options...]";
        internal const string TXT_OPTIONS = @"
Commands:
    info [update]               Brick, sensor and motor information.
    win                         Interactive window interface
    copy <local> <brick>        Copy files between brick and local filesystem.
    copy <brick> <local> [-d] [-q] Options to decode, overwrite without prompt.
                                Brick path may contain wildchars and must precede with 'brick:'
                                Use -- for console input/output. 
    list [<brickpath>] [-r]     List all files and directories under path.
    start <brickfile> [-w]      Start brick project program.
    stop                        Stop all brick programs.
    ports                       Get information on serial bluetooth ports.
    mailbox <name> <message>    Send message to connected EV3 mailbox.
    beep <notes>...             Plays a sequence of sound notes. e.g. CECEG-G- CECEG-G- C6HAGF-A- GFEDC-C-
                                <note> = C|D|E|F|G|A|H[#][<octave>][!<volume>][length]
                                <length> = [o|d|q|e][-.]*    whole, half, quarter, eighth, add 1x, add 0.5x
                                <settings> = [!<tempo>bpm][!!<volume>]
    display|convert <file>      Display or Convert programs, graphics files, sound files.
         [-f=json|yaml|html|txt|ev3gb]   conversion: rgf<->bmp and ev3->rbf, rsf->wav; optional format setting
         [-d]                           display .ev3, .ev3m, .rbf, .rgf, .bmp contents.
    mkdir <brickpath>           Create a directory on the brick
    install <zipfile>           Create a directory and install and app on the brick

Options:
    -p=<port>|--port=<port>     Communication port (USB, COM10) or a USB serial ID (00167352ed78) or a 
                                Bluetooth Bot name (EV3).
    -confirmbeep                Confirm command success with beep.
    -q|--quiet                  Quiet mode, omits printing or overwrite confirmation.
    -delay=<seconds>            Delay executing the command. Recommended 3 seconds for Bluetooth connections.

Commands can be chained using @@ to use a single connection and command line such as 
    list Project/ -p=COM4 -delay=3 @@ copy test1.rtf brick:Project/ -q @@ start Project/Program @@ stop -delay=0.5

Serial port for an EV3 brick can be created after pairing with PC in Win-10. Open Control Panel > 
    Hardware and Sound > Devices and Printers and select item with the brick name.
    Right click on item, select Properties > Services tab and check Bluetooth Services | Serial port (SPP).

Kudos and Credits
    The author of this tool and all the magical unicorns owe credits and thanks to the creators of 
    monobrick.dk Anders Søborg & Lars Jeppesen and David Lechner, creator of lms-hacker-tools.";

        public Commands Command { get; internal set; } = Commands.Help;
        public string Port { get; internal set; } = "USB";
        public bool ConfirmBeep { get; internal set; }
        public bool QuietMode { get; internal set; }
        public int DelayMs { get; internal set; }
        public MailboxSubOptions MailboxSub { get; internal set; } = new MailboxSubOptions();
        public SoundSubOptions SoundSub { get; internal set; } = new SoundSubOptions();
        public FileSubOptions FileSub { get; internal set; } = new FileSubOptions();
        public DecodeSubOptions DecodeSub { get; internal set; } = new DecodeSubOptions();
        public InfoSubOptions InfoSub { get; internal set; } = new InfoSubOptions();
        public ProgramSubOptions ProgramSub { get; internal set; } = new ProgramSubOptions();
        public KeepAliveSubOptions KeepAliveSub { get; internal set; } = new KeepAliveSubOptions();

        #region SubOptions
        public class MailboxSubOptions
        {
            public string Name { get; set; }
            public string Message { get; set; }
        }
        public class SoundSubOptions
        {
            public string SoundSequence { get; set; }
            public bool PianoMode { get; set; }
        }
        public class FileSubOptions
        {
            public string Source { get; internal set; }
            public string Destination { get; internal set; }
            public bool QuietOverwrite { get; internal set; }
            public bool DecodeEnabled { get; internal set; }
            public bool RecursiveList { get; internal set; }
            public bool NeverOverwrite { get; internal set; }
            public bool OutputToStdOut { get; internal set; }
        }
        public class DecodeSubOptions
        {
            public string DecodeTargetExt { get; internal set; }
            public int DecodeDecompileDebugLevel { get; internal set; } = 0;
            public int DecodeInterpretDebugLevel { get; internal set; } = 0;
            public int DecodeInterpretPrintDebugLevel { get; internal set; } = 0;
            public bool DisplayContents { get; internal set; }
            public bool EnableConversion { get; internal set; }
        }
        public class InfoSubOptions
        {
            public bool ContinousUpdate { get; internal set; }
        }
        public class ProgramSubOptions
        {
            public bool WaitForFinish { get; internal set; }
            public string Brickfile { get; internal set; }
            public bool UpdateFavoritesCache { get; internal set; } = true;
            public bool DebugExperimental { get; internal set; }
        }
        public class KeepAliveSubOptions
        {
            public int TimeOut { get; internal set; }
        }
        #endregion SubOptions

        /// <summary>
        /// Get header line
        /// </summary>
        /// <returns></returns>
        public static string GetHeaderLine()
        {
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location);
            string headerline = @"EV3 Magic tool for connecting the EV3 Brick with the magical unicorns. " + $"{fvi.FileVersion}, {fvi.ProductVersion}";
            return headerline;
        }

        /// <summary>
        /// Print usage
        /// </summary>
        static public void PrintHelp()
        {
            ConsoleExt.WriteLine(GetHeaderLine(), ConsoleColor.DarkGray);
            ConsoleExt.WriteLine(TXT_USAGE, ConsoleColor.White);
            Console.WriteLine(TXT_OPTIONS);
        }

        /// <summary>
        /// Create a CommandOptions instance
        /// </summary>
        public CommandOptions(Commands command)
        {
            this.Command = command;
        }

        /// <summary>
        /// Create a CommandOptions instance
        /// </summary>
        /// <param name="args"></param>
        public CommandOptions(string[] args1)
        {
            if (args1 == null || args1.Length == 0)
            {
                // no args given, display help
                this.Command = Commands.Help;
                return;
            }

            //-------------------------------------
            // OPTIONS
            Dictionary<string[], Action<string>> actionsForOptions = new Dictionary<string[], Action<string>>()
            {
                [new[] { "-q", "--quiet" }] = (_) => { this.QuietMode = true; this.FileSub.QuietOverwrite = true; },
                [new[] { "-confirmbeep" }] = (_) => { this.ConfirmBeep = true; },
                [new[] { "-r" }] = (_) => { this.FileSub.RecursiveList = true; },
                [new[] { "-delay=" }] = (arg) =>
                {
                    int value = (int)(double.Parse(arg) * 1000);
                    this.DelayMs = value;
                },
                [new[] { "-p=", "--port=" }] = (arg) => { this.Port = arg; },
                // developer mode only
                [new[] { "-dd=", "--decodedebug=" }] = (arg) =>
                {
                    int value = int.Parse(arg);
                    this.DecodeSub.DecodeDecompileDebugLevel = value % 1000 / 100;
                    this.DecodeSub.DecodeInterpretDebugLevel = value % 100 / 10;
                    this.DecodeSub.DecodeInterpretPrintDebugLevel = value % 10;
                },
                [new[] { "-f=" }] = (target_ext) => { this.DecodeSub.DecodeTargetExt = "." + target_ext; },
            };
            List<string> args2 = new List<string>();
            foreach (var arg in args1)
            {
                string argToSearch = arg;
                string argParam = null;
                if (arg.Contains("="))
                {
                    argToSearch = arg.Substring(0, arg.IndexOf("=") + 1);
                    argParam = arg.Substring(argToSearch.Length);
                }

                var foundOption = actionsForOptions.FirstOrDefault(elem => elem.Key.Contains(argToSearch));
                if (foundOption.Key != null)
                {
                    foundOption.Value.Invoke(argParam);
                    continue;
                }
                else
                {
                    args2.Add(arg);
                }
            }

            //-------------------------------------
            // COMMANDS
            Dictionary<string[], Action> actionsForCommands = new Dictionary<string[], Action>()
            {
                [new[] { "help" }] = () =>
                {
                    this.Command = Commands.Help;
                },
                [new[] { "win" }] = () =>
                {
                    this.Command = Commands.InteractiveWindow;
                },
                [new[] { "info" }] = () =>
                  {
                      this.Command = Commands.Info;

                      if (args2.Count > 1 && args2[1].In("update", "u", "-u"))
                          this.InfoSub.ContinousUpdate = true;
                  },
                [new[] { "copy", "upload", "download" }] = () =>
                {
                    if (args2.Count > 2 && args2.Skip(1).Contains("-d")) { args2.Remove("-d"); this.FileSub.DecodeEnabled = true; }
                    if (args2.Count - 1 < 2) return;

                    this.FileSub.Source = args2[1];
                    this.FileSub.Destination = args2[2];

                    this.Command = Commands.Copy;
                },
                [new[] { "install", "installapp", "installzip" }] = () =>
                {
                    if (args2.Count - 1 < 1) return;

                    this.FileSub.Source = args2[1];
                    this.Command = Commands.InstallAppFromZip;
                },
                [new[] { "display", "decode", "decodefile" }] = () =>
                {
                    if (args2.Count < 1 + 1) return;
                    this.FileSub.Source = args2[1];
                    this.FileSub.DecodeEnabled = true;
                    this.DecodeSub.EnableConversion = false;
                    this.Command = Commands.ConvertFile;

                    if (args2.Count >= 3 && args2.Last() == EV3BrickMagic.Command.Common.STDINOUT_PATH) this.FileSub.OutputToStdOut = true;

                    this.DecodeSub.DisplayContents = true;
                },
                [new[] { "convert", "convertfile" }] = () =>
                {
                    if (args2.Skip(1).Contains("-d")) { args2.Remove("-d"); this.DecodeSub.DisplayContents = true; }
                    //if (args2.Skip(1).Any(elem => elem.StartsWith("-d=")))
                    //{
                    //    var strarg = args2.Skip(1).ToList().Find(elem => elem.StartsWith("-d="));
                    //    args2.Remove(strarg);
                    //    this.DecodeSub.DisplayContents = true;
                    //    this.DecodeSub.DisplayFormat = strarg.Substring(3)
                    //}
                    if (args2.Count < 1 + 1) return;
                    this.FileSub.Source = args2[1];
                    this.FileSub.DecodeEnabled = true;
                    this.Command = Commands.ConvertFile;

                    if (args2.Count >= 3 && args2.Last() == EV3BrickMagic.Command.Common.STDINOUT_PATH) this.FileSub.OutputToStdOut = true;

                    this.DecodeSub.EnableConversion = true;
                },
                [new[] { "beep", "bip", "music", "sound", "play" }] = () =>
                {
                    if (args2.Count < 1 + 1) return;

                    this.Command = Commands.Sound;
                    if (args2[1] == "piano") this.SoundSub.PianoMode = true;
                    else this.SoundSub.SoundSequence = string.Join(" ", args2.Skip(1));
                },
                [new[] { "start", "run" }] = () =>
                {
                    if (args2.Count > 2 && args2.Skip(1).Contains("-w")) { args2.Remove("-w"); this.ProgramSub.WaitForFinish = true; }
                    if (args2.Count > 2 && args2.Skip(1).Contains("-d")) { args2.Remove("-d"); this.ProgramSub.DebugExperimental = true; }
                    if (args2.Count > 2 && args2.Skip(1).Contains("-nf")) { args2.Remove("-nf"); this.ProgramSub.UpdateFavoritesCache = false; }
                    if (args2.Count < 1 + 1) return;

                    this.ProgramSub.Brickfile = args2[1];
                    if (Path.GetExtension(this.ProgramSub.Brickfile) == string.Empty) { this.ProgramSub.Brickfile += MonoBrick.EV3.BrickFile.EXT_RBF_PROGRAM; }
                    this.Command = Commands.StartProgram;
                },
                [new[] { "stop" }] = () =>
                {
                    this.Command = Commands.StopProgram;
                },
                [new[] { "list", "dir", "ls" }] = () =>
                {
                    if (args2.Count >= 1 + 1) this.FileSub.Source = args2[1];
                    this.Command = Commands.ListFiles;
                },
                [new[] { "ports", "listports" }] = () =>
                {
                    this.Command = Commands.ListPorts;
                },
                [new[] { "mailbox", "send", "message" }] = () =>
                {
                    if (args2.Count < 1 + 2) return;

                    this.Command = Commands.SendMail;
                    this.MailboxSub.Name = args2[1];
                    this.MailboxSub.Message = args2[2];
                },
                [new[] { "mkdir", "createdir", "md" }] = () =>
                {
                    if (args2.Count >= 1 + 1) this.FileSub.Destination = args2[1];
                    this.Command = Commands.CreateDirectory;
                },
            };
            if (args2.Count < 1) return;

            var foundCommand = actionsForCommands.FirstOrDefault(elem => elem.Key.Contains(args2[0]));
            if (foundCommand.Key == null && File.Exists(args2[0]))
            {
                // if no command is provided, but a file - display it
                args2.Insert(0, "display");
                foundCommand = actionsForCommands.FirstOrDefault(elem => elem.Key.Contains(args2[0]));
            }

            if (foundCommand.Key != null)
            {
                foundCommand.Value.Invoke();
                return;
            }

            //-------------------------------------
            // COMMANDs closes matching
            {
                var alloptions = actionsForCommands.Aggregate(new List<string>(), (list, elem) => { list.AddRange(elem.Key); return list; });
                string closestoption = ClosestWord(args2[0], alloptions.ToArray());
                Console.WriteLine($"Option {args2[0]} not found. Perhaps you meant {closestoption}.");
            }
        }

        /// <summary>
        /// Find closest word
        /// </summary>
        /// <param name="word"></param>
        /// <param name="terms"></param>
        /// <returns></returns>
        private static string ClosestWord(string word, string[] terms)
        {
            string term = word.ToLower();
            List<string> list = terms.ToList();
            if (list.Contains(term))
                return list.Find(t => t.ToLower() == term);
            else
            {
                int[] counter = new int[terms.Length];
                for (int i = 0; i < terms.Length; i++)
                {
                    for (int x = 0; x < Math.Min(term.Length, terms[i].Length); x++)
                    {
                        int difference = Math.Abs(term[x] - terms[i][x]);
                        counter[i] += difference;
                    }
                }

                int min = counter.Min();
                int index = counter.ToList().FindIndex(t => t == min);
                return terms[index];
            }
        }
    }
}
