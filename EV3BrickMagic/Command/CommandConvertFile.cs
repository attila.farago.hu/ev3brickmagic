﻿using EV3DecompilerLib.BuildEV3;
using EV3ModelLib;
using EV3ModelLib.Helper;
using EV3MonoBrickLib;
using EV3MonoBrickLib.Helper;
using EV3TreeVisLib;
using MonoBrick.EV3;
using OutputColorizer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;

namespace EV3BrickMagic.Command
{
    [TriggeredByCommand(Commands.ConvertFile)]
    class CommandConvertFile : ICommand
    {
        public int Execute(CommandOptions opts)
        {
            /// Convert files
            var path1 = opts.FileSub.Source.Trim('"');
            if (Directory.Exists(path1)) path1 += @"\*.*";
            var path = Path.GetDirectoryName(path1); if (string.IsNullOrEmpty(path)) path = ".";
            var filename1 = Path.GetFileName(path1);

            var matchedfiles = Directory.GetFiles(path, filename1);
            var convertedFiles = new List<string>();
            foreach (var filename in matchedfiles)
            {
                // skip files that will be created and already exists to avoid back and forth conversion in one round 
                if (convertedFiles.Contains(filename))
                {
                    Console.WriteLine($"Skipping re-conversion of an already convered source '{filename}'.");
                    continue;
                }

                // perform conversion
                var newfilename = _DecodeDataFileInternal(filename, matchedfiles.Length > 1, opts);
                if (newfilename != null) convertedFiles.Add(newfilename);
            }

            return 0;
        }

        /// <summary>
        /// internal processing of one file for conversion
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="opts"></param>
        /// <returns></returns>
        internal static string _DecodeDataFileInternal(string filename, bool hasMultipleMatch, CommandOptions opts)
        {
            using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                return _DecodeDataFileInternal(fs, filename, hasMultipleMatch, opts);
            }
        }

        private const string CONST_EXT_BMP = ".bmp";
        private const string CONST_EXT_PNG = ".png";
        private const string CONST_EXT_WAV = ".wav";
        private const string CONST_EXT_JPG = ".jpg";
        private const string CONST_EXT_EV3 = ".ev3";
        private const string CONST_EXT_EV3M = ".ev3m";
        private const string CONST_EXT_EV3GB = ".ev3gb";
        private const string CONST_EXT_RBF = BrickFile.EXT_RBF_PROGRAM;
        private const string CONST_EXT_RGF = BrickFile.EXT_RGF_GRAPHICS;
        private const string CONST_EXT_RSF = BrickFile.EXT_RSF_SOUND;
        private const string CONST_EXT_txt = ".txt";
        private const string CONST_EXT_html = ".html";
        private const string CONST_EXT_yaml = ".yaml";
        private const string CONST_EXT_json = ".json";

        /// <summary>
        /// Internal processing of one file for conversion
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="filename2"></param>
        /// <param name="opts"></param>
        /// <returns></returns>
        internal static string _DecodeDataFileInternal(Stream stream, string filename2, bool hasMultipleMatch, CommandOptions opts)
        {
            string filenamenew = null;
            switch (Path.GetExtension(filename2).ToLower())
            {
                case CONST_EXT_RGF:
                    {
                        filenamenew = Path.ChangeExtension(filename2, CONST_EXT_BMP);
                        RGFHelper.RGFBitmap rgfbitmap = new RGFHelper.RGFBitmap(stream);

                        //-- display the file
                        if (opts.DecodeSub.DisplayContents) DisplayRGFFileToConsole(rgfbitmap);

                        //-- convert if enabled
                        if (opts.DecodeSub.EnableConversion)
                        {
                            if (!CheckAndPromptFileOverwrite(opts.QuietMode, filenamenew, hasMultipleMatch, opts.FileSub)) return null;
                            if (!opts.QuietMode) Console.WriteLine($"Creating bitmap file: {filenamenew}");
                            var bmp = rgfbitmap.ConvertToBitmap();
                            bmp.Save(filenamenew);
                        }
                    }
                    break;

                case CONST_EXT_RSF:
                    {
                        filenamenew = Path.ChangeExtension(filename2, CONST_EXT_WAV);
                        RSFSound rsfsound = new RSFSound(stream);

                        //-- display the file
                        if (opts.DecodeSub.DisplayContents)
                        {
                            using (var ms = new MemoryStream(rsfsound.SoundDataRawWav))
                            {
                                Console.WriteLine($"Playing file: {filename2}");
                                var sp = new SoundPlayer(ms);
                                sp.PlaySync();
                            }
                        }

                        //-- convert if enabled
                        if (opts.DecodeSub.EnableConversion)
                        {
                            if (!CheckAndPromptFileOverwrite(opts.QuietMode, filenamenew, hasMultipleMatch, opts.FileSub)) return null;
                            if (!opts.QuietMode) Console.WriteLine($"Creating sound file: {filenamenew}");

                            using (FileStream file = new FileStream(filenamenew, FileMode.Create, System.IO.FileAccess.Write))
                            {
                                file.Write(rsfsound.SoundDataRawWav, 0, rsfsound.SoundDataRawWav.Length);
                            }
                        }
                    }
                    break;

                case CONST_EXT_BMP:
                case CONST_EXT_PNG:
                case CONST_EXT_JPG:
                    {
                        if (opts.DecodeSub.EnableConversion)
                        {
                            // skip files that will be created and already exists to avoid back and forth conversion in one round 
                            filenamenew = Path.ChangeExtension(filename2, CONST_EXT_RGF);

                            if (!CheckAndPromptFileOverwrite(opts.QuietMode, filenamenew, hasMultipleMatch, opts.FileSub)) return null;
                            filenamenew = RGFHelper.RGFBitmap.ReadAndSaveFromBitmap(filename2, true);
                            if (!opts.QuietMode) Console.WriteLine($"Creating bitmap file: {filenamenew}");

                            var rgfbitmap = new RGFHelper.RGFBitmap(filenamenew);

                            //-- display the file
                            if (opts.DecodeSub.DisplayContents) DisplayRGFFileToConsole(rgfbitmap);
                        }
                    }
                    break;

                //-- RBF: Rudolph program file
                case CONST_EXT_RBF:
                    {
                        EV3DecompilerLib.Decompile.Decompiler.DEBUG_PRINT_LEVEL = opts.DecodeSub.DecodeDecompileDebugLevel;
                        EV3DecompilerLib.Recognize.PatternMatcher.DEBUG_PRINT_LEVEL = opts.DecodeSub.DecodeInterpretDebugLevel;
                        int iDecodeInterpretPrintDebugLevel = opts.DecodeSub.DecodeInterpretPrintDebugLevel;

                        filenamenew = DecodeConvertEV3ProgInternal(stream, filename2, opts.DecodeSub, opts.QuietMode, hasMultipleMatch, opts.FileSub);
                    }
                    break;

                //-- EV3: EV3 classic desktop file
                //-- EV3M: EV3 iPad file
                //-- EV3T: EV3 text node file
                case CONST_EXT_EV3:
                case CONST_EXT_EV3M:
                case CONST_EXT_EV3GB:
                    {
                        filenamenew = DecodeConvertEV3ProgInternal(stream, filename2, opts.DecodeSub, opts.QuietMode, hasMultipleMatch, opts.FileSub);
                        break;
                    }
                    break;


                default:
                    return null;
            }

            return filenamenew;
        }

        #region RGF handling
        private static bool? decodeUserScalingPreference = null; // user was prompted for scaling
        internal static void DisplayRGFFileToConsole(Stream stream)
        {
            RGFHelper.RGFBitmap rgfbitmap;
            rgfbitmap = new RGFHelper.RGFBitmap(stream);
            DisplayRGFFileToConsole(rgfbitmap);
        }

        internal static void DisplayRGFFileToConsole(RGFHelper.RGFBitmap rgfbitmap)
        {
            int? scale;
            scale = null;
            if (Console.WindowWidth < rgfbitmap.Width || Console.WindowHeight < rgfbitmap.Height)
            {
                if (!decodeUserScalingPreference.HasValue)
                {
                    Console.WriteLine($"Bitmap width is greater than display window. Allow resized bitmap display? [Y/n]");
                    var key = Console.ReadKey();
                    decodeUserScalingPreference = (key.Key == ConsoleKey.Enter || key.KeyChar.ToString().ToLower() == "y");
                }
                scale = decodeUserScalingPreference.Value ? null : (int?)1;
            }
            else { scale = 1; }
            rgfbitmap.PrintToConsole(scale);
        }
        #endregion RGF handling

        #region RBF, EV3 handling
        private static string DecodeConvertEV3ProgInternal(Stream stream, string filename, CommandOptions.DecodeSubOptions decodeSub, bool quietMode,
            bool isWithinLoop, CommandOptions.FileSubOptions filesubopts)
        {
            //-- check target extension
            if (CheckTargetExtension(decodeSub, out string targetExt,
                CONST_EXT_txt, CONST_EXT_EV3GB, CONST_EXT_html, CONST_EXT_json, CONST_EXT_yaml, CONST_EXT_EV3) == null) return null;
            string destfile = Path.ChangeExtension(filename, targetExt);

            if (!quietMode) Console.WriteLine($"Decoding program file contents '{filename}'");

            Dictionary<string, Node> retNodes = null;
            List<Node> retNodes2 = null;
            Project ev3project = null;

            // register converters
            EV3DecompilerLib.Recognize.PatternMatcher.RegisterConversion();
            EV3TreeVisLib.Ev3ZipTraverser.RegisterConversion();
            EV3ModelLib.Import.ImportEV3GB.RegisterConversion();

            // debug handling
            switch (Path.GetExtension(filename).ToLower())
            {
                case CONST_EXT_RBF:
                    {
                        EV3DecompilerLib.Decompile.Decompiler.DEBUG_PRINT_LEVEL = decodeSub.DecodeDecompileDebugLevel;
                        EV3DecompilerLib.Recognize.PatternMatcher.DEBUG_PRINT_LEVEL = decodeSub.DecodeInterpretDebugLevel;
                        int iDecodeInterpretPrintDebugLevel = decodeSub.DecodeInterpretPrintDebugLevel;

                        //-- debug info with paging
                        if (iDecodeInterpretPrintDebugLevel > 0)
                        {
                            var bytecode = new EV3DecompilerLib.Decompile.Decompiler().BuildByteCodeForFile(stream, filename);
                            var resultdata = new EV3DecompilerLib.Recognize.PatternMatcher().RecognizeEV3Calls2(bytecode, filename);

                            //-- dummy call, with no formatting to annotate
                            var txt = bytecode.DetailedToString(iDecodeInterpretPrintDebugLevel);

                            foreach (var line in txt.Split(new[] { Environment.NewLine }, StringSplitOptions.None))
                                PrintLine(line, line.Trim().StartsWith("#") ? ConsoleColor.Green : ConsoleColor.Gray);
                        }
                    }
                    break;

                case CONST_EXT_EV3:
                case CONST_EXT_EV3M:
                case CONST_EXT_EV3GB:
                    // NOOP
                    break;

                default:
                    Console.WriteLine("Skipping file. Conversion from source extension not possible.");
                    return null;
            }

            //-- execute conversion
            ProjectResultData projresult = ProjectConversion.Convert(stream, filename);
            retNodes = projresult.Nodes;
            retNodes2 = retNodes.Values.ToList();
            //retProject = projresult.Project;
            //if (mode == CONST_MODE_HTML && retProject?.ProjectNode != null) retNodes.Insert(0, retProject?.ProjectNode);

            //-- print out recognized tree
            if (decodeSub.DisplayContents && (!filesubopts.OutputToStdOut || !decodeSub.EnableConversion))
            {
                //-- re-format output, needs to go to the console instead of html or txt
                Node.PrintOptions.TargetColorIndex =
                    Console.IsInputRedirected || Console.IsOutputRedirected ?
                        PrintOptionsClass.TargetColorEnum.None :
                        PrintOptionsClass.TargetColorEnum.Console;

                if (targetExt == CONST_EXT_EV3GB) Node.PrintOptions.ApplyEV3GBasicFormat();

                //Node nProjectRoot = ev3project?.ProjectNode;
                //if (nProjectRoot != null) retNodes2.Insert(0, nProjectRoot);
                var resultStrConsole = retNodes2.GetFormattedOutput(true);

                foreach (var line in resultStrConsole.Split(new[] { Environment.NewLine }, StringSplitOptions.None))
                    PrintLine(line);
            }

            //-- export rbf program based on format (default is ev3) rbf->ev3/json/html/txt
            if (decodeSub.EnableConversion)
            {
                //-- generate output string
                string recognizedEV3BlocksStr = null;
                switch (targetExt)
                {
                    case CONST_EXT_txt:
                    case CONST_EXT_EV3GB:
                    case CONST_EXT_html:
                        //case "ev3": 
                        //default:
                        {
                            Node.PrintOptions.TargetColorIndex =
                                    targetExt == CONST_EXT_html ?
                                        PrintOptionsClass.TargetColorEnum.Html :
                                            PrintOptionsClass.TargetColorEnum.None;
                            Node.PrintOptions.pmdHighlight = null;
                            if (targetExt == CONST_EXT_EV3GB)
                            {
                                Node.PrintOptions.ApplyEV3GBasicFormat();
                            }
                            else
                            {
                                Node nProjectRoot = ev3project?.ProjectNode;
                                if (nProjectRoot != null) retNodes2.Insert(0, nProjectRoot);
                            }

                            recognizedEV3BlocksStr = retNodes2.GetFormattedOutput(true);
                            //recognizedEV3BlocksStr = retNodes.Values.ToList().GetFormattedOutput(true);
                        }
                        break;

                    case CONST_EXT_json:
                    case CONST_EXT_yaml:
                        {
                            string str = null;
                            if (targetExt == CONST_EXT_json)
                            {
                                str = SerializationHelper.SerializeToJSON(retNodes, true);
                            }
                            else if (targetExt == CONST_EXT_yaml)
                            {
                                str = SerializationHelper.SerializeToYAML(retNodes);
                            }
                            recognizedEV3BlocksStr = str;
                        }
                        break;
                }

                //-- check if exists
                if (!filesubopts.OutputToStdOut && !CheckAndPromptFileOverwrite(quietMode, destfile, isWithinLoop, filesubopts)) return null;

                switch (targetExt)
                {
                    case CONST_EXT_txt:
                    case CONST_EXT_EV3GB:
                    case CONST_EXT_html:
                    case CONST_EXT_json:
                    case CONST_EXT_yaml:
                        {
                            if (!quietMode) Console.WriteLine($"Converting to: {destfile}");
                            if (!filesubopts.OutputToStdOut) File.WriteAllText(destfile, recognizedEV3BlocksStr);
                            else Console.WriteLine(recognizedEV3BlocksStr);
                        }
                        break;

                    case CONST_EXT_EV3:
                        //default:
                        {
                            //-- check target extension
                            Console.WriteLine($"Restoring EV3 project file {destfile}");

                            //-- restore file
                            using (var fsout = File.OpenWrite(destfile))
                            {
                                new EV3GBuilder().BuildEV3(retNodes, filename, fsout);
                            }
                        }
                        break;
                }
            }

            return destfile;
        }

        /// <summary>
        /// Check if target extension is OK for conversion
        /// </summary>
        /// <param name="decodeSub"></param>
        /// <param name="extensions"></param>
        /// <param name="targetExt"></param>
        /// <returns></returns>
        private static string CheckTargetExtension(CommandOptions.DecodeSubOptions decodeSub, out string targetExt, params string[] extensions)
        {
            targetExt = decodeSub.DecodeTargetExt;
            if (!targetExt.In(extensions))
            {
                if (string.IsNullOrEmpty(targetExt)) targetExt = extensions.First();
                else { Console.WriteLine("Skipping file. Conversion to target extension not possible."); return null; }
            }

            return targetExt;
        }

        /// <summary>
        /// Check if file can be written or overwrite is allowed
        /// </summary>
        /// <param name="quietMode"></param>
        /// <param name="destfile"></param>
        private static bool CheckAndPromptFileOverwrite(bool quietMode, string destfile, bool isWithinLoop, CommandOptions.FileSubOptions filesub)
        {
            if (File.Exists(destfile) && !quietMode)
            {
                if (filesub.QuietOverwrite) return true;

                if (!Console.IsInputRedirected && !Console.IsOutputRedirected)
                {
                    var retval = true;
                    if (filesub.NeverOverwrite) retval = false;

                    if (retval)
                    {
                        Console.WriteLine($@"File already exists. Do you want to overwrite '{Path.GetFileName(destfile)}'? " +
                            (!isWithinLoop ? "[Overwrite / Skip]" : "[Overwrite / overwrite All / Skip / sKip all]")); //TODO: change to nEver

                        var key = Console.ReadKey(true);
                        if (isWithinLoop && key.KeyChar.ToString().ToLower() == "k") { filesub.NeverOverwrite = true; retval = false; }
                        else if (isWithinLoop && key.KeyChar.ToString().ToLower() == "a") { filesub.QuietOverwrite = true; }
                        else if (key.Key != ConsoleKey.Enter && key.KeyChar.ToString().ToLower() != "o") { retval = false; }
                    }

                    if (!retval)
                    {
                        Console.WriteLine($"Skipping overwriting existing file '{destfile}'.");
                        return false;
                    }
                }
                else
                {
                    Console.WriteLine($"Skipping overwriting existing file '{destfile}' as console input is redirected.");
                    return false;
                }
            }
            return true;
        }

        private static bool usepaging = !Console.IsOutputRedirected;
        private static int pagingcount = 0;
        /// <summary>
        /// Print lines with paging
        /// </summary>
        /// <param name="line"></param>
        /// <param name="color"></param>
        private static void PrintLine(string line, ConsoleColor? color = null)
        {
            if (Console.IsOutputRedirected) { Console.WriteLine(line); return; }

            switch (Node.PrintOptions.TargetColorIndex)
            {
                //case PrintOptionsClass.TargetColorEnum.None:
                //case PrintOptionsClass.TargetColorEnum.Html:
                default:
                    ConsoleExt.WriteLine(line, color);
                    break;
                case PrintOptionsClass.TargetColorEnum.Console:
                    {
                        (int ct, int cl) = (Console.CursorTop, Console.CursorLeft);
                        try
                        {
                            Colorizer.WriteLine(line);
                        }
                        catch (Exception)
                        {
                            Console.SetCursorPosition(cl, ct);
                            Console.WriteLine(line);
                        }
                    }
                    break;
            }

            pagingcount += line.Count(ch => ch == '\n') + 1;
            if (usepaging && pagingcount >= Console.WindowHeight - 4)
            {
                ConsoleExt.Write("-- More --", ConsoleColor.White);
                ConsoleExt.Write(" (space = advance 1 page, enter = advance without paging, q = quit) ", ConsoleColor.DarkGray);
                // wait for user input key press 
                while (true)
                {
                    var key = Console.ReadKey(true);

                    // space = advance 1 page, enter = advance without paging, q = quit 
                    if (key.KeyChar == 'q') Environment.Exit(0);
                    if (key.Key == ConsoleKey.Enter) { usepaging = false; break; }
                    if (key.KeyChar == ' ') break;
                }
                // clear the prompt
                int currentLineCursor = Console.CursorTop;

                // clearline - move to line start and clear it
                Console.SetCursorPosition(0, currentLineCursor);
                Console.WriteLine(new string(' ', Console.BufferWidth));
                Console.SetCursorPosition(0, currentLineCursor);

                pagingcount = 0;
            }
        }
        #endregion
    }
}
