﻿using EV3BrickMagic.Command.InteractiveWindow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EV3BrickMagic.Command
{
    [TriggeredByCommand(Commands.InteractiveWindow)]
    public class CommandInteractiveWindow : ICommand
    {
        public int Execute(CommandOptions opts)
        {
            Application.EnableVisualStyles();
            var f = new MainForm();
            Application.Run(f);

            return 0;
        }
    }
}