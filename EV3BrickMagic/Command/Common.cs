﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV3BrickMagic.Command
{
    internal static class Common
    {
        internal const string BRICK_PATH_PREFIX = "brick:";
        internal const string STDINOUT_PATH = "--";
        internal readonly static char[] BUSYINDICATOR = { '|', '/', '-', '\\' };
    }
}
