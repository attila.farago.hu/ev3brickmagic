﻿using EV3BrickMagic.Helpers;
using EV3DecompilerLib.Recognize;
using EV3MonoBrickLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EV3BrickMagic.Command
{
    [TriggeredByCommand(Commands.StartProgram)]
    class CommandStartProgram : ICommand
    {
        public int Execute(CommandOptions opts)
        {
            //if (opts.ProgramSub.DebugExperimental)
            //{
            //    Console.WriteLine("Experimental debugging feature.");
            //    var filesrc = opts.ProgramSub.Brickfile;
            //    var filedst = Path.GetFileName(filesrc);
            //    EV3.Brick.FileSystem.ReadFile(filesrc, filedst);
            //    //TODO: read into filestream only

            //    //TODO: CommandDecodeFile._DecodeDataFileInternal
            //    var bytecode = new EV3DecompilerLib.Decompile.Decompiler().BuildByteCodeForFile(filedst);
            //    var recognizedEV3Blocks = new EV3DecompilerLib.Recognize.PatternMatcher().RecognizeEV3Calls(bytecode, filedst);
            //    Console.WriteLine(bytecode.DetailedToString(2));

            //    var originalY = Console.CursorTop;
            //    EV3DecompilerLib.Recognize.PatternMatchData.PrintPMDList(recognizedEV3Blocks.program);

            //    /// Start program on Brick
            //    EV3.Brick.StopProgram();
            //    EV3.Brick.StartProgram(opts.ProgramSub.Brickfile);
            //    Console.WriteLine($"Started program {opts.ProgramSub.Brickfile}.");

            //    Int32[] globals_last = null;
            //    Int32[] globals = null;
            //    while (EV3.Brick.IsProgramRunning())
            //    {
            //        globals = EV3.Brick.ReadGlobals(0, 400);
            //        int readlen = globals.Length;
            //        List<PatternMatchData> changeList = new List<PatternMatchData>();
            //        for (int i = 0; i < Math.Min(readlen, bytecode.Globals); i++)
            //        {
            //            if (globals[i] > 0 && (globals_last == null || globals[i] != globals_last[i]))
            //            {
            //                var globalsElem = i * 4;
            //                var pmdFound = recognizedEV3Blocks.program.Flatten(x => x).FirstOrDefault(pmd => { return pmd.TrackerGlobalStart == globalsElem; });
            //                //TODO: hash
            //                if (pmdFound != null)
            //                {
            //                    //Console.Write($"GLOBAL{i} = {globals[i]}");
            //                    //Console.Write("\t" + found.Name + "\t" + (found.TrackerGlobalStart == changed ? "start" : "end"));
            //                    //Console.WriteLine();

            //                    changeList.Add(pmdFound);
            //                }
            //            }
            //        }

            //        //if (changeList.Count > 0)
            //        {
            //            var pmdLastChange = changeList.Count > 0 ? changeList
            //                .OrderBy(elem => elem.Offset) // todo ordering base on ...?
            //                .Last() : null;
            //            Console.CursorVisible = false;
            //            Console.SetCursorPosition(0, originalY);
            //            EV3DecompilerLib.Recognize.PatternMatchData.PrintPMDList(recognizedEV3Blocks.program, changeList);

            //            //var idx = 0;
            //            ////Console.CursorTop = originalY;
            //            //while (idx < 40)
            //            //{
            //            //    Console.Write($"{idx * 4}({globals[idx]})\t");
            //            //    if (idx % 10 == 0) Console.WriteLine();
            //            //    idx++;
            //            //}
            //            //Console.Write(DateTime.Now.Ticks); Console.Write(" "); Console.Write(changeList.Count);
            //        }
            //        globals_last = globals;

            //        Thread.Sleep(10);
            //    }

            //    Console.CursorVisible = true;
            //    return;
            //}

            /// Start program on Brick
            if (!EV3.Brick.FileSystem.FileExists(opts.ProgramSub.Brickfile)) return 2;
            if (EV3.Brick.IsProgramRunning()) EV3.Brick.StopProgram(true); //-- stop if any other programs were running
            if (opts.ProgramSub.UpdateFavoritesCache) EV3.Brick.PutCache(opts.ProgramSub.Brickfile);//-- put it on the top of most recently used list
            EV3.Brick.StartProgram(opts.ProgramSub.Brickfile);

            if (!opts.QuietMode) Console.WriteLine($"Started program {opts.ProgramSub.Brickfile}.");

            if (opts.ProgramSub.WaitForFinish)
            {
                Console.Write("Waiting for program completition (or press SPACE to stop)  ");
                int BUSYINDICATORIdx = 0;
                while (EV3.Brick.IsProgramRunning())
                {
                    if (Console.KeyAvailable && Console.ReadKey(true).Key == ConsoleKey.Spacebar)
                    {
                        EV3.Brick.StopProgram();
                        break;
                    }
                    Thread.Sleep(100);
                    Console.Write("\b" + Common.BUSYINDICATOR[BUSYINDICATORIdx]);
                    BUSYINDICATORIdx = (++BUSYINDICATORIdx % Common.BUSYINDICATOR.Length);
                }
                Console.Write("\b ");
                Console.WriteLine();
            }
            return 0;
        }
    }
}
