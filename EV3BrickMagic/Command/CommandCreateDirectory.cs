﻿using EV3MonoBrickLib;
using MonoBrick.EV3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV3BrickMagic.Command
{
    [TriggeredByCommand(Commands.CreateDirectory)]
    class CommandCreateDirectory : ICommand
    {
        public int Execute(CommandOptions opts)
        {
            var tdir = opts.FileSub.Destination;

            if (!tdir.StartsWith("/") && !tdir.StartsWith("..")) tdir = FilSystem.BRICK_PROJECTS_FOLDER + tdir;

            Console.WriteLine($"Creating directory {tdir}");
            EV3.Brick.FileSystem.CreateDirectory(tdir);

            return 0;
        }
    }
}
