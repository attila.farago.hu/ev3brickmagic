﻿using EV3MonoBrickLib;
using EV3MonoBrickLib.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace EV3BrickMagic.Command
{
    [TriggeredByCommand(Commands.Sound)]
    class CommandSound : ICommand
    {
        const ushort DEFAULT_DURATION = 100;

        public int Execute(CommandOptions opts)
        {
            if (!opts.SoundSub.PianoMode)
                PlaySounds(opts);
            else if (opts.SoundSub.PianoMode)
                PlayPiano(opts);

            return 0;
        }

        /// <summary>
        /// Play sounds
        /// </summary>
        /// <param name="opts"></param>
        private static void PlaySounds(CommandOptions opts)
        {
            Regex rex = new Regex(
                @"((?<tempo>\d+)bpm)|(!!(?<volumeall>[1-9]\d*))|(((?<note>(?<notekey>[CDEFGAH])(?<sharp>#)?(?<octave>[45678])?(!(?<volume>[1-9]\d*))?)|(?<rest>\W))(?<length>[odqe]|[._\-])*)",
                RegexOptions.Compiled);

            //opts.SoundSub.SoundSequence = "600bpm!!20CECEG-G-!!40CECEG-G-!!100CECEG-G-";
            var matches = rex.Matches(opts.SoundSub.SoundSequence.Trim());

            // BPS means quarters per second -- 1000/bps=duration or 1000/duration=bps
            var tempo_bps = (ushort)(60000.0 / DEFAULT_DURATION);
            byte volumeall = 5;
            foreach (Match match in matches)
            {
                ushort beat_duration = (ushort)(60000.0 / tempo_bps);
                var groups = match.Groups.Cast<Group>().ToList();

                // set new permanent tempo 
                var tempostr = _groupCaptureGet("tempo", groups).FirstOrDefault();
                if (tempostr != null && ushort.TryParse(tempostr, out tempo_bps))
                    continue;

                // set permanent volume
                var volumestr = _groupCaptureGet("volumeall", groups).FirstOrDefault();
                if (volumestr != null && byte.TryParse(volumestr, out volumeall))
                    continue;

                // read notes
                var notekey = _groupCaptureGet("notekey", groups).FirstOrDefault();
                var duration1 = _parseDuration(_groupCaptureGet("length", groups), beat_duration);
                if (notekey != null)
                {
                    var sharp = _groupCaptureGet("sharp", groups).FirstOrDefault();
                    var octave = _groupCaptureGet("octave", groups).FirstOrDefault();
                    if (!byte.TryParse(_groupCaptureGet("volume", groups).FirstOrDefault(), out byte volumethis)) volumethis = volumeall;

                    EV3.Brick.PlayNote(volumethis, notekey + sharp + octave, duration1, true);
                    continue;
                }

                // read rest/pause
                if (_groupCaptureGet("rest", groups).FirstOrDefault() != null)
                {
                    Thread.Sleep(duration1);
                }
            }
        }

        /// <summary>
        /// Get matches from capture group
        /// </summary>
        /// <param name="groupName"></param>
        /// <param name="groups"></param>
        /// <returns></returns>
        private static List<string> _groupCaptureGet(string groupName, List<Group> groups)
        {
            var elems = groups.Where(elem => elem.Name == groupName)
                .SelectMany(elem => elem.Captures.Cast<Capture>().Select(capture => capture.Value))
                .ToList();
            //if (groups.Any(elem => elem.Name == groupName && elem.Captures.Count > 0))return "";
            return elems;
        }

        /// <summary>
        /// Parse duration strings
        /// </summary>
        /// <param name="pauseElements"></param>
        /// <param name="beat_duration"></param>
        /// <returns></returns>
        private static ushort _parseDuration(List<string> pauseElements, ushort beat_duration)
        {
            ushort duration = beat_duration;
            ushort duration_plus = 0;
            foreach (var noteLengthElem in pauseElements)
            {
                switch (noteLengthElem)
                {
                    case "o": duration = (ushort)(beat_duration * 4); break;
                    case "d": duration = (ushort)(beat_duration * 2); break;
                    case "q": duration = (ushort)(beat_duration * 1); break;
                    case "e": duration = (ushort)(beat_duration / 2); break;
                    case "-": case "_": duration_plus += duration; break;
                    case ".": duration_plus += (ushort)(duration / 2); break;
                }
            }
            duration = (ushort)(duration + duration_plus);
            return duration;
        }

        /// <summary>
        /// Piano mode
        /// </summary>
        private static void PlayPiano(CommandOptions opts)
        {
            const string PIANO_TRANSLATION = "1234567890qwertyuiopasdfghjklz";
            SoundHelper.Note note_first = SoundHelper._DoubleToNote(0);
            SoundHelper.Note note_last = SoundHelper._DoubleToNote(PIANO_TRANSLATION.Length - 1);
            Console.WriteLine($"Piano mode on. Press any [{PIANO_TRANSLATION}] key to play between {note_first}-{note_last} or ESC to quit.");
            while (true)
            {
                var key = Console.ReadKey(true);

                var idx = PIANO_TRANSLATION.IndexOf(key.KeyChar);
                //var i = translation.
                if (idx < 0) break;

                //if ((key.Modifiers & ConsoleModifiers.Shift) != 0)
                //{
                //    var halfkeys = new List<int>() { 0, 1, 3, 4, 5 };
                //    if (halfkeys.Contains(idx)) idx++;
                //}
                SoundHelper.Note note = SoundHelper._DoubleToNote(idx);

                EV3.Brick.PlayNote(5, note, DEFAULT_DURATION, true);
            }
        }
    }
}
