﻿using EV3MonoBrickLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV3BrickMagic.Command
{
    [TriggeredByCommand(Commands.ListPorts)]
    class CommandListPorts : ICommand
    {
        public int Execute(CommandOptions opts)
        {
            if (!opts.QuietMode) Console.WriteLine("Possible connection ports:");
            List<ComHelper.PortInfo> ports = GetAllPorts();
            foreach (var port in ports)
            {
                Console.WriteLine($"  {port.Port,-7} {port.DeviceName,-15} {port.DeviceSerial,-12} {port.LastSeen,10}");
            }
            return 0;
        }

        public static List<ComHelper.PortInfo> GetAllPorts()
        {
            List<ComHelper.PortInfo> ports = null;
            var usbports = ComHelper.GetUSBPorts();
            try
            {
                var btports = _GetBTPortsInternal(); // ComHelper.GetSerialPorts(false).Where(elem => !string.IsNullOrEmpty(elem.DeviceName)).ToList();
                ports = usbports.Concat(btports).ToList();
            }
            catch { ports = usbports; }
            if (!usbports.Any()) ports.Insert(0, new ComHelper.PortInfo() { Port = "USB" });
            return ports;
        }

        internal static List<ComHelper.PortInfo> _GetBTPortsInternal()
        {
            var ports = ComHelper.GetSerialPorts(false).Where(elem => !string.IsNullOrEmpty(elem.DeviceName)).ToList();
            return ports;
        }
    }
}
