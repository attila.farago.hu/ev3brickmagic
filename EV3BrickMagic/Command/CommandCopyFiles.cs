﻿using EV3BrickMagic.Helpers;
using EV3MonoBrickLib;
using MonoBrick.EV3;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV3BrickMagic.Command
{
    [TriggeredByCommand(Commands.Copy)]
    class CommandCopyFiles : ICommand
    {
        public int Execute(CommandOptions opts)
        {
            /// Copy files to and from the brick
            //var cmdline = "brick:test1/Program1.rbf .";
            //var cmdline = "brick:test1/Program1.rbf .";
            //var cmdline = "brick:test*/Program1.rbf .";
            //var cmdline = "brick:test?/Program1.rbf .";
            //var cmdline = "brick:test1/Program*.rbf out\";
            //var cmdline = "brick:*.rbf .";
            //var cmdline = "brick:*.rbf .";
            //var cmdline = "Neutral.rgf brick:test1/";
            //var cmdline = "*.rgf brick:test1/";
            //var cmdline = "brick:test1/Program.rbf --";
            //var cmdline = "-- brick:test1/Program.rbf";
            //var cmdline = "test1/Program.rbf ."; //assume par1 as brick
            //var cmdline = "*.rbf test1/Program.rbf"; //NOT SUPPORTED: assume par2 as brick
            //var cmdline = "test1/Program.rbf --"; //assume par1 as brick
            //var cmdline = "-- test1/Program.rbf"; //assume par2 as brick
            var fileSource = opts.FileSub.Source;
            var fileTarget = opts.FileSub.Destination;

            bool? brickIsAtSource = null;
            if (fileSource.StartsWith(Common.BRICK_PATH_PREFIX))
            {
                fileSource = fileSource.Substring(Common.BRICK_PATH_PREFIX.Length);
                brickIsAtSource = true;
            }
            else if (fileTarget.StartsWith(Common.BRICK_PATH_PREFIX))
            {
                fileTarget = fileTarget.Substring(Common.BRICK_PATH_PREFIX.Length);
                brickIsAtSource = false;
            }
            else
            {
                //fileTarget != Common.STDINOUT_PATH
                if (fileSource == Common.STDINOUT_PATH)
                {
                    brickIsAtSource = false;
                }
                else if (fileTarget == Common.STDINOUT_PATH)
                {
                    brickIsAtSource = true;
                }
                else if (Directory.Exists(fileTarget))
                {
                    brickIsAtSource = true;
                }
                else if ((Directory.Exists(Path.GetDirectoryName(fileSource)) && Directory.GetFiles(Path.GetDirectoryName(fileSource), Path.GetFileName(fileSource)).Length > 0) ||
                    (Path.GetDirectoryName(fileSource) == "") && Directory.GetFiles(".", Path.GetFileName(fileSource)).Length > 0)
                {
                    brickIsAtSource = false;
                }

                if (!opts.QuietMode)
                {
                    if (brickIsAtSource == true)
                        Console.WriteLine("Assuming brick->local copy. Consider adding \"brick:\" prefix either to the source or to the target path.");
                    else if (brickIsAtSource == false)
                        Console.WriteLine("Assuming local->brick copy. Consider adding \"brick:\" prefix either to the source or to the target path.");
                }
            }

            if (brickIsAtSource == true)
            {
                fileSource = MonoBrick.EV3.FilSystem.AlignProjectFilename(fileSource);
                return _CopyBrickToLocalInternal(opts, fileSource, fileTarget) ?
                    0 : 1;
            }
            else if (brickIsAtSource == false)
            {
                fileTarget = MonoBrick.EV3.FilSystem.AlignProjectFilename(fileTarget);
                return _CopyLocalToBrickInternal(opts, fileSource, fileTarget) ?
                    0 : 1;
            }
            else
            {
                Console.WriteLine("Error: either source or target file must refer to the brick.");
                return 1;
            }
        }
        /// <summary>
        /// Copy files from brick to local filesystem
        /// </summary>
        /// <param name="opts"></param>
        /// <param name="fileSource"></param>
        /// <param name="fileTarget"></param>
        private static bool _CopyBrickToLocalInternal(CommandOptions opts, string fileSource, string fileTarget)
        {
            //-- check target file or dir
            bool stdinout_active = fileTarget == Common.STDINOUT_PATH;
            if (!stdinout_active && !Directory.Exists(fileTarget))
            {
                Console.WriteLine($"Error: Target directory and does not exist");
                return false;
            }

            //-- determine lowest cleary (no wildcards) specified directory - to create local file structure only above
            var pathelemsSource0 = Path.GetDirectoryName(fileSource);
            var pathelemsSource = pathelemsSource0.Split('/', '\\');
            var sourceSearchDirectory = string.Join("/", pathelemsSource.TakeWhile(elem => !elem.ContainsAny(StringExtensions.WILDCHARS)));
            if (string.IsNullOrEmpty(sourceSearchDirectory)) sourceSearchDirectory = FilSystem.BRICK_PROJECTS_FOLDER;
            if (string.IsNullOrEmpty(Path.GetFileName(fileSource))) fileSource += "*";
            string sourceBaseDirectoryForBrickTarget = sourceSearchDirectory;

            List<string> sourceFiles = IterateBrickFolders(sourceSearchDirectory, new[] { fileSource }, true);
            if (sourceFiles == null) throw new Exception("Brick file reading failed.");

            //-- perform copy to local file system
            bool anySuccess = false;
            foreach (var file1 in sourceFiles)
            {
                try
                {
                    string destfile = null;
                    //-- MODE: BRICK -> LOCAL
                    if (!stdinout_active)
                    {
                        destfile = file1;
                        if (!file1.StartsWith(sourceBaseDirectoryForBrickTarget)) { Console.WriteLine($"Error in file {file1}"); continue; }
                        destfile = destfile.Substring(sourceBaseDirectoryForBrickTarget.Length).TrimStart('/', '\\');
                        destfile = Path.Combine(fileTarget, destfile);
                        destfile = destfile?.Replace('/', '\\');
                    }

                    if (!opts.QuietMode) Console.WriteLine($"Copying {file1} to {(!stdinout_active ? destfile : "console")}");
                    if (!stdinout_active && !Directory.Exists(Path.GetDirectoryName(destfile))) Directory.CreateDirectory(Path.GetDirectoryName(destfile));
                    if (!stdinout_active && File.Exists(destfile) && !opts.FileSub.QuietOverwrite)
                    {
                        Console.WriteLine($"Local file already exists. Do you want to overwrite? [Y/n]");
                        var key = Console.ReadKey(true);
                        if (key.KeyChar.ToString().ToLower() == "q") return false;
                        if (key.Key != ConsoleKey.Enter && key.KeyChar.ToString().ToLower() != "y") continue;
                    }

                    //-- perform file copy (read)
                    if (!stdinout_active)
                    {
                        EV3.Brick.FileSystem.ReadFile(file1, destfile);
                        anySuccess = true;

                        //-- decode file of requested
                        if (opts.FileSub.DecodeEnabled)
                        {
                            Console.WriteLine();

                            var opts2 = opts;
                            opts2.DecodeSub.DisplayContents = true; opts2.DecodeSub.EnableConversion = false;
                            CommandConvertFile._DecodeDataFileInternal(destfile, sourceFiles.Count > 1, opts2);
                            //conversion is disabled -> no need to worry on re-conversion of the same file back and forth

                            if (sourceFiles.Last() != file1) Console.WriteLine(new string('-', 80));
                        }
                    }
                    else
                    {
                        // output to console
                        var decodeSuccess = false;
                        if (opts.FileSub.DecodeEnabled)
                        {
                            byte[] ba = EV3.Brick.FileSystem.ReadFileToByteArray(file1);
                            anySuccess = true;

                            using (MemoryStream stream = new MemoryStream(ba))
                            {
                                var opts2 = opts;
                                opts2.DecodeSub.DisplayContents = true; opts2.DecodeSub.EnableConversion = false;
                                //conversion is disabled -> no need to worry on re-conversion of the same file back and forth
                                decodeSuccess = CommandConvertFile._DecodeDataFileInternal(stream, file1, sourceFiles.Count > 1, opts) != null;
                            }
                        }

                        if (!decodeSuccess)
                        {
                            string s = EV3.Brick.FileSystem.ReadFileToString(file1);

                            Console.ForegroundColor = ConsoleColor.DarkGray;
                            Console.WriteLine(s.Replace("\r\n", "\r").Replace("\r", "\r\n"));
                            Console.ForegroundColor = ConsoleColor.Gray;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error while copying: {ex.Message}");
                }
            }

            return anySuccess;
        }

        /// <summary>
        /// Copy files from local to brick filesystem
        /// </summary>
        /// <param name="opts"></param>
        /// <param name="fileSource"></param>
        /// <param name="fileTarget"></param>
        private static bool _CopyLocalToBrickInternal(CommandOptions opts, string fileSource, string fileTarget)
        {
            //-- check target file or dir
            bool stdinout_active = fileSource == Common.STDINOUT_PATH;

            List<string> sourceFiles;
            if (string.IsNullOrEmpty(Path.GetFileName(fileSource))) fileSource += "*";
            string targetDir = null;
            if (!stdinout_active)
            {
                var dirname = Path.GetDirectoryName(fileSource); if (string.IsNullOrEmpty(dirname)) dirname = ".";
                sourceFiles = Directory.EnumerateFiles(dirname, Path.GetFileName(fileSource), SearchOption.TopDirectoryOnly)
                    .ToList();

                //-- check if source file exists
                if (sourceFiles.Count == 0)
                {
                    Console.WriteLine($"Error: Source files {fileSource} does not exist.");
                    return false;
                }

                targetDir = fileTarget;
            }
            else
            {
                sourceFiles = new List<string>() { Common.STDINOUT_PATH };

                targetDir = Path.GetDirectoryName(fileTarget).Replace('\\', '/');
            }

            //-- check if target directory exists
            if (!EV3.Brick.FileSystem.FileExists(targetDir))
            {
                Console.WriteLine($"Error: Target directory {targetDir} does not exist on brick.");
                return false;
            }

            //-- perform copy to local file system
            bool anySuccess = false;
            foreach (var file1 in sourceFiles)
            {
                string destfile = null;

                //-- MODE: LOCAL --> BRICK
                if (!stdinout_active)
                {
                    destfile = Path.GetFileName(file1);
                    destfile = Path.Combine(fileTarget, destfile);
                }
                else
                {
                    destfile = fileTarget;
                }
                destfile = destfile?.Replace('\\', '/');

                //-- check target filename length
                if (Path.GetFileName(destfile).Length >= 32)
                {
                    Console.WriteLine($"Skipping file {destfile} as it exceeds maximum length of 32 characters.");
                    continue;
                }

                if (EV3.Brick.FileSystem.FileExists(destfile) && !opts.FileSub.QuietOverwrite)
                {
                    if (!Console.IsInputRedirected)
                    {
                        Console.WriteLine($"Brick file already exists. Do you want to overwrite? [Y/N]");
                        var key = Console.ReadKey(true);
                        if (key.KeyChar.ToString().ToLower() == "q") return false;
                        if (key.Key != ConsoleKey.Enter && key.KeyChar.ToString().ToLower() != "y") continue;
                    }
                    else
                    {
                        Console.WriteLine($"Skipping copy to existing brick file '{destfile}' as console input is redirected.");
                    }
                }

                //-- perform file copy (write)
                Console.WriteLine($"Copying {(!stdinout_active ? file1 : "console")} to {destfile}");
                if (!stdinout_active)
                {
                    EV3.Brick.FileSystem.WriteFile(file1, destfile);
                    anySuccess = true;
                }
                else
                {
                    // read from console
                    if (!Console.IsInputRedirected)
                        Console.WriteLine($"Please enter data terminated by an empty newline.");
                    string contents = null;
                    string readline;
                    while (true)
                    {
                        readline = Console.ReadLine();
                        if (string.IsNullOrEmpty(readline)) break;
                        if (contents != null) contents += "\r";
                        contents += readline;
                    }
                    EV3.Brick.FileSystem.WriteFileFromString(contents, destfile);
                    anySuccess = true;
                }
            }

            return anySuccess;
        }

        /// <summary>
        /// Iterate through brick folder, finding matching path/dir names
        /// </summary>
        /// <param name="path"></param>
        /// <param name="pattern"></param>
        /// <returns></returns>
        internal static List<string> IterateBrickFolders(string path, string[] patterns, bool recursion = false)
        {
            try
            {
                EV3.Brick.FileSystem.GetFolderInfo(path, out BrickFolder[] subfolders, out BrickFile[] files);
                List<string> retval = new List<string>();
                subfolders = subfolders
                    .Where(elem => !SKIPPED_DIRECTORIES.Any(skp => elem.FullName.StartsWith(skp)))
                    .ToArray();
                if (recursion)
                {
                    foreach (var folder in subfolders)
                    {
                        //IDEA: dive only if path matches the pattern so far -- if (folder.Like(Path.GetDirectoryName(pattern).Replace(@"\", "/")))
                        {
                            var sublist = IterateBrickFolders(folder.FullName, patterns, recursion);
                            if (sublist != null)
                                retval.AddRange(sublist);
                        }
                    }
                }

                var matchedFiles = files.Select(elem => elem.FullName)
                    .Where(elem => patterns.Any(pat => elem.Like(pat)))
                    .ToList();
                retval.AddRange(matchedFiles);
                return retval;
            }
            catch
            {
                //NOOP
            }

            return null;
        }

        readonly static List<string> SKIPPED_DIRECTORIES = new List<string>() { "/usr/lib", "/usr/bin", "/usr/sbin", "/sbin", "/sys", "/proc" };
    }
}
