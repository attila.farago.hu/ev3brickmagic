﻿using EV3MonoBrickLib;
using MonoBrick.EV3;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV3BrickMagic.Command
{
    [TriggeredByCommand(Commands.ListFiles)]
    class CommandListFiles : ICommand
    {
        public int Execute(CommandOptions opts)
        {
            if (!opts.QuietMode) Console.WriteLine($"List of files and directories:");

            /// List files on brick
            var fileSource = opts.FileSub.Source ?? "";
            if (fileSource.StartsWith(Common.BRICK_PATH_PREFIX)) fileSource = fileSource.Substring(Common.BRICK_PATH_PREFIX.Length);
            if (string.IsNullOrEmpty(fileSource)) fileSource = "*";
            if (fileSource != "/") fileSource = fileSource.TrimEnd('/');
            var fileSourceNew = MonoBrick.EV3.FilSystem.AlignProjectFilename(fileSource);

            //var s = "../prjs/test1/Program.rbf";
            //var x = EV3.Brick.FileSystem.FileExists2(s);

            string path1 = Path.GetDirectoryName(fileSourceNew)?.Replace('\\', '/') ?? fileSourceNew;
            string path2 = Path.GetFileName(fileSourceNew);
            string basedir = path1;
            List<string> filepatterns = new List<string>() { fileSource, "*/" + fileSource, fileSource + "/*", "*/" + fileSource + "/*" };

            var dirCheck = EV3.Brick.FileSystem.GetFolderInfoHandleError(basedir);
            if (dirCheck.Item1 == null)
            {
                //-- determine lowest cleary (no wildcards) specified directory - to create local file structure only above
                var pathelemsSource0 = Path.GetDirectoryName(fileSourceNew);
                var pathelemsSource = pathelemsSource0.Split('/');
                var sourceSearchDirectory = string.Join("/", pathelemsSource.TakeWhile(elem => !elem.ContainsAny(StringExtensions.WILDCHARS)));
                if (string.IsNullOrEmpty(sourceSearchDirectory)) sourceSearchDirectory = FilSystem.BRICK_PROJECTS_FOLDER;
                basedir = sourceSearchDirectory;

                dirCheck = EV3.Brick.FileSystem.GetFolderInfoHandleError(basedir);
            }
            else if (dirCheck.Item1.Any(elem => Path.GetFileName(elem.Name) == path2))
            {
                // base folder exists - and also there is a meaningful subfolder
                basedir = fileSourceNew;
                dirCheck = EV3.Brick.FileSystem.GetFolderInfoHandleError(basedir);
            }
            else
            {
                //already done: NOOP
            }

            bool doRecursion = basedir != "/" && opts.FileSub.RecursiveList; // basedir.StartsWith(MonoBrick.EV3.FilSystem.BRICK_PROJECTS_FOLDER.TrimEnd('/'));
            List<string> sourceFiles = CommandCopyFiles.IterateBrickFolders(basedir, filepatterns.ToArray(), doRecursion);
            bool foundMatch = false;
            List<string> displayedFiles = new List<string>();
            if (dirCheck.Item1 != null && dirCheck.Item1.Length > 0)
            {
                IEnumerable<string> matchingDirs = dirCheck.Item1.Select(elem => elem.FullName);
                if (!dirCheck.Item1[0].Path.StartsWith(fileSource)) matchingDirs = matchingDirs.Where(elem => elem.Like(fileSource));
                if (matchingDirs.Count() > 0)
                {
                    foreach (var dir1 in matchingDirs)
                    {
                        var dir1trim = dir1.StartsWith(basedir) ? dir1.Substring(basedir.Length).TrimStart('/') : dir1;
                        ConsoleExt.WriteLine(dir1trim + "/", ConsoleColor.White);
                        var subfiles = sourceFiles
                                    .Where(elem1 => elem1.StartsWith(dir1 + "/"))
                                    //.Select(elem2 => (elem2.StartsWith(basedir) ? elem2.Substring(basedir.Length).TrimStart('/') : elem2))
                                    .OrderBy(elem3 => elem3)
                                    .ToArray();
                        string dirlast = dir1 + "/";
                        foreach (var file in subfiles)
                        {
                            var dir3 = new string(file.Reverse().SkipWhile(ch => ch != '/').Reverse().ToArray());
                            var dir3trim = dir3.StartsWith(basedir) ? dir3.Substring(basedir.Length + 1) : dir3;
                            if (dir3 != dirlast)
                            {
                                var printdir =
                                    (dir3.StartsWith(FilSystem.BRICK_PROJECTS_FOLDER)) ? dir3.Substring(FilSystem.BRICK_PROJECTS_FOLDER.Length) : dir3;
                                ConsoleExt.WriteLine("".PadLeft(Math.Max(dir3trim.Count(ch => ch == '/') - 1, 0) * 2) +
                                    printdir, ConsoleColor.White);
                                dirlast = dir3;
                            }
                            Console.WriteLine("".PadLeft(dir3trim.Count(ch => ch == '/') * 2) +
                                file.Substring(dir3.Length));
                            displayedFiles.Add(file);
                        }
                    }

                    foundMatch = true;
                }
            }

            //-- display all not yet displayed files
            var leftFiles = sourceFiles.Except(displayedFiles).ToArray();
            if (leftFiles.Count() > 0)
            {
                Console.WriteLine(string.Join("\n", leftFiles.Select(elem => elem.StartsWith(basedir) ? elem.Substring(basedir.Length).TrimStart('/') : elem)));
                foundMatch = true;
            }

            if (!foundMatch) { Console.WriteLine("No mathcing files or directories found."); return 1; }
            return 0;
        }
    }
}
