﻿using EV3MonoBrickLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV3BrickMagic.Command
{
    [TriggeredByCommand(Commands.StopProgram)]
    class CommandStopProgram : ICommand
    {
        public int Execute(CommandOptions opts)
        {
            /// Stop running programs
            EV3.Brick.StopProgram();
            if (!opts.QuietMode) Console.WriteLine($"Stopped all programs.");
            return 0;
        }
    }
}
