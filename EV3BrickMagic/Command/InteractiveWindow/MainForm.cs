﻿using EV3MonoBrickLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EV3BrickMagic.Command.InteractiveWindow
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            if (!EV3.IsConnected)
                (new ConnectForm()).ShowDialog();

            if (!EV3.IsConnected)
            {
                Application.Exit();
                return;
            }

            label1.Text = "";
            label1.Text += CommandOptions.GetHeaderLine() + "\r\n\r\n";
            Monitor_ReadBaseInfo();
        }

        private void muiMessaging_Click(object sender, EventArgs e)
        {
            (new MessagingForm()).ShowDialog();
        }

        private void muiMotorControl_Click(object sender, EventArgs e)
        {
            (new MotorControlForm()).ShowDialog();
        }

        private void muiFileList_Click(object sender, EventArgs e)
        {
            (new FileListForm()).ShowDialog();
        }

        void Monitor_ReadBaseInfo()
        {
            StringBuilder sb = new StringBuilder();
            lock (EV3.lockBrick)
            {
                sb.AppendLine($"{"Name",-12}{EV3.Brick.GetGetBrickName()}");
                sb.AppendLine($"{"ID",-12}{EV3.Brick.GetID()}");
                sb.AppendLine($"{"Firmware",-12}{EV3.Brick.GetFirwmareVersion()}");
                sb.AppendLine($"{"Hardware",-12}{EV3.Brick.GetHardwareVersion()}");
                sb.AppendLine($"{"Memory free",-12}{EV3.Brick.GetMemoryUsage().free} kB");

                (Single vbatt, Single ibatt, byte battpc) = EV3.Brick.GetBatteryData();
                var sbatt = $"{"Battery",-12}{vbatt,-0:F3} V, {ibatt,-0:F3} A, {battpc} %";
                sb.AppendLine($"{sbatt,-80}");
            }
            label1.Text += sb.ToString();
        }

        private void MainForm_KeyPress(object sender, KeyPressEventArgs e)
        {
        }

        private void MainForm_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.M:
                    muiMotorControl_Click(sender, null);
                    break;
                case Keys.F:
                    muiFileList_Click(sender, null);
                    break;
                case Keys.E:
                    muiMessaging_Click(sender, null);
                    break;
            }
        }
    }
}
