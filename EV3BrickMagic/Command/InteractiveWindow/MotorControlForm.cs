﻿using EV3MonoBrickLib;
using MonoBrick.EV3;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EV3BrickMagic.Command.InteractiveWindow
{
    public partial class MotorControlForm : Form
    {
        public MotorControlForm()
        {
            InitializeComponent();
        }

        private bool IsThreadStopping { get; set; }
        private void MotorControl_Load(object sender, EventArgs e)
        {
            motors = new Motor[][] { new[] { EV3.Brick.MotorA, EV3.Brick.MotorD }, new[] { EV3.Brick.MotorB, EV3.Brick.MotorC } };
            //Monitor_UpdateText();
            //this.ClientSize = this.ClientSize.Height = label2.Location.Y*2 + label2.Height;

            if (!EV3.IsConnected) return;
            var thread = new Thread(() =>
            {
                while (!IsThreadStopping)
                {
                    try
                    {
                        Thread.Sleep(100);
                        Monitor_UpdateText();
                    }
                    catch { }
                }
            });
            thread.IsBackground = true;
            thread.Start();
        }

        Motor[][] motors = null;
        sbyte[][] motorslast = { new sbyte[] { 0, 0 }, new sbyte[] { 0, 0 } };
        bool[][] motorslastbrake = { new bool[] { false, false }, new bool[] { false, false } };

        private void Monitor_UpdateText()
        {
            //return;
            StringBuilder sb = new StringBuilder();

            SensorType[] st; bool changed;
            lock (EV3.lockBrick)
                (st, changed) = EV3.Brick.GetSensorTypes();
            //if (changed) { EV3.Brick.Sensors.ToList().ForEach(elem => elem.Value.Initialize()); }
            Label[] slabels = new Label[] { label4, label5, label6, label7 };
            foreach (var sensor1 in EV3.Brick.Sensors)
            {
                string stext;
                lock (EV3.lockBrick)
                {
                    if (st[(int)sensor1.Key] != sensor1.Value.SensorType) sensor1.Value.Initialize();
                    stext = $"{sensor1.Key,-12}{sensor1.Value.SensorType.ToString() + "." + sensor1.Value.GetSensorModeString(),-30}";
                    //+$"({sensor1.Value.SensorInfo.Minvalue}-{sensor1.Value.SensorInfo.Maxvalue})"
                    string sensorValue = Monitor_GetSensorValue(EV3.Brick.Sensors[sensor1.Key], slabels[(int)sensor1.Key]);
                    stext += $" Value: {sensorValue,-30}";
                }
                var label = slabels[(int)sensor1.Key];
                label.Invoke(new Action(() => label.Text = stext));
            }
            Label[] mlabels = new Label[] { label2, label8, label9, label10 };
            foreach (var motor in EV3.Brick.Motors)
            {
                string stext;
                lock (EV3.lockBrick)
                {
                    var idx = motors[0].Contains(motor.Value) ? 0 : 1;
                    var idxmi = motors[idx][0] == motor.Value ? 0 : 1;
                    //motorslastbrake[idxMotorControl][]
                    string selectedMotorStr = motorslastbrake[idx][idxmi] ? "*" : " ";
                    stext = ($"{motor.Key.ToString() + selectedMotorStr,-12}{motor.Value.GetName(),-30}");
                    stext += ($" Value: {motor.Value.GetTachoCount().ToString() + " / " + motor.Value.GetSpeed().ToString(),-30}");
                }
                var label = mlabels[(int)motor.Key];
                label.Invoke(new Action(() => label.Text = stext));
            }
        }

        /// <summary>
        /// Sensor value to string conversion
        /// </summary>
        /// <param name="sensor1"></param>
        /// <returns></returns>
        private string Monitor_GetSensorValue(Sensor sensor1, Label label)
        {
            // http://docs.ev3dev.org/projects/lego-linux-drivers/en/ev3dev-stretch/sensor_data.html#lego-ev3-color
            switch (sensor1.GetSensorType())
            {
                case SensorType.Color:
                    switch ((ColorMode)sensor1.GetSensorMode())
                    {
                        case ColorMode.Color:
                            {
                                EV3Color colev3 = sensor1.Color_ReadColor();
                                Color color = EV3ColorToColor[colev3];
                                label.BackColor = color;
                                label.ForeColor = (color.GetBrightness() < 0.5) ? Color.White : Color.Black;
                                return colev3.ToString();
                            }
                            break;
                        case ColorMode.RGBRaw:
                            {
                                int[] rgb = sensor1.GetRaw(3);
                                const double COLOR_RGB_SENSROR_GAIN = 2.0; // color sensor led is quite weak, will not get above 480-500
                                int[] rgbscaled = rgb.Select(elem => (int)Math.Round(Math.Min(255, (double)elem * 256.0 * COLOR_RGB_SENSROR_GAIN / 1023.0))).ToArray();
                                Color color = Color.FromArgb(rgbscaled[0], rgbscaled[1], rgbscaled[2]);
                                label.BackColor = color;
                                label.ForeColor = (color.GetBrightness() < 0.5) ? Color.White : Color.Black;
                                return sensor1.ReadAsString();
                            }
                            break;
                    }
                    label.ForeColor = Color.Black;
                    break;
                case SensorType.Gyro:
                    switch ((GyroMode)sensor1.GetSensorMode())
                    {
                        case GyroMode.AngleAndRotaSpeed:
                            int[] rgb = sensor1.GetRaw(2);
                            return $"{rgb[0]},{rgb[1]}";
                    }
                    break;
            }

            return sensor1.ReadAsString();
        }

        /// <summary>
        /// EV3Color to Color
        /// </summary>
        static readonly Dictionary<EV3Color, Color> EV3ColorToColor = new Dictionary<EV3Color, Color>()
        {
            [EV3Color.None] = SystemColors.Control,
            [EV3Color.Black] = Color.Black,
            [EV3Color.Blue] = Color.Blue,
            [EV3Color.Green] = Color.Green,
            [EV3Color.Yellow] = Color.Yellow,
            [EV3Color.Red] = Color.Red,
            [EV3Color.White] = Color.White,
            [EV3Color.Brown] = Color.Brown
        };

        void Monitor__SetMotorSpeed(int mainmi, int mi, sbyte motorspeed_new, bool brake_new)
        {

            if ((motorslast[mainmi][mi] != motorspeed_new) ||
                (motorslastbrake[mainmi][mi] != brake_new))
            {
                motorslast[mainmi][mi] = motorspeed_new;
                lock (EV3.lockBrick)
                {
                    //Thread.Sleep(10);
                    motors[mainmi][mi].Off();
                    if (motorspeed_new != 0)
                        motors[mainmi][mi].On(motorspeed_new);
                    Thread.Sleep(10);
                }
                //Thread.Sleep(10);
            }
            if (motorslastbrake[mainmi][mi] != brake_new)
            {
                motorslastbrake[mainmi][mi] = brake_new;
                if (brake_new)
                    lock (EV3.lockBrick)
                    {
                        motors[mainmi][mi].Brake();
                        //else
                        //    motors[mainmi][mi].Off();
                        Thread.Sleep(10);
                    }
            }
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            var basekey = keyData & Keys.KeyCode;
            var isShift = (keyData & Keys.Shift) != 0;
            var isControl = (keyData & Keys.Control) != 0;
            sbyte speed = !isShift ?
                        (isControl ?
                          (sbyte)10 :
                              (sbyte)30) :
                                  (sbyte)100;
            switch (basekey)
            {
                case Keys.Down:
                case Keys.Up:
                    if (basekey == Keys.Down) speed = (sbyte)(-speed);
                    Monitor__SetMotorSpeed(0, 0, speed, false);
                    return true;
                case Keys.Right:
                case Keys.Left:
                    if (basekey == Keys.Right) speed = (sbyte)(-speed);
                    Monitor__SetMotorSpeed(0, 1, speed, false);
                    return true;
                case Keys.S:
                case Keys.W:
                    if (basekey == Keys.S) speed = (sbyte)(-speed);
                    Monitor__SetMotorSpeed(1, 0, speed, false);
                    return true;
                case Keys.A:
                case Keys.D:
                    if (basekey == Keys.D) speed = (sbyte)(-speed);
                    Monitor__SetMotorSpeed(1, 1, speed, false);
                    return true;
                case Keys.ControlKey:
                case Keys.ShiftKey:
                    if (motorslast[0][0] != 0) Monitor__SetMotorSpeed(0, 0, (sbyte)(speed * Math.Sign(motorslast[0][0])), false);
                    if (motorslast[0][1] != 0) Monitor__SetMotorSpeed(0, 1, (sbyte)(speed * Math.Sign(motorslast[0][1])), false);
                    if (motorslast[1][0] != 0) Monitor__SetMotorSpeed(1, 0, (sbyte)(speed * Math.Sign(motorslast[1][0])), false);
                    if (motorslast[1][1] != 0) Monitor__SetMotorSpeed(1, 1, (sbyte)(speed * Math.Sign(motorslast[1][1])), false);
                    return true;
                case Keys.Menu:
                    if (motorslast[0][0] == 0) Monitor__SetMotorSpeed(0, 0, 0, true);
                    if (motorslast[0][1] == 0) Monitor__SetMotorSpeed(0, 1, 0, true);
                    if (motorslast[1][0] == 0) Monitor__SetMotorSpeed(1, 0, 0, true);
                    if (motorslast[1][1] == 0) Monitor__SetMotorSpeed(1, 1, 0, true);
                    return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Up:
                case Keys.Down:
                    Monitor__SetMotorSpeed(0, 0, 0, e.Alt);
                    break;
                case Keys.Left:
                case Keys.Right:
                    Monitor__SetMotorSpeed(0, 1, 0, e.Alt);
                    break;
                case Keys.W:
                case Keys.S:
                    Monitor__SetMotorSpeed(1, 0, 0, e.Alt);
                    break;
                case Keys.A:
                case Keys.D:
                    Monitor__SetMotorSpeed(1, 1, 0, e.Alt);
                    break;
                case Keys.ShiftKey:
                case Keys.ControlKey:
                    var isShift = e.Shift;
                    var isControl = e.Control;
                    sbyte speed = !isShift ?
                                (isControl ?
                                  (sbyte)10 :
                                      (sbyte)30) :
                                          (sbyte)100;
                    if (motorslast[0][0] != 0) Monitor__SetMotorSpeed(0, 0, (sbyte)(speed * Math.Sign(motorslast[0][0])), false);
                    if (motorslast[0][1] != 0) Monitor__SetMotorSpeed(0, 1, (sbyte)(speed * Math.Sign(motorslast[0][1])), false);
                    if (motorslast[1][0] != 0) Monitor__SetMotorSpeed(1, 0, (sbyte)(speed * Math.Sign(motorslast[1][0])), false);
                    if (motorslast[1][1] != 0) Monitor__SetMotorSpeed(1, 1, (sbyte)(speed * Math.Sign(motorslast[1][1])), false);
                    break;
                case Keys.Menu:
                    if (motorslast[0][0] == 0) Monitor__SetMotorSpeed(0, 0, 0, e.Alt);
                    if (motorslast[0][1] == 0) Monitor__SetMotorSpeed(0, 1, 0, e.Alt);
                    if (motorslast[1][0] == 0) Monitor__SetMotorSpeed(1, 0, 0, e.Alt);
                    if (motorslast[1][1] == 0) Monitor__SetMotorSpeed(1, 1, 0, e.Alt);
                    break;
                case Keys.R:
                    lock (EV3.lockBrick)
                        for (int ma = 0; ma < 2; ma++)
                            for (int mi = 0; mi < 2; mi++)
                            {
                                motors[ma][mi].ResetTacho();
                                Thread.Sleep(50);
                            }
                    break;
                case Keys.D1:
                case Keys.D2:
                case Keys.D3:
                case Keys.D4:
                    {
                        SensorPort port = SensorPort.In1 + (e.KeyCode - Keys.D1);
                        bool sshift = e.Shift;
                        lock (EV3.lockBrick)
                        {
                            var sensor1 = EV3.Brick.Sensors[port];
                            sensor1.SetMode(sensor1.SensorMode + (sshift ? -1 : 1));
                            sensor1.GetSensorMode(); // force update
                        }
                    }
                    break;
            }
        }

        private void MotorControl_FormClosing(object sender, FormClosingEventArgs e)
        {
            IsThreadStopping = true;
            lock (EV3.lockBrick)
            {
                motors[0][0].Off();
                motors[0][1].Off();
                motors[1][0].Off();
                motors[1][1].Off();
            }
        }

        private void MotorControlForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 27)
            {
                Close();
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
