﻿using EV3BrickMagic;
using EV3DecompilerLib.Decompile;
using EV3DecompilerLib.Recognize;
using EV3ModelLib;
using EV3ModelLib.Helper;
using EV3MonoBrickLib;
using MonoBrick.EV3;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EV3BrickMagic.Command.InteractiveWindow
{
    public partial class FileListForm : Form
    {
        public FileListForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            pnlRBFFile_Resize(null, null);

            if (!EV3.IsConnected) EV3.Connect("USB");
        }

        public string get_parent_dir_path(string path)
        {
            // notice that i used two separators windows style "\\" and linux "/" (for bad formed paths)
            // We make sure to remove extra unneeded characters.
            int index = path.Trim('/', '\\').LastIndexOfAny(new char[] { '\\', '/' });

            // now if index is >= 0 that means we have at least one parent directory, otherwise the given path is the root most.
            if (index >= 0)
                return path.Remove(index);
            else
                return "";
        }

        private string currentEV3Folder = null;
        private string currentSysFolder = null;
        private Dictionary<string, string> folderPositionsEV3 = new Dictionary<string, string>();
        private Dictionary<string, string> folderPositionsSys = new Dictionary<string, string>();
        private void UpdateEV3FileList(string folder)
        {
            if (!EV3.IsConnected) return;

            string positionToSearch = null;
            if (currentEV3Folder != folder && folderPositionsEV3.ContainsKey(folder)) { positionToSearch = folderPositionsEV3[folder]; }

            folderPositionsEV3[currentEV3Folder] = GetSelectedItem()?.Name;
            currentEV3Folder = MonoBrick.EV3.FilSystem.AlignProjectFilename(folder);

            lblEV3CurrentPath.Text = currentEV3Folder;
            EV3.Brick.FileSystem.GetFolderInfo(currentEV3Folder, out BrickFolder[] subfolders, out BrickFile[] files);

            var parent = get_parent_dir_path(currentEV3Folder);

            lvEV3Files.BeginUpdate();
            lvEV3Files.Items.Clear();
            ListViewItem selectedItem = null;
            if (parent != "")
            {
                var text = "[" + ".." + "]";
                var item = lvEV3Files.Items.Add(text, 0);
                item.Tag = new BrickFolder(Path.GetFileName(parent.Trim('/')), get_parent_dir_path(parent) + '/');
                if (selectedItem == null) selectedItem = item;
            }
            subfolders.ToList().ForEach(elem =>
            {
                var text = "[" + elem.Name.Trim('/') + "]";
                var item = lvEV3Files.Items.Add(text, 0);
                item.Tag = elem;
                if (selectedItem == null ||
                    positionToSearch == elem.Name) selectedItem = item;
            });
            files.ToList().ForEach(elem =>
            {
                var text = elem.Name;
                var item = lvEV3Files.Items.Add(text);
                item.ImageKey = Path.GetExtension(elem.Name)?.ToLower();
                item.Tag = elem;
                if (selectedItem == null ||
                    positionToSearch == elem.Name) selectedItem = item;
            });
            if (selectedItem != null)
            {
                selectedItem.Selected = true;
                selectedItem.Focused = true;
            }
            lvEV3Files.EndUpdate();
        }

        private void lvEV3Files_DoubleClick(object sender, EventArgs e)
        {
            ExecuteSelectedFileOrDir2();
        }

        private void lvEV3Files_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13) //Keys.Enter)
                lvEV3Files_DoubleClick(sender, null);
        }

        private void lvFiles_SelectedIndexChanged(object sender, EventArgs e)
        {
            picboxRGFImage.Visible = false;
            wbFileContents.Visible = false;
            pnlRBFFile.Visible = false;
            timerProgramRunning.Enabled = false;

            bool isBrickPath = sender == lvEV3Files;
            string filename = null;
            if (isBrickPath)
            {
                var elem = GetSelectedItem();
                if (elem is BrickFile) filename = (elem as BrickFile).FullName;
            }
            else
            {
                filename = GetSysSelectedFileName();
            }

            switch (Path.GetExtension(filename)?.ToLower())
            {
                case BrickFile.EXT_RGF_GRAPHICS:
                    RGFDecode(isBrickPath, filename);
                    break;
                case BrickFile.EXT_RBF_PROGRAM:
                    pnlRBFFile.Visible = true;
                    btnRBFRunProgram.Enabled = isBrickPath;
                    if (isBrickPath) RBFUpdateRunning();

                    m_RBFInMemory = _GetFileContents(isBrickPath, filename);
                    m_RBFInMemoryName = filename;
                    RBFDecodeEV3GFromMemory();
                    break;
                case BrickFile.EXT_RTF_DATAFILE:
                    RTFRead(isBrickPath, filename);
                    break;
                case ".png":
                case ".bmp":
                case ".jpg":
                    if (!isBrickPath)
                    {
                        Bitmap bmp = new Bitmap(filename);
                        picboxRGFImage.Image = bmp;
                        picboxRGFImage.Width = bmp.Width;
                        picboxRGFImage.Height = bmp.Height;
                        picboxRGFImage.Visible = true;
                    }
                    break;
            }
        }

        private void tabPage2_Leave(object sender, EventArgs e)
        {
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            timerProgramRunning.Enabled = false;
            currentEV3Folder = FilSystem.BRICK_PROJECTS_FOLDER0;
            UpdateEV3FileList(currentEV3Folder);
            currentSysFolder = Path.GetDirectoryName(Application.ExecutablePath);
            UpdateSysFileList(currentSysFolder);
            lvEV3Files.Focus();
        }

        private void btnRBFRunProgram_Click(object sender, EventArgs e)
        {
            var file = GetSelectedItem();
            if (file != null && file is BrickFile)
            {
                lock (EV3.lockBrick)
                {
                    EV3.Brick.StartProgram(file.FullName);
                    RBFUpdateRunning();
                }
            }
        }

        private IBrickItem GetSelectedItem()
        {
            if (lvEV3Files.SelectedItems.Count > 0)
            {
                return lvEV3Files.SelectedItems[0].Tag as IBrickItem;
            }
            return null;
        }

        private void timerProgramRunning_Tick(object sender, EventArgs e)
        {
            RBFUpdateRunning();
        }

        private void btnRBFStop_Click(object sender, EventArgs e)
        {
            RBFStopAll();
        }

        byte[] m_RBFInMemory = null;
        string m_RBFInMemoryName = null;
        private void btnRBFDecodeASM_Click(object sender, EventArgs e)
        {
            RBFDecodeASMFromMemory();
        }

        private void RBFUpdateRunning()
        {
            lock (EV3.lockBrick)
            {
                var isRunnign = EV3.Brick.IsProgramRunning();
                pbRBFRunning.Visible = isRunnign;
                btnRBFStop.Visible = isRunnign;
                btnRBFRunProgram.Visible = !isRunnign;
                //btnRBFDecodeASM.Enabled = btnRBFDecodeEV3G.Enabled = !isRunnign;
                timerProgramRunning.Enabled = isRunnign;
            }
        }

        private void RBFStopAll()
        {
            lock (EV3.lockBrick)
                EV3.Brick.StopProgram();
        }

        private void RBFDecodeASMFromMemory()
        {
            var data = m_RBFInMemory;
            var filefullname = m_RBFInMemoryName;
            var bytecode = new Decompiler().BuildByteCodeForData(data, Path.GetFileName(filefullname));
            var recognizedEV3Blocks = new PatternMatcher().RecognizeEV3Calls(bytecode, Path.GetFileName(filefullname)); //just for commenting

            string text = bytecode.DetailedToString(2);

            text = "<pre style='font-size:12px'>" + text + "</pre>";
            wbFileContents.DocumentText = "0";
            wbFileContents.Document.OpenNew(true);
            wbFileContents.Document.Write(text);
            wbFileContents.Refresh();
            wbFileContents.Visible = true;
        }

        private void ExecuteSelectedFileOrDir2()
        {
            if (lvEV3Files.SelectedItems.Count == 0) return;
            IBrickItem elem = GetSelectedItem();
            if (elem != null && elem is BrickFolder)
                UpdateEV3FileList(elem.FullName);
        }

        private async void RGFDecode(bool isBrickPath, string filefullname)
        {
            byte[] data;
            System.Drawing.Bitmap bmp = await Task.Run(async () =>
            {
                data = _GetFileContents(isBrickPath, filefullname);
                var rgfpic = new RGFHelper.RGFBitmap(data);
                return rgfpic.ConvertToBitmap();
            });
            picboxRGFImage.Image = bmp;
            picboxRGFImage.Width = bmp.Width;
            picboxRGFImage.Height = bmp.Height;
            picboxRGFImage.Visible = true;

            //MemoryStream ms = new MemoryStream();
            //bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp); bmp.Dispose();
            //ms.Position = 0;
            //webBrowser2.DocumentStream = ms;
            ////webBrowser2.Refresh();
        }

        private byte[] _GetFileContents(bool isBrickPath, string filefullname)
        {
            byte[] data;
            if (isBrickPath)
            {
                lock (EV3.lockBrick)
                    data = EV3.Brick.FileSystem.ReadFileToByteArray(filefullname);
            }
            else
            {
                data = File.ReadAllBytes(filefullname);
            }

            return data;
        }

        private async void RBFDecodeEV3GFromMemory()
        {
            var data = m_RBFInMemory;
            var filefullname = m_RBFInMemoryName;
            string text = await Task.Run(async () =>
            {
                var bytecode = new Decompiler().BuildByteCodeForData(data, Path.GetFileName(filefullname));
                Node.PrintOptions.TargetColorIndex = PrintOptionsClass.TargetColorEnum.Html;
                var resultdata = new PatternMatcher().RecognizeEV3Calls2(bytecode, Path.GetFileName(filefullname));
                var recognizedEV3BlocksStr = resultdata.Nodes.GetFormattedOutput();
                return recognizedEV3BlocksStr;
            });

            text = "<pre style='font-size:12px'>" + text + "</pre>";
            wbFileContents.DocumentText = "0";
            wbFileContents.Document.OpenNew(true);
            wbFileContents.Document.Write(text);
            wbFileContents.Refresh();
            wbFileContents.Visible = true;
        }

        private async void RTFRead(bool isBrickPath, string filefullname)
        {
            string text = await Task.Run(async () =>
            {
                byte[] data = _GetFileContents(isBrickPath, filefullname);
                return System.Text.Encoding.ASCII.GetString(data);
            });
            text = "<pre style='font-size:12px'>" + text + "</pre>";
            wbFileContents.DocumentText = "0";
            wbFileContents.Document.OpenNew(true);
            wbFileContents.Document.Write(text);
            wbFileContents.Refresh();
            wbFileContents.Visible = true;
        }

        private void btnRBFDecodeEV3G_Click(object sender, EventArgs e)
        {
            RBFDecodeEV3GFromMemory();
        }

        private void pnlRBFFile_Resize(object sender, EventArgs e)
        {
            var btnwidth = (pnlRBFFile.Width - 4 * 8) / 2; // 3;

            btnRBFDecodeEV3G.Width =
            //btnRBFDecodeASM.Width =
            btnRBFStop.Width =
            pbRBFRunning.Width =
            btnRBFRunProgram.Width = btnwidth;

            btnRBFDecodeEV3G.Left = 8; // + btnwidth + 8;

            btnRBFStop.Left =
            pbRBFRunning.Left =
            btnRBFRunProgram.Left = 8 + (btnwidth + 8);// * 2;
        }

        private void txtMSGSend_KeyPress(object sender, KeyPressEventArgs e)
        {
        }

        private void UpdateSysFileList(string root)
        {
            try
            {
                ListViewItem lvi;

                string positionToSearch = Path.GetFileName(GetSysSelectedFileName());
                //if (currentSysFolder != folder && folderPositionsEV3.ContainsKey(folder)) { positionToSearch = folderPositionsEV3[folder]; }

                if (root.CompareTo("") == 0) return;
                DirectoryInfo dir = new DirectoryInfo(root);
                DirectoryInfo[] dirs = dir.GetDirectories();
                FileInfo[] files = dir.GetFiles();

                this.lvSysFiles.Items.Clear();
                this.lblSysCurrentPath.Text = currentSysFolder = root;
                this.lvSysFiles.BeginUpdate();

                ListViewItem selectedItem = null;
                if (dir.Parent != null)
                {
                    //if (selectedItem == null) selectedItem = item;
                    DirectoryInfo di = dir.Parent;
                    lvi = new ListViewItem();
                    lvi.Text = "[..]";
                    lvi.Tag = di;
                    lvi.ImageIndex = 0;
                    this.lvSysFiles.Items.Add(lvi);
                    if (selectedItem == null) selectedItem = lvi;
                }

                foreach (DirectoryInfo di in dirs)
                {
                    lvi = new ListViewItem();
                    lvi.Text = "[" + di.Name + "]";
                    lvi.Tag = di;
                    lvi.ImageIndex = 0;
                    this.lvSysFiles.Items.Add(lvi);
                    if (selectedItem == null ||
                        positionToSearch == di.Name) selectedItem = lvi;
                }
                foreach (FileInfo fi in files)
                {
                    lvi = new ListViewItem();
                    lvi.Text = fi.Name;
                    lvi.Tag = fi;
                    lvi.ImageKey = Path.GetExtension(fi.Name)?.ToLower();
                    this.lvSysFiles.Items.Add(lvi);
                    if (selectedItem == null ||
                        positionToSearch == fi.Name) selectedItem = lvi;
                }
                if (selectedItem != null)
                {
                    selectedItem.Selected = true;
                    selectedItem.Focused = true;
                }
                this.lvSysFiles.EndUpdate();
            }
            catch (System.Exception err)
            {
                MessageBox.Show("Error: " + err.Message);
            }
        }

        private void lvSysFiles_DoubleClick(object sender, EventArgs e)
        {
            ListView lw = (System.Windows.Forms.ListView)sender;
            if (lw.SelectedItems.Count > 0 && lw.SelectedItems[0].Tag is DirectoryInfo)
                UpdateSysFileList((lw.SelectedItems[0].Tag as DirectoryInfo).FullName);
        }

        private void lvSysFiles_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                lvSysFiles_DoubleClick(sender, null);
        }

        private void lvFiles_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F5:
                case Keys.F10:
                case Keys.Tab:
                    e.IsInputKey = true;
                    break;
            }
        }

        void _IterateCopyDirEV3ToSys(string ev3dir, string sysdestdir)
        {
            (BrickFolder[] subfolders, BrickFile[] files) = EV3.Brick.FileSystem.GetFolderInfoHandleError(ev3dir);

            Directory.CreateDirectory(sysdestdir);

            foreach (var file1 in files)
                EV3.Brick.FileSystem.ReadFile(file1.FullName, Path.Combine(sysdestdir, file1.Name));

            foreach (var srcdir1 in subfolders)
                _IterateCopyDirEV3ToSys(srcdir1.FullName, Path.Combine(sysdestdir, srcdir1.Name.Trim('/')));
        }

        private void lvFiles_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F5:
                    {
                        bool isBrickPath = sender == lvEV3Files;
                        CopyFiles(isBrickPath);
                    }
                    break;
                case Keys.Tab:
                    {
                        if (sender == lvEV3Files) lvSysFiles.Focus();
                        if (sender == lvSysFiles) lvEV3Files.Focus();
                    }
                    break;
                case Keys.F10:
                    {
                        ConverBitmapFile();
                    }
                    break;
            }
        }

        private void CopyFiles(bool isBrickPath)
        {
            if (isBrickPath)
            {
                var file1 = lvEV3Files.SelectedItems[0].Tag as IBrickItem;
                if (file1 == null) return;

                var destdir = lblSysCurrentPath.Text;
                if (file1 is BrickFile)
                {
                    EV3.Brick.FileSystem.ReadFile(file1.FullName, Path.Combine(destdir, file1.Name));
                    UpdateSysFileList(currentSysFolder);
                }
                else if (file1 is BrickFolder)
                {
                    _IterateCopyDirEV3ToSys(file1.FullName, Path.Combine(destdir, file1.Name.Trim('/')));
                    UpdateSysFileList(currentSysFolder);
                }
            }
            else if (!isBrickPath)
            {
                var file1 = lvSysFiles.SelectedItems[0].Tag as FileInfo;
                if (file1 == null) return;
                var destdir = lblEV3CurrentPath.Text;
                EV3.Brick.FileSystem.WriteFile(file1.FullName, Path.Combine(destdir, file1.Name).Replace('\\', '/'));
                UpdateEV3FileList(currentEV3Folder);
            }
        }

        private void labelFileCopy_Click(object sender, EventArgs e)
        {
            bool isBrickPath = lvEV3Files.Focused;
            CopyFiles(isBrickPath);
        }

        private void FileListForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 27)
            {
                Close();
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {
            ConverBitmapFile();
        }

        private void ConverBitmapFile()
        {
            // Convert
            var filename = GetSysSelectedFileName();
            switch (Path.GetExtension(filename)?.ToLower())
            {
                case ".png":
                case ".bmp":
                case ".jpg":
                    {
                        var filenamenew = RGFHelper.RGFBitmap.ReadAndSaveFromBitmap(filename, false);
                        UpdateSysFileList(currentSysFolder);
                    }
                    break;
                case BrickFile.EXT_RGF_GRAPHICS:
                    {
                        var rgfbmp = new RGFHelper.RGFBitmap(filename);
                        var bmp = rgfbmp.ConvertToBitmap();
                        var filenamenew = Path.ChangeExtension(filename, ".bmp");
                        bmp.Save(filenamenew);
                        UpdateSysFileList(currentSysFolder);
                    }
                    break;
            }
        }

        private string GetSysSelectedFileName()
        {
            return lvSysFiles.SelectedItems.Count > 0 ?
                            (lvSysFiles.SelectedItems[0].Tag as FileInfo)?.FullName :
                            null;
        }
    }
}
