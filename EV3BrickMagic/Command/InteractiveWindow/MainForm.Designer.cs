﻿namespace EV3BrickMagic.Command.InteractiveWindow
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.muiFileList = new System.Windows.Forms.ToolStripMenuItem();
            this.muiMotorControl = new System.Windows.Forms.ToolStripMenuItem();
            this.muiMessaging = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.MainMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainMenu
            // 
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.muiFileList,
            this.muiMotorControl,
            this.muiMessaging});
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Size = new System.Drawing.Size(784, 24);
            this.MainMenu.TabIndex = 15;
            this.MainMenu.Text = "menuStrip1AA";
            // 
            // muiFileList
            // 
            this.muiFileList.Name = "muiFileList";
            this.muiFileList.Size = new System.Drawing.Size(58, 20);
            this.muiFileList.Text = "&File List";
            this.muiFileList.Click += new System.EventHandler(this.muiFileList_Click);
            // 
            // muiMotorControl
            // 
            this.muiMotorControl.Name = "muiMotorControl";
            this.muiMotorControl.Size = new System.Drawing.Size(143, 20);
            this.muiMotorControl.Text = "&Motor and Port Control";
            this.muiMotorControl.Click += new System.EventHandler(this.muiMotorControl_Click);
            // 
            // muiMessaging
            // 
            this.muiMessaging.Name = "muiMessaging";
            this.muiMessaging.Size = new System.Drawing.Size(76, 20);
            this.muiMessaging.Text = "M&essaging";
            this.muiMessaging.Click += new System.EventHandler(this.muiMessaging_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Image = global::EV3BrickMagic.Properties.Resources.EV3BrickMagicUnicorn_tr;
            this.label1.ImageAlign = System.Drawing.ContentAlignment.BottomRight;
            this.label1.Location = new System.Drawing.Point(12, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(760, 419);
            this.label1.TabIndex = 18;
            this.label1.Text = "label1";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.MainMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "EV3 Brick Magic";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MainForm_KeyPress);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyUp);
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip MainMenu;
        private System.Windows.Forms.ToolStripMenuItem muiMotorControl;
        private System.Windows.Forms.ToolStripMenuItem muiMessaging;
        private System.Windows.Forms.ToolStripMenuItem muiFileList;
        private System.Windows.Forms.Label label1;
    }
}