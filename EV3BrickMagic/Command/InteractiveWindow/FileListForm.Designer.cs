﻿namespace EV3BrickMagic.Command.InteractiveWindow
{
    partial class FileListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FileListForm));
            this.lvSysFiles = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.pnlRBFFile = new System.Windows.Forms.Panel();
            this.btnRBFDecodeEV3G = new System.Windows.Forms.Button();
            this.pbRBFRunning = new System.Windows.Forms.ProgressBar();
            this.btnRBFRunProgram = new System.Windows.Forms.Button();
            this.btnRBFStop = new System.Windows.Forms.Button();
            this.lblSysCurrentPath = new System.Windows.Forms.Label();
            this.lblFileCopy = new System.Windows.Forms.Label();
            this.lblEV3CurrentPath = new System.Windows.Forms.Label();
            this.picboxRGFImage = new System.Windows.Forms.PictureBox();
            this.lvEV3Files = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.wbFileContents = new System.Windows.Forms.WebBrowser();
            this.timerProgramRunning = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.pnlRBFFile.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxRGFImage)).BeginInit();
            this.SuspendLayout();
            // 
            // lvSysFiles
            // 
            this.lvSysFiles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lvSysFiles.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3});
            this.lvSysFiles.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lvSysFiles.HideSelection = false;
            this.lvSysFiles.Location = new System.Drawing.Point(213, 27);
            this.lvSysFiles.Name = "lvSysFiles";
            this.lvSysFiles.Size = new System.Drawing.Size(201, 398);
            this.lvSysFiles.SmallImageList = this.imageList1;
            this.lvSysFiles.TabIndex = 8;
            this.lvSysFiles.UseCompatibleStateImageBehavior = false;
            this.lvSysFiles.View = System.Windows.Forms.View.Details;
            this.lvSysFiles.SelectedIndexChanged += new System.EventHandler(this.lvFiles_SelectedIndexChanged);
            this.lvSysFiles.DoubleClick += new System.EventHandler(this.lvSysFiles_DoubleClick);
            this.lvSysFiles.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lvSysFiles_KeyPress);
            this.lvSysFiles.KeyUp += new System.Windows.Forms.KeyEventHandler(this.lvFiles_KeyUp);
            this.lvSysFiles.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.lvFiles_PreviewKeyDown);
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Name";
            this.columnHeader3.Width = 174;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "folder.png");
            this.imageList1.Images.SetKeyName(1, ".rbf");
            this.imageList1.Images.SetKeyName(2, ".rsf");
            this.imageList1.Images.SetKeyName(3, ".rtf");
            this.imageList1.Images.SetKeyName(4, ".rgf");
            // 
            // pnlRBFFile
            // 
            this.pnlRBFFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlRBFFile.Controls.Add(this.btnRBFDecodeEV3G);
            this.pnlRBFFile.Controls.Add(this.pbRBFRunning);
            this.pnlRBFFile.Controls.Add(this.btnRBFRunProgram);
            this.pnlRBFFile.Controls.Add(this.btnRBFStop);
            this.pnlRBFFile.Location = new System.Drawing.Point(420, 415);
            this.pnlRBFFile.Name = "pnlRBFFile";
            this.pnlRBFFile.Size = new System.Drawing.Size(550, 34);
            this.pnlRBFFile.TabIndex = 7;
            this.pnlRBFFile.Resize += new System.EventHandler(this.pnlRBFFile_Resize);
            // 
            // btnRBFDecodeEV3G
            // 
            this.btnRBFDecodeEV3G.Location = new System.Drawing.Point(3, 3);
            this.btnRBFDecodeEV3G.Name = "btnRBFDecodeEV3G";
            this.btnRBFDecodeEV3G.Size = new System.Drawing.Size(189, 31);
            this.btnRBFDecodeEV3G.TabIndex = 3;
            this.btnRBFDecodeEV3G.Text = "&Decode EV3G";
            this.btnRBFDecodeEV3G.UseVisualStyleBackColor = true;
            this.btnRBFDecodeEV3G.Click += new System.EventHandler(this.btnRBFDecodeEV3G_Click);
            // 
            // pbRBFRunning
            // 
            this.pbRBFRunning.Location = new System.Drawing.Point(200, 28);
            this.pbRBFRunning.Name = "pbRBFRunning";
            this.pbRBFRunning.Size = new System.Drawing.Size(179, 10);
            this.pbRBFRunning.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.pbRBFRunning.TabIndex = 1;
            this.pbRBFRunning.Value = 100;
            this.pbRBFRunning.Visible = false;
            // 
            // btnRBFRunProgram
            // 
            this.btnRBFRunProgram.Location = new System.Drawing.Point(198, 3);
            this.btnRBFRunProgram.Name = "btnRBFRunProgram";
            this.btnRBFRunProgram.Size = new System.Drawing.Size(180, 31);
            this.btnRBFRunProgram.TabIndex = 0;
            this.btnRBFRunProgram.Text = "&Run program";
            this.btnRBFRunProgram.UseVisualStyleBackColor = true;
            this.btnRBFRunProgram.Click += new System.EventHandler(this.btnRBFRunProgram_Click);
            // 
            // btnRBFStop
            // 
            this.btnRBFStop.Location = new System.Drawing.Point(198, 3);
            this.btnRBFStop.Name = "btnRBFStop";
            this.btnRBFStop.Size = new System.Drawing.Size(180, 31);
            this.btnRBFStop.TabIndex = 2;
            this.btnRBFStop.Text = "&Stop Program";
            this.btnRBFStop.UseVisualStyleBackColor = true;
            this.btnRBFStop.Click += new System.EventHandler(this.btnRBFStop_Click);
            // 
            // lblSysCurrentPath
            // 
            this.lblSysCurrentPath.AutoEllipsis = true;
            this.lblSysCurrentPath.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.lblSysCurrentPath.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblSysCurrentPath.Location = new System.Drawing.Point(213, 6);
            this.lblSysCurrentPath.Name = "lblSysCurrentPath";
            this.lblSysCurrentPath.Size = new System.Drawing.Size(201, 18);
            this.lblSysCurrentPath.TabIndex = 5;
            this.lblSysCurrentPath.Text = "lblSysCurrentPath";
            this.lblSysCurrentPath.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFileCopy
            // 
            this.lblFileCopy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblFileCopy.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.lblFileCopy.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblFileCopy.Location = new System.Drawing.Point(7, 428);
            this.lblFileCopy.Name = "lblFileCopy";
            this.lblFileCopy.Size = new System.Drawing.Size(286, 24);
            this.lblFileCopy.TabIndex = 5;
            this.lblFileCopy.Text = "F5 Copy";
            this.lblFileCopy.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblFileCopy.Click += new System.EventHandler(this.labelFileCopy_Click);
            // 
            // lblEV3CurrentPath
            // 
            this.lblEV3CurrentPath.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.lblEV3CurrentPath.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblEV3CurrentPath.Location = new System.Drawing.Point(7, 6);
            this.lblEV3CurrentPath.Name = "lblEV3CurrentPath";
            this.lblEV3CurrentPath.Size = new System.Drawing.Size(200, 18);
            this.lblEV3CurrentPath.TabIndex = 5;
            this.lblEV3CurrentPath.Text = "lblEV3CurrentPath";
            this.lblEV3CurrentPath.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // picboxRGFImage
            // 
            this.picboxRGFImage.Location = new System.Drawing.Point(445, 27);
            this.picboxRGFImage.Name = "picboxRGFImage";
            this.picboxRGFImage.Size = new System.Drawing.Size(330, 186);
            this.picboxRGFImage.TabIndex = 4;
            this.picboxRGFImage.TabStop = false;
            // 
            // lvEV3Files
            // 
            this.lvEV3Files.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lvEV3Files.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.lvEV3Files.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lvEV3Files.HideSelection = false;
            this.lvEV3Files.Location = new System.Drawing.Point(6, 27);
            this.lvEV3Files.Name = "lvEV3Files";
            this.lvEV3Files.Size = new System.Drawing.Size(201, 398);
            this.lvEV3Files.SmallImageList = this.imageList1;
            this.lvEV3Files.TabIndex = 1;
            this.lvEV3Files.UseCompatibleStateImageBehavior = false;
            this.lvEV3Files.View = System.Windows.Forms.View.Details;
            this.lvEV3Files.SelectedIndexChanged += new System.EventHandler(this.lvFiles_SelectedIndexChanged);
            this.lvEV3Files.DoubleClick += new System.EventHandler(this.lvEV3Files_DoubleClick);
            this.lvEV3Files.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lvEV3Files_KeyPress);
            this.lvEV3Files.KeyUp += new System.Windows.Forms.KeyEventHandler(this.lvFiles_KeyUp);
            this.lvEV3Files.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.lvFiles_PreviewKeyDown);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Name";
            this.columnHeader1.Width = 174;
            // 
            // wbFileContents
            // 
            this.wbFileContents.AllowNavigation = false;
            this.wbFileContents.AllowWebBrowserDrop = false;
            this.wbFileContents.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.wbFileContents.Location = new System.Drawing.Point(420, 6);
            this.wbFileContents.MinimumSize = new System.Drawing.Size(20, 20);
            this.wbFileContents.Name = "wbFileContents";
            this.wbFileContents.Size = new System.Drawing.Size(550, 403);
            this.wbFileContents.TabIndex = 11;
            // 
            // timerProgramRunning
            // 
            this.timerProgramRunning.Tick += new System.EventHandler(this.timerProgramRunning_Tick);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(299, 429);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 24);
            this.label1.TabIndex = 12;
            this.label1.Text = "F10 Convert";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // FileListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 461);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lvSysFiles);
            this.Controls.Add(this.pnlRBFFile);
            this.Controls.Add(this.lblSysCurrentPath);
            this.Controls.Add(this.lblFileCopy);
            this.Controls.Add(this.lblEV3CurrentPath);
            this.Controls.Add(this.picboxRGFImage);
            this.Controls.Add(this.lvEV3Files);
            this.Controls.Add(this.wbFileContents);
            this.KeyPreview = true;
            this.Name = "FileListForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "File List";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FileListForm_KeyPress);
            this.pnlRBFFile.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picboxRGFImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ListView lvEV3Files;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.PictureBox picboxRGFImage;
        private System.Windows.Forms.Label lblEV3CurrentPath;
        private System.Windows.Forms.Panel pnlRBFFile;
        private System.Windows.Forms.Button btnRBFRunProgram;
        private System.Windows.Forms.ProgressBar pbRBFRunning;
        private System.Windows.Forms.Timer timerProgramRunning;
        private System.Windows.Forms.Button btnRBFStop;
        private System.Windows.Forms.Button btnRBFDecodeEV3G;
        private System.Windows.Forms.ListView lvSysFiles;
        private System.Windows.Forms.Label lblSysCurrentPath;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.WebBrowser wbFileContents;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Label lblFileCopy;
        private System.Windows.Forms.Label label1;
    }
}

