﻿namespace EV3BrickMagic.Command.InteractiveWindow
{
    partial class MessagingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnMSGSend = new System.Windows.Forms.Button();
            this.txtMSGTitle = new System.Windows.Forms.TextBox();
            this.txtMSGSend = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 43);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Message Text";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Message Title";
            // 
            // btnMSGSend
            // 
            this.btnMSGSend.Location = new System.Drawing.Point(309, 40);
            this.btnMSGSend.Name = "btnMSGSend";
            this.btnMSGSend.Size = new System.Drawing.Size(108, 20);
            this.btnMSGSend.TabIndex = 6;
            this.btnMSGSend.Text = "Send message";
            this.btnMSGSend.UseVisualStyleBackColor = true;
            this.btnMSGSend.Click += new System.EventHandler(this.btnMSGSend_Click);
            // 
            // txtMSGTitle
            // 
            this.txtMSGTitle.Location = new System.Drawing.Point(101, 12);
            this.txtMSGTitle.Name = "txtMSGTitle";
            this.txtMSGTitle.Size = new System.Drawing.Size(202, 20);
            this.txtMSGTitle.TabIndex = 4;
            this.txtMSGTitle.Text = "abc";
            // 
            // txtMSGSend
            // 
            this.txtMSGSend.Location = new System.Drawing.Point(101, 40);
            this.txtMSGSend.Name = "txtMSGSend";
            this.txtMSGSend.Size = new System.Drawing.Size(202, 20);
            this.txtMSGSend.TabIndex = 5;
            this.txtMSGSend.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMSGSend_KeyPress);
            // 
            // MessagingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(426, 70);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnMSGSend);
            this.Controls.Add(this.txtMSGTitle);
            this.Controls.Add(this.txtMSGSend);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Name = "MessagingForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Messaging";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MessagingForm_KeyPress);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnMSGSend;
        private System.Windows.Forms.TextBox txtMSGTitle;
        private System.Windows.Forms.TextBox txtMSGSend;
    }
}