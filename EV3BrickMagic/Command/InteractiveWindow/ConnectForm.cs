﻿using EV3MonoBrickLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EV3BrickMagic.Command.InteractiveWindow
{
    public partial class ConnectForm : Form
    {
        public ConnectForm()
        {
            InitializeComponent();
        }

        private void ConnectForm_Load(object sender, EventArgs e)
        {
        }

        private void ConnectForm_Activated(object sender, EventArgs e)
        {
            var ports = CommandListPorts.GetAllPorts();

            lvBricks.BeginUpdate();
            lvBricks.Items.Clear();
            foreach (var port in ports)
            {
                var lv = lvBricks.Items.Add(port.DeviceName);
                lv.SubItems.Add(port.Port);
                lv.SubItems.Add(port.DeviceSerial);
                lv.Tag = port;
                lv.ImageIndex = port.Port == "USB" ? 0 : 1;
            }
            if (lvBricks.Items.Count > 0) lvBricks.Items[0].Selected = true;
            lvBricks.EndUpdate();
        }

        private void LvBricks_DoubleClick(object sender, EventArgs e)
        {
            BtnConnect_Click(null, null);
        }

        private void BtnConnect_Click(object sender, EventArgs e)
        {
            if (lvBricks.SelectedItems.Count > 0)
            {
                var item = lvBricks.SelectedItems[0];
                var port = (EV3MonoBrickLib.ComHelper.PortInfo)item.Tag;

                try
                {
                    lblStatus.Text = "";
                    EV3.Connect(port.Port, port.DeviceSerial);
                }
                catch (Exception ex)
                {
                    lblStatus.Text = ex.Message;
                }
                if (EV3.IsConnected) this.Close();
            }
        }

        private void LvBricks_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                BtnConnect_Click(null, null);
            }
        }

        private void LvBricks_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            lblStatus.Text = "";
        }
    }
}
