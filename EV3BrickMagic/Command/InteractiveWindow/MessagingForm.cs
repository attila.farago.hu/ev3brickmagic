﻿using EV3MonoBrickLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EV3BrickMagic.Command.InteractiveWindow
{
    public partial class MessagingForm : Form
    {
        public MessagingForm()
        {
            InitializeComponent();
        }

        private void btnMSGSend_Click(object sender, EventArgs e)
        {
            EV3.Brick.Mailbox.Send(txtMSGTitle.Text, txtMSGSend.Text);
        }

        private void txtMSGSend_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnMSGSend.PerformClick();
        }

        private void MessagingForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 27)
            {
                Close();
            }
        }
    }
}
