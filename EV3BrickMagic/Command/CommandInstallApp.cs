﻿using EV3BrickMagic.Helpers;
using EV3MonoBrickLib;
using MonoBrick.EV3;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ICSharpCode.SharpZipLib.Zip;
using System.Text.RegularExpressions;

namespace EV3BrickMagic.Command
{
    [TriggeredByCommand(Commands.InstallAppFromZip)]
    class CommandInstallAppFiles : ICommand
    {
        public int Execute(CommandOptions opts)
        {
            /// Copy files to and from the brick
            ///     extract an app from zip and install it to the brick
            ///     zip: IRDrive/IRDrive.rbf
            ///         mkdir /home/root/lms2012/apps/IRDrive
            ///         copy IRDrive\*.* brick:/home/root/lms2012/apps/IRDrive -q
            var fileSource = opts.FileSub.Source;

            if (Path.GetExtension(fileSource).ToLower() != ".zip")
            {
                Console.WriteLine("Please specify a zip file. Source for installing an app must be a zip file.");
                return 1;
            }


            // open and parse zip file
            using (var zf = new ZipFile(fileSource))
            {
                var zefiles = zf.Cast<ZipEntry>().Where(elem => elem.IsFile).ToList();
                const string patFILENAME = @"[a-zA-z0-9_ ]+";
                Regex re = new Regex("(?<dir>" + patFILENAME + ")" + @"[\\/]" + "(?<file>" + patFILENAME + ")");

                var zeelems = zefiles.Select(elem =>
                {
                    var match = re.Match(elem.Name);
                    return new
                    {
                        dir = match.Success ? match.Groups["dir"].Value : null,
                        file = match.Success ? match.Groups["file"].Value : null,
                        ze = elem
                    };
                }).ToList();

                var dirname = zeelems.First().dir;
                if (zeelems.Count() == 0 || string.IsNullOrEmpty(dirname) || zeelems.Any(elem => elem.dir != dirname))
                {
                    Console.WriteLine(@"Zip file must contain an app directory, and all app files below including rbf, rgf, rsf files.");
                    return 1;
                }

                Console.WriteLine($"Installing app '{dirname}'");

                // create directory
                const string APP_DIRECTORY = @"/home/root/lms2012/apps/";
                var targetdir = APP_DIRECTORY + dirname;
                if (!EV3.Brick.FileSystem.FileExists(targetdir))
                {
                    Console.WriteLine($"    Creating app directory {dirname}");
                    EV3.Brick.FileSystem.CreateDirectory(targetdir);
                }

                // copy files
                foreach (var zefile in zeelems)
                {
                    string targetfile = APP_DIRECTORY + zefile.ze.Name.Replace(@"\", "/");
                    bool doesExist = EV3.Brick.FileSystem.FileExists(targetfile);

                    Console.WriteLine($"    Copying file {zefile.ze.Name} {(doesExist ? "- overwriting" : null)}");
                    EV3.Brick.FileSystem.WriteFile(zf.GetInputStream(zefile.ze), targetfile, (int)zefile.ze.Size);
                }

                Console.WriteLine($"    Success installing app {dirname}, go to EV3 main app menu, under the third tab.");
            }

            return 0;
        }
    }
}
