﻿using EV3MonoBrickLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV3BrickMagic.Command
{
    [TriggeredByCommand(Commands.SendMail)]
    class CommandSendMail : ICommand
    {
        public int Execute(CommandOptions opts)
        {
            // Send message to mailbox
            EV3.Brick.Mailbox.Send(opts.MailboxSub.Name, opts.MailboxSub.Message);
            return 0;
        }
    }
}
