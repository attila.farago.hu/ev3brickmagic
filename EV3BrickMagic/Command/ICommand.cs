﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV3BrickMagic.Command
{
    interface ICommand
    {
        int Execute(CommandOptions opts);
    }
}
