﻿using EV3MonoBrickLib;
using MonoBrick.EV3;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EV3BrickMagic.Command
{
    [TriggeredByCommand(Commands.Info)]
    class CommandInfo : ICommand
    {
        public int Execute(CommandOptions opts)
        {
            /// Display information and add sensor and motor control
            CancellationTokenSource cts = new CancellationTokenSource();

            void _AppendLine(string s) { Console.WriteLine(s); }
            void _AppendText(string s) { Console.Write(s); }

            int BUSYINDICATORIdx = 0;
            Console.TreatControlCAsInput = true;
            Motor[][] motors = { new[] { EV3.Brick.MotorA, EV3.Brick.MotorD }, new[] { EV3.Brick.MotorB, EV3.Brick.MotorC } };
            sbyte[][] motorslast = { new sbyte[] { 0, 0 }, new sbyte[] { 0, 0 } };
            bool[][] motorslastbrake = { new bool[] { false, false }, new bool[] { false, false } };
            int idxMotorControl = 0;
            //bool bMonitorSensorMode = true;
            //int bMonitorSensorXPosition = 0;
            //DateTime tMonitorlastRead = DateTime.Now;

            _AppendLine($"{"Name",-12}{EV3.Brick.GetGetBrickName()}");
            _AppendLine($"{"ID",-12}{EV3.Brick.GetID()}");
            _AppendLine($"{"Firmware",-12}{EV3.Brick.GetFirwmareVersion()}");
            _AppendLine($"{"Hardware",-12}{EV3.Brick.GetHardwareVersion()}");
            _AppendLine($"{"Memory free",-12}{EV3.Brick.GetMemoryUsage().free} kB");

            try
            {
                //-- start task for UI update
                var kbInfoTask = Task.Run(() =>
                {
                    while (true)
                    {
                        var _cursorTopLast = Console.CursorTop;
                        try
                        {
                            lock (EV3.lockBrick)
                            {
                                (Single vbatt, Single ibatt, byte battpc) = EV3.Brick.GetBatteryData();
                                var sbatt = $"{"Battery",-12}{vbatt,-0:F3} V, {ibatt,-0:F3} A, {battpc} %";
                                _AppendLine($"{sbatt,-80}");

                                (SensorType[] st, bool changed) = EV3.Brick.GetSensorTypes();
                                //if (changed) { EV3.Brick.Sensors.ToList().ForEach(elem => elem.Value.Initialize()); }
                                foreach (var sensor1 in EV3.Brick.Sensors)
                                {
                                    if (st[(int)sensor1.Key] != sensor1.Value.SensorType) sensor1.Value.Initialize();

                                    //_AppendText($"{sensor1.Key,-12}{st[(int)sensor1.Key].ToString() + "." + sensor1.Value.GetSensorModeString(),-30}");
                                    _AppendText($"{sensor1.Key,-12}{sensor1.Value.SensorType.ToString() + "." + sensor1.Value.GetSensorModeString(),-30}");
                                    //+$"({sensor1.Value.SensorInfo.Minvalue}-{sensor1.Value.SensorInfo.Maxvalue})"
                                    (string sensorValue, ConsoleColor? color) = GetSensorValue(EV3.Brick.Sensors[sensor1.Key]);
                                    if (color.HasValue) { Console.BackgroundColor = color.Value; if (color.Value >= ConsoleColor.Gray) Console.ForegroundColor = ConsoleColor.Black; }
                                    _AppendLine($" Value: {sensorValue,-30}");
                                    if (color.HasValue) Console.ResetColor();
                                }
                                foreach (var motor in EV3.Brick.Motors)
                                {
                                    var idx = motors[0].Contains(motor.Value) ? 0 : 1;
                                    var idxmi = motors[idx][0] == motor.Value ? 0 : 1;
                                    //motorslastbrake[idxMotorControl][]
                                    string selectedMotorStr = motorslastbrake[idx][idxmi] ? "*" : " ";
                                    selectedMotorStr +=
                                        opts.InfoSub.ContinousUpdate ?
                                            (idx == idxMotorControl ?
                                                    " <<" : "   ") :
                                                    string.Empty;
                                    _AppendText($"{motor.Key.ToString() + selectedMotorStr,-12}{motor.Value.GetName(),-30}");
                                    _AppendLine($" Value: {motor.Value.GetTachoCount().ToString() + " / " + motor.Value.GetSpeed().ToString(),-30}");
                                }
                            }

                            //-- write update text
                            if (!opts.InfoSub.ContinousUpdate) break;

                            //if (bMonitorSensorMode && ((DateTime.Now - tMonitorlastRead).Milliseconds > 10))
                            //{
                            //    var height = 10;
                            //    var top = Console.CursorTop;
                            //    bMonitorSensorXPosition = (bMonitorSensorXPosition + 1) % Console.WindowWidth;

                            //    //var value = EV3.Brick.Sensors[SensorPort.In1].GetRaw();
                            //    for (int y=top; y<=top+height; y++)
                            //    {
                            //        Console.SetCursorPosition(bMonitorSensorXPosition, y);
                            //        Console.Write(". ");
                            //    }
                            //    string s;
                            //    lock (EV3.lockBrick)
                            //    {
                            //        s = EV3.Brick.Sensors[SensorPort.In1].ReadAsString();
                            //        if (s.Contains(" ")) s = s.Substring(0, s.IndexOf(' '));
                            //    }
                            //    if (int.TryParse(s, out _))
                            //    {
                            //        var value = int.Parse(s);
                            //        var max = 100;
                            //        var value1 = (int)Math.Min(max, (double)value * height / max);
                            //        Console.CursorTop = top + (height - value1);
                            //        Console.CursorLeft = bMonitorSensorXPosition;
                            //        Console.Write("o");
                            //    }
                            //    tMonitorlastRead = DateTime.Now;
                            //    Console.CursorTop = top + height;
                            //}

                            Console.WriteLine();
                            string motorsString = $"{motors[idxMotorControl][0].Port.ToString().Last()}+{motors[idxMotorControl][1].Port.ToString().Last()}";
                            Console.WriteLine($"Motor control with [Arrows] speed modified by [Shift], [Control]. [Space] to toggle, [R] to reset, [Alt] to brake.");
                            Console.WriteLine($"Sensor mode change by [1]-[4] with shift modifier.");
                            Console.Write("Updating values from brick. Press [Q] to quit. ");
                            Console.WriteLine(Common.BUSYINDICATOR[BUSYINDICATORIdx]); BUSYINDICATORIdx = (++BUSYINDICATORIdx % Common.BUSYINDICATOR.Length);

                            //if (Console.KeyAvailable) { Console.ReadKey(true); break; }
                            //Thread.Sleep(500);
                            if (!cts.IsCancellationRequested)
                            {
                                Console.CursorVisible = false;
                                Console.SetCursorPosition(0, _cursorTopLast);

                                Thread.Sleep(10);
                            }
                            else
                            {
                                Console.CursorVisible = true;
                                break;
                            }
                        }
                        catch (Exception ex)
                        {
                            // NOOP
                            Console.SetCursorPosition(0, _cursorTopLast);
                        }
                    };
                });

                bool[] keystate_last = new bool[255];
                bool[] keystate_pressed = new bool[255];
                while (true)
                {
                    if (!opts.InfoSub.ContinousUpdate) { break; }

                    //-- MOTOR CONTROL
                    bool _IsVirtualKeyCodePressed(ConsoleKey key) { return _IsVirtualKeyCodePressed2((int)key); }
                    bool _IsVirtualKeyCodePressed2(int key)
                    {
                        //-- check virtual key state
                        var aks = NativeMethods.GetAsyncKeyState(key);
                        var ispressed = ((aks >> 8) & 0x80) != 0;
                        if (ispressed && !keystate_last[key])
                            keystate_pressed[key] = true;
                        keystate_last[key] = ispressed;
                        return ispressed;
                    }
                    bool _WasVirtualKeyPressedConsumed(ConsoleKey key)
                    {
                        //-- check pressed and consume
                        _IsVirtualKeyCodePressed(key);
                        var retval = (keystate_pressed[(int)key]);
                        (keystate_pressed[(int)key]) = false;
                        return retval;
                    }

                    void _SetMotorSpeed(int mi, sbyte motorspeed_new, bool brake_new)
                    {

                        if ((motorslast[idxMotorControl][mi] != motorspeed_new) ||
                            (motorslastbrake[idxMotorControl][mi] != brake_new))
                        {
                            motorslast[idxMotorControl][mi] = motorspeed_new;
                            lock (EV3.lockBrick)
                            {
                                //Thread.Sleep(10);
                                motors[idxMotorControl][mi].Off();
                                if (motorspeed_new != 0)
                                    motors[idxMotorControl][mi].On(motorspeed_new);
                                Thread.Sleep(10);
                            }
                            //Thread.Sleep(10);
                        }
                        if (motorslastbrake[idxMotorControl][mi] != brake_new)
                        {
                            motorslastbrake[idxMotorControl][mi] = brake_new;
                            if (brake_new)
                                lock (EV3.lockBrick)
                                {
                                    motors[idxMotorControl][mi].Brake();
                                    //else
                                    //    motors[idxMotorControl][mi].Off();
                                    Thread.Sleep(10);
                                }
                        }
                    }

                    //-- calculate and set new speed                        
                    var sleft = _IsVirtualKeyCodePressed(ConsoleKey.LeftArrow);
                    var sright = _IsVirtualKeyCodePressed(ConsoleKey.RightArrow);
                    var sup = _IsVirtualKeyCodePressed(ConsoleKey.UpArrow);
                    var sdown = _IsVirtualKeyCodePressed(ConsoleKey.DownArrow);
                    var sshift = _IsVirtualKeyCodePressed2(0x10 /*VK_SHIFT*/);
                    var salt = _IsVirtualKeyCodePressed2(0x12 /*VK_ALT*/);
                    var speed = !sshift ?
                                  (_IsVirtualKeyCodePressed2(0x11 /*VK_CONTROL*/) ?
                                    10 :
                                        30) :
                                            100;

                    for (int mi = 0; mi < 2; mi++)
                    {
                        sbyte motorspeed_new = (mi == 0 ?
                            (sup ? (sbyte)+speed : (sdown ? (sbyte)-speed : (sbyte)0)) :
                            (sright ? (sbyte)+speed : (sleft ? (sbyte)-speed : (sbyte)0))
                            );
                        bool brake_new = motorspeed_new == 0 ? salt : false; //-- apply brake only the non moving motor
                                                                             //if (motorslastbrake[idxMotorControl][mi] != brake_new) Console.WriteLine($"{motors[idxMotorControl][mi].Port} {brake_new}");
                                                                             //if (motorslast[idxMotorControl][mi] != motorspeed_new) Console.WriteLine($"{motors[idxMotorControl][mi].Port} {motorspeed_new}");
                        _SetMotorSpeed(mi, motorspeed_new, brake_new);
                    }

                    //-- space means change of motors
                    if (_WasVirtualKeyPressedConsumed(ConsoleKey.Spacebar))
                    {
                        for (int mi = 0; mi < 2; mi++) _SetMotorSpeed(mi, 0, false);
                        idxMotorControl = 1 - idxMotorControl;
                        Thread.Sleep(50);
                    }

                    //-- reset tacho count
                    if (_WasVirtualKeyPressedConsumed(ConsoleKey.R))
                    {
                        lock (EV3.lockBrick)
                            for (int mi = 0; mi < 2; mi++) { motors[idxMotorControl][mi].ResetTacho(); Thread.Sleep(50); }
                    }

                    ConsoleKey[] keys = { ConsoleKey.D1, ConsoleKey.D2, ConsoleKey.D3, ConsoleKey.D4 };
                    keys.ToList().ForEach(key =>
                                        {
                                            if (_WasVirtualKeyPressedConsumed(key))
                                            {
                                                SensorPort port = SensorPort.In1 + (key - ConsoleKey.D1);
                                                lock (EV3.lockBrick)
                                                {
                                                    var sensor1 = EV3.Brick.Sensors[port];
                                                    sensor1.SetMode(sensor1.SensorMode + (sshift ? -1 : 1));
                                                    sensor1.GetSensorMode(); // force update
                                                }
                                            }
                                        });

                    //-- Cancel
                    if (_WasVirtualKeyPressedConsumed(ConsoleKey.Q) || _WasVirtualKeyPressedConsumed(ConsoleKey.C) || _WasVirtualKeyPressedConsumed(ConsoleKey.Escape))
                        break;

                    Thread.Sleep(10);
                };

                //-- cancel UI update
                cts.Cancel();

                //-- read out keyboard puffer
                while (Console.KeyAvailable) Console.ReadKey(true);
                kbInfoTask.Wait();
            }
            finally
            {
                EV3.Brick.Motors.ToList().ForEach(motor => { motor.Value.Off(); Thread.Sleep(10); });
            }

            return 0;
        }

        /// <summary>
        /// EV3Color to Console Color
        /// </summary>
        static readonly Dictionary<EV3Color, ConsoleColor> EV3ColorToConsoleColor = new Dictionary<EV3Color, ConsoleColor>()
        {
            [EV3Color.None] = ConsoleColor.Black,
            [EV3Color.Black] = ConsoleColor.Black,
            [EV3Color.Blue] = ConsoleColor.Blue,
            [EV3Color.Green] = ConsoleColor.Green,
            [EV3Color.Yellow] = ConsoleColor.Yellow,
            [EV3Color.Red] = ConsoleColor.DarkRed,
            [EV3Color.White] = ConsoleColor.White,
            [EV3Color.Brown] = ConsoleColor.DarkYellow
        };

        /// <summary>
        /// Sensor value to string conversion
        /// </summary>
        /// <param name="sensor1"></param>
        /// <returns></returns>
        private static (string, ConsoleColor?) GetSensorValue(Sensor sensor1)
        {
            // http://docs.ev3dev.org/projects/lego-linux-drivers/en/ev3dev-stretch/sensor_data.html#lego-ev3-color
            switch (sensor1.GetSensorType())
            {
                case SensorType.Color:
                    switch ((ColorMode)sensor1.GetSensorMode())
                    {
                        case ColorMode.Color:
                            {
                                EV3Color colev3 = sensor1.Color_ReadColor();
                                ConsoleColor concolor = EV3ColorToConsoleColor[colev3];
                                return (colev3.ToString(), concolor);
                            }
                            break;
                            //case ColorMode.RGBRaw:
                            //    {
                            //        int[] rgb = sensor1.GetRaw(3);
                            //        //const double COLOR_RGB_SENSROR_GAIN = 2.0; // color sensor led is quite weak, will not get above 480-500
                            //        //int[] rgbscaled = rgb.Select(elem => (int)Math.Round(Math.Min(255, (double)elem * 256.0 * COLOR_RGB_SENSROR_GAIN / 1023.0))).ToArray();
                            //        //var color = Color.FromArgb(rgbscaled[0], rgbscaled[1], rgbscaled[2]);
                            //        //var concolor = color.ToNearestConsoleColor();
                            //        //var colork = color.ToClosestKnownColor();
                            //        return ($"RGB({rgb[0],4},{rgb[1],4},{rgb[2],4})", null);

                            //        /* http://blog.thekitchenscientist.co.uk/2015/02/ev3-python-raw-rgb-values.html
                            //            33, 6, 15 (Black)
                            //            47,104,154 (Blue)
                            //            34, 139, 31 (Green)
                            //            434, 244, 48 (Yellow)
                            //            252, 49, 27 (Red)
                            //            446, 428, 302 (White)
                            //            87, 59, 29 (Brown) */
                            //    }
                            //    break;
                            //case ColorMode.RawReflected:
                            //    {
                            //        int[] rgb = sensor1.GetRaw(2);
                            //        return ($"{rgb[0]},{rgb[1]}", null);
                            //    }
                            //    break;
                    }
                    break;

                case SensorType.Gyro:
                    switch ((GyroMode)sensor1.GetSensorMode())
                    {
                        case GyroMode.AngleAndRotaSpeed:
                            int[] rgb = sensor1.GetRaw(2);
                            return ($"{rgb[0]},{rgb[1]}", null);
                    }
                    break;
            }

            return (sensor1.ReadAsString(), null);
        }
    }
}
