﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV3BrickMagic.Command
{
    [TriggeredByCommand(Commands.Help)]
    class CommandHelp : ICommand
    {
        public int Execute(CommandOptions opts)
        {
            CommandOptions.PrintHelp();
            return 0;
        }
    }
}
