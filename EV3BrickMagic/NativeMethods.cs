﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace EV3BrickMagic
{
    internal class NativeMethods
    {
        [DllImport("USER32.dll")]
        internal static extern short GetAsyncKeyState(int nVirtKey);

        [DllImport("kernel32.dll", SetLastError = true)]
        internal static extern uint GetConsoleProcessList(
           uint[] ProcessList,
           uint ProcessCount
           );

    }
}
