﻿using EV3MonoBrickLib;
using MonoBrick.EV3;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EV3BrickMagic.Command.InteractiveWindow
{
    public partial class MotorControlForm : Form
    {
        public MotorControlForm()
        {
            InitializeComponent();
        }

        private void MotorControl_Load(object sender, EventArgs e)
        {
            motors = new Motor[][] { new[] { EV3.Brick.MotorA, EV3.Brick.MotorD }, new[] { EV3.Brick.MotorB, EV3.Brick.MotorC } };
            Monitor_ReadBaseInfo();
            Monitor_UpdateText();
        }

        Motor[][] motors = null;
        sbyte[][] motorslast = { new sbyte[] { 0, 0 }, new sbyte[] { 0, 0 } };
        bool[][] motorslastbrake = { new bool[] { false, false }, new bool[] { false, false } };
        Object lockBrick = new object();

        void Monitor_ReadBaseInfo()
        {
            StringBuilder sb = new StringBuilder();
            lock (lockBrick)
            {
                sb.AppendLine($"{"Name",-12}{EV3.Brick.GetGetBrickName()}");
                sb.AppendLine($"{"ID",-12}{EV3.Brick.GetID()}");
                sb.AppendLine($"{"Firmware",-12}{EV3.Brick.GetFirwmareVersion()}");
                sb.AppendLine($"{"Hardware",-12}{EV3.Brick.GetHardwareVersion()}");
                sb.AppendLine($"{"Memory free",-12}{EV3.Brick.GetMemoryUsage().free} kB");
            }
            label1.Text = sb.ToString();
        }

        private void Monitor_UpdateText()
        {
            StringBuilder sb = new StringBuilder();
            (Single vbatt, Single ibatt, byte battpc) = EV3.Brick.GetBatteryData();
            var sbatt = $"{"Battery",-12}{vbatt,-0:F3} V, {ibatt,-0:F3} A, {battpc} %";
            sb.AppendLine($"{sbatt,-80}");

            (SensorType[] st, bool changed) = EV3.Brick.GetSensorTypes();
            //if (changed) { EV3.Brick.Sensors.ToList().ForEach(elem => elem.Value.Initialize()); }
            foreach (var sensor1 in EV3.Brick.Sensors)
            {
                if (st[(int)sensor1.Key] != sensor1.Value.SensorType) sensor1.Value.Initialize();

                //sb.AppendText($"{sensor1.Key,-12}{st[(int)sensor1.Key].ToString() + "." + sensor1.Value.GetSensorModeString(),-30}");
                sb.Append($"{sensor1.Key,-12}{sensor1.Value.SensorType.ToString() + "." + sensor1.Value.GetSensorModeString(),-30}");
                //+$"({sensor1.Value.SensorInfo.Minvalue}-{sensor1.Value.SensorInfo.Maxvalue})"
                (string sensorValue, ConsoleColor? color) = Monitor_GetSensorValue(EV3.Brick.Sensors[sensor1.Key]);
                if (color.HasValue) { Console.BackgroundColor = color.Value; if (color.Value >= ConsoleColor.Gray) Console.ForegroundColor = ConsoleColor.Black; }
                sb.AppendLine($" Value: {sensorValue,-30}");
                if (color.HasValue) Console.ResetColor();
            }
            foreach (var motor in EV3.Brick.Motors)
            {
                var idx = motors[0].Contains(motor.Value) ? 0 : 1;
                var idxmi = motors[idx][0] == motor.Value ? 0 : 1;
                //motorslastbrake[idxMotorControl][]
                string selectedMotorStr = motorslastbrake[idx][idxmi] ? "*" : " ";
                sb.Append($"{motor.Key.ToString() + selectedMotorStr,-12}{motor.Value.GetName(),-30}");
                sb.AppendLine($" Value: {motor.Value.GetTachoCount().ToString() + " / " + motor.Value.GetSpeed().ToString(),-30}");
            }

            label2.Text = sb.ToString();
        }

        private static (string, ConsoleColor?) Monitor_GetSensorValue(Sensor sensor1)
        {
            // http://docs.ev3dev.org/projects/lego-linux-drivers/en/ev3dev-stretch/sensor_data.html#lego-ev3-color
            switch (sensor1.GetSensorType())
            {
                case SensorType.Color:
                    switch ((ColorMode)sensor1.GetSensorMode())
                    {
                        case ColorMode.Color:
                            {
                                EV3Color colev3 = sensor1.Color_ReadColor();
                                ConsoleColor? concolor = null; //EV3ColorToConsoleColor[colev3];
                                return (colev3.ToString(), concolor);
                            }
                            break;
                            //case ColorMode.RGBRaw:
                            //    {
                            //        int[] rgb = sensor1.GetRaw(3);
                            //        //const double COLOR_RGB_SENSROR_GAIN = 2.0; // color sensor led is quite weak, will not get above 480-500
                            //        //int[] rgbscaled = rgb.Select(elem => (int)Math.Round(Math.Min(255, (double)elem * 256.0 * COLOR_RGB_SENSROR_GAIN / 1023.0))).ToArray();
                            //        //var color = Color.FromArgb(rgbscaled[0], rgbscaled[1], rgbscaled[2]);
                            //        //var concolor = color.ToNearestConsoleColor();
                            //        //var colork = color.ToClosestKnownColor();
                            //        return ($"RGB({rgb[0],4},{rgb[1],4},{rgb[2],4})", null);

                            //        /* http://blog.thekitchenscientist.co.uk/2015/02/ev3-python-raw-rgb-values.html
                            //            33, 6, 15 (Black)
                            //            47,104,154 (Blue)
                            //            34, 139, 31 (Green)
                            //            434, 244, 48 (Yellow)
                            //            252, 49, 27 (Red)
                            //            446, 428, 302 (White)
                            //            87, 59, 29 (Brown) */
                            //    }
                            //    break;
                            //case ColorMode.RawReflected:
                            //    {
                            //        int[] rgb = sensor1.GetRaw(2);
                            //        return ($"{rgb[0]},{rgb[1]}", null);
                            //    }
                            //    break;
                    }
                    break;

                case SensorType.Gyro:
                    switch ((GyroMode)sensor1.GetSensorMode())
                    {
                        case GyroMode.AngleAndRotaSpeed:
                            int[] rgb = sensor1.GetRaw(2);
                            return ($"{rgb[0]},{rgb[1]}", null);
                    }
                    break;
            }

            return (sensor1.ReadAsString(), null);
        }

        void Monitor__SetMotorSpeed(int mainmi, int mi, sbyte motorspeed_new, bool brake_new)
        {

            if ((motorslast[mainmi][mi] != motorspeed_new) ||
                (motorslastbrake[mainmi][mi] != brake_new))
            {
                motorslast[mainmi][mi] = motorspeed_new;
                lock (lockBrick)
                {
                    //Thread.Sleep(10);
                    motors[mainmi][mi].Off();
                    if (motorspeed_new != 0)
                        motors[mainmi][mi].On(motorspeed_new);
                    Thread.Sleep(10);
                }
                //Thread.Sleep(10);
            }
            if (motorslastbrake[mainmi][mi] != brake_new)
            {
                motorslastbrake[mainmi][mi] = brake_new;
                if (brake_new)
                    lock (lockBrick)
                    {
                        motors[mainmi][mi].Brake();
                        //else
                        //    motors[mainmi][mi].Off();
                        Thread.Sleep(10);
                    }
            }
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            var basekey = keyData & Keys.KeyCode;
            var isShift = (keyData & Keys.Shift) != 0;
            var isControl = (keyData & Keys.Control) != 0;
            sbyte speed = !isShift ?
                        (isControl ?
                          (sbyte)10 :
                              (sbyte)30) :
                                  (sbyte)100;
            switch (basekey)
            {
                case Keys.Down:
                case Keys.Up:
                    if (basekey == Keys.Down) speed = (sbyte)(-speed);
                    Monitor__SetMotorSpeed(0, 0, speed, false);
                    return true;
                case Keys.Right:
                case Keys.Left:
                    if (basekey == Keys.Right) speed = (sbyte)(-speed);
                    Monitor__SetMotorSpeed(0, 1, speed, false);
                    return true;
                case Keys.S:
                case Keys.W:
                    if (basekey == Keys.S) speed = (sbyte)(-speed);
                    Monitor__SetMotorSpeed(1, 0, speed, false);
                    return true;
                case Keys.A:
                case Keys.D:
                    if (basekey == Keys.D) speed = (sbyte)(-speed);
                    Monitor__SetMotorSpeed(1, 1, speed, false);
                    return true;
                case Keys.ControlKey:
                case Keys.ShiftKey:
                    if (motorslast[0][0] != 0) Monitor__SetMotorSpeed(0, 0, (sbyte)(speed * Math.Sign(motorslast[0][0])), false);
                    if (motorslast[0][1] != 0) Monitor__SetMotorSpeed(0, 1, (sbyte)(speed * Math.Sign(motorslast[0][1])), false);
                    if (motorslast[1][0] != 0) Monitor__SetMotorSpeed(1, 0, (sbyte)(speed * Math.Sign(motorslast[1][0])), false);
                    if (motorslast[1][1] != 0) Monitor__SetMotorSpeed(1, 1, (sbyte)(speed * Math.Sign(motorslast[1][1])), false);
                    return true;
                case Keys.Menu:
                    if (motorslast[0][0] == 0) Monitor__SetMotorSpeed(0, 0, 0, true);
                    if (motorslast[0][1] == 0) Monitor__SetMotorSpeed(0, 1, 0, true);
                    if (motorslast[1][0] == 0) Monitor__SetMotorSpeed(1, 0, 0, true);
                    if (motorslast[1][1] == 0) Monitor__SetMotorSpeed(1, 1, 0, true);
                    return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Up:
                case Keys.Down:
                    Monitor__SetMotorSpeed(0, 0, 0, e.Alt);
                    break;
                case Keys.Left:
                case Keys.Right:
                    Monitor__SetMotorSpeed(0, 1, 0, e.Alt);
                    break;
                case Keys.W:
                case Keys.S:
                    Monitor__SetMotorSpeed(1, 0, 0, e.Alt);
                    break;
                case Keys.A:
                case Keys.D:
                    Monitor__SetMotorSpeed(1, 1, 0, e.Alt);
                    break;
                case Keys.ShiftKey:
                case Keys.ControlKey:
                    var isShift = e.Shift;
                    var isControl = e.Control;
                    sbyte speed = !isShift ?
                                (isControl ?
                                  (sbyte)10 :
                                      (sbyte)30) :
                                          (sbyte)100;
                    if (motorslast[0][0] != 0) Monitor__SetMotorSpeed(0, 0, (sbyte)(speed * Math.Sign(motorslast[0][0])), false);
                    if (motorslast[0][1] != 0) Monitor__SetMotorSpeed(0, 1, (sbyte)(speed * Math.Sign(motorslast[0][1])), false);
                    if (motorslast[1][0] != 0) Monitor__SetMotorSpeed(1, 0, (sbyte)(speed * Math.Sign(motorslast[1][0])), false);
                    if (motorslast[1][1] != 0) Monitor__SetMotorSpeed(1, 1, (sbyte)(speed * Math.Sign(motorslast[1][1])), false);
                    break;
                case Keys.Menu:
                    if (motorslast[0][0] == 0) Monitor__SetMotorSpeed(0, 0, 0, e.Alt);
                    if (motorslast[0][1] == 0) Monitor__SetMotorSpeed(0, 1, 0, e.Alt);
                    if (motorslast[1][0] == 0) Monitor__SetMotorSpeed(1, 0, 0, e.Alt);
                    if (motorslast[1][1] == 0) Monitor__SetMotorSpeed(1, 1, 0, e.Alt);
                    break;
            }
        }

        private void MotorControl_FormClosing(object sender, FormClosingEventArgs e)
        {
            lock (lockBrick)
            {
                motors[0][0].Off();
                motors[0][1].Off();
                motors[1][0].Off();
                motors[1][1].Off();
            }
        }

        private void timerMotorUpdate_Tick(object sender, EventArgs e)
        {
            if (!EV3.IsConnected) return;
            Monitor_UpdateText();
        }

        private void MotorControlForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 27)
            {
                Close();
            }
        }
    }
}
