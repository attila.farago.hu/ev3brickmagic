﻿using System.Diagnostics;

namespace EV3TreeVisLib.Position
{
    /// <summary>
    /// Point structure for Node Bounds
    /// </summary>
    [DebuggerDisplay("{X} {Y}")]
    internal class Point
    {
        public Point(double X, double Y)
        {
            this.X = X; this.Y = Y;
        }
        internal double X { get; set; }
        internal double Y { get; set; }
    }
}
