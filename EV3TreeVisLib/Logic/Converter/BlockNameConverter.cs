﻿using System;
using EV3ModelLib;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;

namespace EV3TreeVisLib.Logic.Converter
{
    /// <summary>
    /// Block name converter and shortener
    /// </summary>
    public static partial class BlockNameConverter
    {
        /// <summary>
        /// Block name unescaping
        /// </summary>
        /// <param name="blockName"></param>
        /// <returns></returns>
        public static string UnescapeBlockName(string blockName)
        {
            return new Regex(@"\\(.)").Replace(blockName, "$1");
        }

        internal const string CONST_Display_BlockFamily = "BlockFamily";
        internal const string CONST_Display_IconName = "IconName";

        internal const string CONST_BlockFamily_FORK = Node.BLOCK_Fork;
        internal const string CONST_BlockFamily_CONNECTOR = LogBlock.ExtraBlocks.CONNECTOR;
        internal const string CONST_BlockFamily_EMPTY = Node.BLOCK_Empty;
        internal const string CONST_BlockFamily_FlowControl = "FlowControl";
        internal const string CONST_BlockFamily_Action = "Action";
        internal const string CONST_BlockFamily_Sensor = "Sensor";
        internal const string CONST_BlockFamily_DataOperations = "DataOperations";
        internal const string CONST_BlockFamily_Advanced = "Advanced";
        internal const string CONST_BlockFamily_MyBlocks = "MyBlocks";

        internal const string CONST_BlockTarget_CommentBlock = @"CommentBlock\.vix";
        internal const string CONST_BlockTarget_DataLogMaster = @"X3Placeholder_2A058539\-ED76\-4476\-93FE\-CCE8AA559C5A_DataLogMasterSingle\.vix";
        internal const string CONST_BlockTarget_ConfigurableWhileLoop = @"ConfigurableWhileLoop";
        internal const string CONST_BlockName_Switch_PairedConfigurableMethodCall = @"PairedConfigurableMethodCall";
        internal const string CONST_BlockName_ConfigurableWaitFor = @"ConfigurableWaitFor";

        /// <summary>
        /// Return the display details for a block
        /// </summary>
        /// <param name="blockName"></param>
        /// <returns></returns>
        public static LegoBlockRecord GetBlockDisplayDetails(string blockName)
        {
            blockName = UnescapeAndTruncVIXExtension(blockName);
            if (BlockTranslateMap.ContainsKey(blockName)) return BlockTranslateMap[blockName];
            return new BlockNameConverter.LegoBlockRecord();
        }

        /// <summary>
        /// truncate name to a friendly name
        /// </summary>
        /// <param name="blockName"></param>
        /// <returns></returns>
        internal static string UnescapeAndTruncVIXExtension(string blockName)
        {
            return UnescapeBlockName(blockName)
                .Replace(@".vix", string.Empty)
                .Replace(@".gvi", string.Empty);
        }

        /// <summary>
        /// truncate name to a friendly name
        /// </summary>
        /// <param name="blockName"></param>
        /// <returns></returns>
        public static string TruncName(string blockName)
        {
            return UnescapeBlockName(blockName)
                .Replace(@".vix", string.Empty)
                .Replace(@".gvi", string.Empty)
                .Replace(@".ev3p", string.Empty)
                .Replace(@"X3.Lib:", string.Empty)
                .Replace(@"X3Placeholder_2A058539-ED76-4476-93FE-CCE8AA559C5A_", string.Empty) //MathEquation
                ;
        }

        /// <summary>
        /// translate blockname to friendly name
        /// </summary>
        /// <param name="blockName"></param>
        /// <returns></returns>
        public static string Translate(string blockName)
        {
            BlockNameConverter.LegoBlockRecord blockDisplayDetails = GetBlockDisplayDetails(blockName);
            if (blockDisplayDetails != null && !string.IsNullOrEmpty(blockDisplayDetails.ShortName)) return blockDisplayDetails.ShortName;
            return TruncName(blockName);
        }

        /// <summary>
        /// Add custome my block icons for display
        /// </summary>
        public static void AddDictMyBlockIcons(string filename, string iconfile)
        {
            DictMyBlockIcons.Value[filename] = iconfile;
        }

        internal static ThreadLocal<Dictionary<string, string>> DictMyBlockIcons = new ThreadLocal<Dictionary<string, string>>(() =>
        {
            return new Dictionary<string, string>();
        });
    }
}
