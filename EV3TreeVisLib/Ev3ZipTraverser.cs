﻿using EV3ModelLib;
using EV3TreeVisLib.Helpers;
using EV3TreeVisLib.Logic;
using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml.Linq;

namespace EV3TreeVisLib
{
    /// <summary>
    /// Zip/EV3 file traverser to explore and process all -matching- ev3p program files
    /// </summary>
    public class Ev3ZipTraverser : IProjectConverter
    {
        /// <summary>
        /// Options for Zip Traverser
        /// </summary>
        public class OptionsClass
        {
            public bool HandleAndReportErrors { get; set; } = true;
            public bool DebugFlag { get; set; } = false;
            internal bool IsEV3Desktop { get; set; } = false;
            internal bool ProjectOnly { get; set; } = false; // DOT MODE compatibilty
            public bool SkipProject { get; set; } = false;
        }
        public static ThreadLocal<OptionsClass> Options = new ThreadLocal<OptionsClass>(() =>
        {
            return new OptionsClass();
        });


        internal Ev3FileTraverser Ev3FileTraverser { get; set; }

        protected ZipFile m_zipFile = null;
        protected string m_zipName = null;
        protected Ev3Project m_ev3project = null;

        public ProjectResultData TraverseZip3(Stream zipFileStream, string zipName, OptionsClass options = null)
        {
            TraverseStart(zipFileStream, zipName, ref options);

            try
            {
                //-- traverse zip file
                (Dictionary<string, LogBlock> retBlocks, Ev3Project retProject) = TraverseZip_Internal();

                //-- convert to new Node format
                var retNodes = retBlocks.ToDictionary(elem => elem.Key, elem => NodeConversion.Convert(elem.Value));

                return (retNodes, retProject);
            }
            finally
            {
                // close file
                TraverseEnd();
            }

        }
        public ProjectResultData TraverseZip3(string zipName, OptionsClass options = null)
        {
            using (var fs = File.Open(zipName, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                return TraverseZip3(fs, zipName, options);
            }
        }
        ProjectResultData IProjectConverter.Convert(Stream instream, string filename)
        {
            return TraverseZip3(instream, filename);
        }

        /// <summary>
        /// static ctor
        /// </summary>
        static Ev3ZipTraverser()
        {
            RegisterConversion();
        }
        public static void RegisterConversion()
        {
            ProjectConversion.RegisterConverter(".ev3", typeof(Ev3ZipTraverser));
            ProjectConversion.RegisterConverter(".ev3m", typeof(Ev3ZipTraverser));
        }

        /// <summary>
        /// implementation of zip traversal
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        private (Dictionary<string, LogBlock> retBlocks, Ev3Project retProject) TraverseZip_Internal()
        {
            // extract project
            m_ev3project = Options.Value.SkipProject ? null : ExtractEV3Project();

            // handle zip nodes
            Dictionary<string, LogBlock> retdict = new Dictionary<string, LogBlock>();
            if (!Options.Value.ProjectOnly)
            {
                // iterate through ev3p programs that match the name criteria if any
                List<ZipEntry> zipEntriesAll = m_zipFile.Cast<ZipEntry>()
                    .Where(ze => ze.Name.EndsWith(".ev3p"))
                    .OrderBy(ze => ze.Name)
                    .ToList();
                List<ZipEntry> zipEntries = zipEntriesAll
                    .ToList();
                foreach (ZipEntry zipEntry in zipEntries)
                {
                    // traverse a single program/file and output the node
                    LogBlock lbRoot = ProcessSingleZipEntry(retdict, zipEntry);
                }
            }

            return (retdict, m_ev3project);
        }

        private LogBlock ProcessSingleZipEntry(Dictionary<string, LogBlock> retdict, ZipEntry zipEntry)
        {
            LogBlock lbRoot = TraverseOneZipEntry(zipEntry.Name);
            if (lbRoot == null) return null;

            retdict[zipEntry.Name] = lbRoot;

            // execute metrics calculation 
            return lbRoot;
        }

        /// <summary>
        /// Start the traversing, init all elements
        /// </summary>
        /// <param name="zipFileStream"></param>
        /// <param name="zipName_in"></param>
        /// <param name="options"></param>
        internal void TraverseStart(Stream zipFileStream, string zipName_in, ref OptionsClass options)
        {
            if (Options == null && options == null) options = new OptionsClass();
            if (options != null) Options.Value = options;

            m_zipName = zipName_in;
            Options.Value.IsEV3Desktop = m_zipName.EndsWith("ev3"); //.ev3m is Ipad;
            Ev3FileTraverser = new Ev3FileTraverser();

            TraverseStartOpenZip(zipFileStream);
        }

        /// <summary>
        /// Start zip traversal
        /// </summary>
        /// <param name="zipFileStream"></param>
        internal void TraverseStartOpenZip(Stream zipFileStream)
        {
            // open the zip file and process;
            m_zipFile = new ZipFile(zipFileStream, true);
        }

        /// <summary>
        /// End traversal, close all resources
        /// </summary>
        internal void TraverseEnd()
        {
            if (m_zipFile != null)
            {
                m_zipFile.Close();
                m_zipFile = null;
            }
        }

        /// <summary>
        /// Extract the EV3 Project from the generic project file
        /// </summary>
        /// <returns></returns>
        internal Ev3Project ExtractEV3Project()
        {
            Ev3Project ev3project = null;

            // iterate through MyBlock definitions
            foreach (ZipEntry zipEntry in m_zipFile.Cast<ZipEntry>().Where(ze => ze.Name.EndsWith(".ev3p.mbxml")))
            {
                Stream zipElementStream = m_zipFile.GetInputStream(zipEntry);
                Ev3FileTraverser.TraverseAndAddMyBlockDefinition(zipElementStream, zipEntry.Name.Replace(".mbxml", string.Empty));
            }

            // PROJECT: collect project statistics
            try
            {
                ev3project = new Ev3Project(Path.GetFileName(m_zipName));
                ev3project.ProcessProject(m_zipFile, Options.Value.IsEV3Desktop);
            }
            catch (Exception)
            {
                // if there is an exception - only fail the current file
                if (!Options.Value.HandleAndReportErrors) throw; // re-throw is error handling is disabled
                else ev3project = null;
            }

            return ev3project;
        }

        /// <summary>
        /// Traverse One zip entry by name
        /// </summary>
        /// <param name="entryName"></param>
        /// <returns></returns>
        internal LogBlock TraverseOneZipEntry(string entryName)
        {
            ZipEntry zipEntry = m_zipFile.GetEntry(entryName);
            Stream zipElementStream = m_zipFile.GetInputStream(zipEntry);

            // process the program file
            List<LogBlock> lbLogBlocks = Ev3FileTraverser.TraverseFile(zipElementStream, entryName,
                new Ev3FileTraverser.OptionsClass()
                {
                    HandleAndReportErrors = Options.Value.HandleAndReportErrors,
                    IsEV3Desktop = Options.Value.IsEV3Desktop
                });
            if (lbLogBlocks == null) return null;
            LogBlock lbRoot = lbLogBlocks[0];

            return lbRoot;
        }

        /// <summary>
        /// Project update one zip entry contents with xdoc
        /// </summary>
        /// <param name="zipFile"></param>
        /// <param name="ze"></param>
        /// <param name="xdoc"></param>
        private void Update_ZipEntryWithXDoc(ZipFile zipFile, ZipEntry ze, XDocument xdoc)
        {
            zipFile.BeginUpdate();
            zipFile.Delete(ze.Name);

            using (MemoryStream msEntry = new MemoryStream())
            {
                xdoc.Save(msEntry);

                CustomStaticDataSource sds = new CustomStaticDataSource();
                sds.SetStream(msEntry);
                zipFile.Add(sds, ze.Name);

                zipFile.CommitUpdate();
                //zipFile.IsStreamOwner = false;
                //zipFile.Close();
            }
        }

        /// <summary>
        /// Return data for selected programs
        /// </summary>
        public Dictionary<string, byte[]> GetDataForPrograms(Stream zipFileStream, List<string> selectedProgramNames)
        {
            TraverseStartOpenZip(zipFileStream);
            try
            {
                var result = new Dictionary<string, byte[]>();

                m_zipFile.Cast<ZipEntry>().ToList().ForEach(
                    zipEntry =>
                    {
                        var filename = zipEntry.Name;
                        if (filename.EndsWith(".mbxml")) filename = filename.Replace(".mbxml", string.Empty);

                        if (selectedProgramNames.Contains(filename))
                        {
                            Stream zipElementStream = m_zipFile.GetInputStream(zipEntry);
                            using (var memoryStream = new MemoryStream())
                            {
                                zipElementStream.CopyTo(memoryStream);
                                result[zipEntry.Name] = memoryStream.ToArray();
                            }
                        }
                    });

                return result;
            }
            finally
            {
                TraverseEnd();
            }
        }
    }
}
