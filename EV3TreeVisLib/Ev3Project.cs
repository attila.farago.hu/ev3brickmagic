﻿using EV3ModelLib;
using EV3ModelLib.Helper;
using EV3TreeVisLib.Logic.Converter;
using ICSharpCode.SharpZipLib.Zip;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Wmhelp.XPath2;

namespace EV3TreeVisLib
{
    /// <summary>
    /// Class to process generic EV3 project related statistics and build project tree
    /// </summary>
    public class Ev3Project : Project
    {
        public Ev3Project(string name) : base(name) { }

        /// <summary>
        /// Process the project file and program files and create a tree of dependencies
        /// </summary>
        public Node ProcessProject(ZipFile zf, bool isEV3Desktop = true)
        {
            //Name = zipName;

            ZipEntry ze = zf.GetEntry(isEV3Desktop ? "Project.lvprojx" : "Project.lvproject");
            if (ze == null) return null;
            Stream projectFileStream = zf.GetInputStream(ze);

            //--------------------------
            // explore global variables
            XDocument xdocProject = XDocument.Load(projectFileStream);
            foreach (XElement elem in xdocProject.XPath2SelectElements("//*:NamedGlobalData/*:Datum"))
            {
                var vname = elem.Attribute("Name").Value.ToString();
                var vtype = elem.Attribute("Type").Value.ToString();
                var vtype2 = ProjectVariableDataType_To_ProgramDataType(vtype);
                GlobalVariables[vname] = new GlobalVariable() { DataType = vtype2 };
            }

            //--------------------------
            // explore media files
            foreach (XElement elem in xdocProject.XPath2SelectElements("//*:DefinitionReference[@DocumentTypeIdentifier='NationalInstruments.ExternalFileSupport.Modeling.ExternalFileType']"))
            {
                var name = elem.Attribute("Name").Value.ToString();
                var name1 = BlockNameConverter.UnescapeBlockName(name);

                var ext1 = Path.GetExtension(name1);
                if (ext1.In(Node.EXT_RSF_SOUND, Node.EXT_RGF_GRAPHICS))
                {
                    var mediaFile = new MediaFile();
                    MediaFiles.Add(name1, mediaFile);

                    //TODO: consider not adding binary contents for text/console mode
                    try
                    {
                        if (ext1 == Node.EXT_RGF_GRAPHICS)
                        {
                            ZipEntry ze1 = zf.GetEntry(name1);
                            Stream rgfFileStream = zf.GetInputStream(ze1);
                            var rgfbitmap = new RGFHelper.RGFBitmap(rgfFileStream, ze1.Size);
                            var bmp = rgfbitmap.ConvertToBitmap();
                            var str = $"data:image/bmp;base64,";
                            var converter = new System.Drawing.ImageConverter();
                            var bytes = (byte[])converter.ConvertTo(bmp, typeof(byte[]));
                            str += System.Convert.ToBase64String(bytes);
                            mediaFile.Contents = str;
                        }
                        else if (ext1 == Node.EXT_RSF_SOUND)
                        {
                            ZipEntry ze1 = zf.GetEntry(name1);
                            Stream rsfFileStream = zf.GetInputStream(ze1);
                            var rsf = new EV3ModelLib.Helper.RSFSound(rsfFileStream, ze1.Size);
                            var bytes = rsf.SoundDataRawWav;
                            var str = $"data:audio/wav;base64,";
                            str += System.Convert.ToBase64String(bytes);
                            mediaFile.Contents = str;
                        }
                    }
                    catch
                    {
                        // NOOP
                    }
                }
            }

            //--------------------------
            // Explore dependencies and global variables usage
            foreach (XElement elem in xdocProject
                .XPath2SelectElements("//*:SourceFileReference")
                .OrderBy(elem => elem.Attribute("Name").Value.ToString())
                .ToList())
            {
                var nameattr = elem.Attribute("Name").Value.ToString();
                var blockname_full = BlockNameConverter.UnescapeBlockName(nameattr);
                var blockname = Path.GetFileNameWithoutExtension(blockname_full);
                if (Path.GetExtension(blockname_full) == ".ev3p")
                {
                    //var name2 = Path.GetFileNameWithoutExtension(name1);
                    if (!Dependencies.TryGetValue(blockname, out ProgramItem pri))
                        Dependencies[blockname] = pri = new ProgramItem();
                    //if (!Dependencies.ContainsKey(blockname)) Dependencies[blockname] = new ProgramItem();
                    if (BlockNameConverter.DictMyBlockIcons.Value.ContainsKey(blockname)) //!! full??
                        pri.IconForMyBlock = BlockNameConverter.DictMyBlockIcons.Value[blockname_full];
                    //TODO: no current knowledge about the parameters, not yet processed

                    Stream programFileStream = zf.GetInputStream(zf.GetEntry(blockname_full));
                    XDocument xdocProgram = XDocument.Load(programFileStream);

                    // ev3p files calling in the program
                    xdocProgram.XPath2SelectValues("//*:ConfigurableMethodCall[@Target and contains(@Target,'.ev3p')]/@Target")
                                    .OfType<Wmhelp.XPath2.Value.UntypedAtomic>()
                                    .Select(blockname2 => BlockNameConverter.UnescapeBlockName(blockname2.Value))
                                    .Distinct()
                                    .ToList().ForEach(blockname2a =>
                                        {
                                            blockname2a = Path.GetFileNameWithoutExtension(blockname2a);

                                            //if (!Dependencies.ContainsKey(item2)) Dependencies.Add(item2, new List<string>());
                                            Dependencies[blockname].UsedBy.Add(blockname2a);

                                            //!!Path.GetFileNameWithoutExtension(
                                            //if (!Dependencies_UsedBy.ContainsKey(blockname2a)) Dependencies_UsedBy.Add(blockname2a, new List<string>());
                                            //Dependencies_UsedBy[blockname2a].Add(blockname);
                                        });

                    // variables usage in the ev3p program
                    xdocProgram.XPath2SelectValues("//*:ConfigurableMethodCall[contains(@Target,'Global')]/*:ConfigurableMethodTerminal/*:Terminal[@Id='name']/../@ConfiguredValue")
                        .OfType<Wmhelp.XPath2.Value.UntypedAtomic>()
                        .Select(gvarname => gvarname.Value)
                        .Distinct()
                        .ToList().ForEach(gvarname1 =>
                        {
                            if (!string.IsNullOrEmpty(gvarname1))
                            {
                                // in case a global variable was not defined yet
                                if (!GlobalVariables.ContainsKey(gvarname1))
                                {
                                    // retrieve the data type from the program
                                    var vtype2 = xdocProgram.XPath2SelectOne<XAttribute>("//*:ConfigurableMethodCall[contains(@Target,'Global')]/*:ConfigurableMethodTerminal[@ConfiguredValue='" + gvarname1 + "']/..//*:ConfigurableMethodTerminal/*:Terminal[@Id='valueIn' or @Id='valueOut']/@DataType").Value;
                                    //var vtype2Short = DataWireRegistry.TranslateWireDataTypeToDisplay(vtype2);
                                    GlobalVariables.Add(gvarname1, new GlobalVariable() { DataType = vtype2 });

                                    AddWarning($"Project missing variable {gvarname1}[{vtype2}]", blockname);
                                }
                                GlobalVariables[gvarname1].UsedBy.Add(blockname);
                            }
                        });

                    // get all nodes count
                    var totalnodes = xdocProgram.XPath2SelectOne("count(" + "//" + Ev3FileTraverser.NodeSelectorPatternWithStartAndComments + ")").ToString();
                    Dependencies[blockname].BlockCount = int.Parse(totalnodes);

                    // mediafiles usage / usedby 
                    xdocProgram.XPath2SelectValues(@"//*:ConfigurableMethodCall[@Target='PlaySoundFile\.vix']/*:ConfigurableMethodTerminal/*:Terminal[@Id='Name']/../@ConfiguredValue")
                        .OfType<Wmhelp.XPath2.Value.UntypedAtomic>()
                        .Select(mediafile => mediafile.Value + Node.EXT_RSF_SOUND)
                        .Union(
                            xdocProgram.XPath2SelectValues(@"//*:ConfigurableMethodCall[@Target='DisplayFile\.vix']/*:ConfigurableMethodTerminal/*:Terminal[@Id='Filename']/../@ConfiguredValue")
                            .OfType<Wmhelp.XPath2.Value.UntypedAtomic>()
                            .Select(mediafile => mediafile.Value + Node.EXT_RGF_GRAPHICS)
                        )
                        .Distinct()
                        .ToList().ForEach(mediafile1 =>
                        {
                            if (MediaFiles.ContainsKey(mediafile1))
                            {
                                MediaFiles[mediafile1].UsedBy.Add(blockname);
                            }
                            else
                            {
                                // Warning: a called media is missing
                                string mediaType = GetMediaTypeByExtension(mediafile1);
                                AddWarning($"Project missing {mediaType} '{mediafile1}'", blockname);
                            }
                        });
                }
            }


            //--------------------------
            // get project infos
            foreach (var filename in new string[] { "___CopyrightYear", "___ProjectTitle", "___ProjectDescription" })
            {
                ZipEntry ze1 = zf.GetEntry(filename);
                if (ze1 == null) continue;
                using (StreamReader reader = new StreamReader(zf.GetInputStream(ze1), Encoding.UTF8))
                {
                    var name1 = filename.Replace("___", "");
                    var value = reader.ReadToEnd().Trim().Replace("\r", " ").Replace("\n", " ");
                    ProjectInfo.Add(name1, value);
                }
            }

            //-- process data and create Project Node
            return base.ProcessProject();

            //=================================================================================
            // print
            //Console.WriteLine("GlobalVariables:");
            //GlobalVariables.OrderBy(item => item.Key).ToList().ForEach(item =>
            //    {
            //        Console.Write($"   {item.Key} [{DataWireRegistry.TranslateWireDataTypeToDisplay(item.Value.Item1)}] => ");
            //        Console.Write(string.Join(" | ", item.Value.Item2.ToList().Select(item1 => Path.GetFileNameWithoutExtension(item1)).ToArray()));
            //        Console.WriteLine();
            //    }
            //);
            //Console.WriteLine("MediaFiles:");
            //MediaFiles.OrderBy(item => item).ToList().ForEach(item => Console.WriteLine($"   {item}"));
            //Console.WriteLine("Programs and dependencies:");

            //Printer.Mode = Printer.Modes.Console;
            //LogBlock.PrintLogBlockToConsole(lbRoot, false);

            //dependencies.OrderBy(item => item.Key).ToList().ForEach(item =>
            //{
            //    Console.Write($"   {Path.GetFileNameWithoutExtension(item.Key)}");
            //    if (item.Value.Count > 0)
            //    {
            //        Console.Write(" => ");
            //        Console.Write(string.Join(" | ", item.Value.Select(item2 => Path.GetFileNameWithoutExtension(item2)).ToArray()));
            //    }
            //    Console.WriteLine();
            //});
            //Console.WriteLine("ProjectInfo:");
            //ProjectInfo.OrderBy(item => item.Key).ToList().ForEach(item =>
            //{
            //    Console.WriteLine($"   {item.Key} - {item.Value.Replace("\n", "\n     ")}");
            //});
        }

        ///// <summary>
        ///// Sort elements alphabetically - with project modification mode
        ///// </summary>
        ///// <param name="xdoc"></param>
        //internal Tuple<XDocument, ZipEntry> Update_SortEntriesAlphabetically(ZipFile zf)
        //{
        //    ZipEntry ze = zf.GetEntry("Project.lvprojx");
        //    if (ze == null) return null;
        //    Stream projectFileStream = zf.GetInputStream(ze);
        //    XDocument xdoc = XDocument.Load(projectFileStream);

        //    //-- Sort elements alphabetically (Programs, MyBlocks, Media)
        //    var xproject = xdoc.XPath2SelectElement(@"*:SourceFile/*:Namespace[@Name='Default']/*:Project");
        //    var xtarget = xproject.XPath2SelectElement(@"*:Target[@DocumentTypeIdentifier='VIVMTarget']");
        //    var xProjectReferences = xtarget.XPath2SelectElements(@"*:ProjectReference").ToArray();
        //    var xSourceFileReferences = xtarget.XPath2SelectElements(@"*:SourceFileReference")
        //        .OrderBy(elem => elem.XPath2SelectOne("string(@StoragePath)").ToString()).ToArray();
        //    var xDefinitionReference = xtarget.XPath2SelectElements(@"*:DefinitionReference")
        //        .OrderBy(elem => elem.XPath2SelectOne("string(@Name)").ToString()).ToArray();
        //    var xOthers = xtarget.Elements()
        //        .Except(xProjectReferences).Except(xSourceFileReferences).Except(xDefinitionReference)
        //        .ToArray();
        //    xtarget.Elements().Remove();
        //    xtarget.Add(xProjectReferences);
        //    xtarget.Add(xSourceFileReferences);
        //    xtarget.Add(xDefinitionReference);
        //    xtarget.Add(xOthers);

        //    //-- Sort variables elements alphabetically
        //    var xProjectSettings = xproject.XPath2SelectElement(@"*:ProjectSettings");
        //    var xNamedGlobalData = xProjectSettings.XPath2SelectElement(@"*:NamedGlobalData");
        //    var xDatums = xNamedGlobalData.XPath2SelectElements(@"*:Datum");
        //    xNamedGlobalData.ReplaceNodes(
        //           xDatums.OrderBy(elem => elem.XPath2SelectOne("string(@Name)").ToString()).ToArray()
        //           );

        //    return new Tuple<XDocument, ZipEntry>(xdoc, ze);
        //}

        /// <summary>
        /// Data type mappings
        /// </summary>
        private static readonly Dictionary<string, string> ProjectVariableDataType_To_ProgramDataType_Mapping = new Dictionary<string, string>
        {
            ["X3String"] = "String",
            ["X3Numeric"] = "Single",
            ["X3NumericArray"] = "Single[]",
            ["X3Boolean"] = "Boolean",
            ["X3BooleanArray"] = "Boolean[]",
        };

        /// <summary>
        /// Map data type as .ev3p and .ev3/project naming differ
        /// </summary>
        /// <param name="projectDatatype"></param>
        /// <returns></returns>
        private static string ProjectVariableDataType_To_ProgramDataType(string projectDatatype)
        {
            if (ProjectVariableDataType_To_ProgramDataType_Mapping.ContainsKey(projectDatatype ?? string.Empty))
                return ProjectVariableDataType_To_ProgramDataType_Mapping[projectDatatype];
            return string.Empty;
        }

        /// <summary>
        /// Add warning message
        /// </summary>
        /// <param name="message"></param>
        /// <param name="block"></param>
        protected override void AddWarning(string message, string block)
        {
            base.AddWarning(message, Path.GetFileNameWithoutExtension(block));
        }
    }
}
