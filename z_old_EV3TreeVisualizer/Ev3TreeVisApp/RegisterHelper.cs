﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Security.Principal;

namespace Ev3TreeVisApp
{
    static class RegisterHelper
    {
        /// <summary>
        /// Register EV3TreeVis as a Shell association with UAC elevation if neccessary
        /// </summary>
        /// <param name="args"></param>
        public static void Register(string[] args)
        {
            WindowsPrincipal pricipal = new WindowsPrincipal(WindowsIdentity.GetCurrent());
            bool hasAdministrativeRight = pricipal.IsInRole(WindowsBuiltInRole.Administrator);

            if (!hasAdministrativeRight)
            {
                // runs with the same arguments plus flag mentioning the main action performing
                var info = new ProcessStartInfo(
                    Assembly.GetEntryAssembly().Location,
                    String.Join(" ", args))
                {
                    Verb = "runas", // indicates to elevate privileges
                };

                var process = new Process
                {
                    EnableRaisingEvents = true, // enable WaitForExit()
                    StartInfo = info
                };

                try
                {
                    process.Start();
                    process.WaitForExit(); // sleep calling process thread until evoked process exit
                }
                catch (System.ComponentModel.Win32Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            else
            {
                string fileext = ".ev3";
                Microsoft.Win32.RegistryKey keyext = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(fileext);
                if (keyext == null)
                {
                    Console.WriteLine("Extension " + fileext + " successfully registered for windows shell integration.");
                    keyext = Microsoft.Win32.Registry.ClassesRoot.CreateSubKey(fileext);
                    keyext.SetValue("", "EV3Project");
                }

                Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.ClassesRoot.CreateSubKey(@"EV3Project\shell\open_treevis");
                key.SetValue("", "Open with EV3 Tree Visualizer");
                key.Close();
                key = Microsoft.Win32.Registry.ClassesRoot.CreateSubKey(@"EV3Project\shell\open_treevis\command");
                key.SetValue("", "cmd /c EV3TreeVis.exe \"%1\" && pause");
                key.Close();
                Console.WriteLine("EV3TreeVis successfully registered for windows shell integration for .ev3 files.");
                Console.WriteLine("\nPress any key to continue . . .");
                Console.ReadKey(true);
            }
        }
    }
}
