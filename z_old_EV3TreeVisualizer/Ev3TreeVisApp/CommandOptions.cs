﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using EV3TreeVisLib;

namespace Ev3TreeVisApp
{
    public class CommandOptions
    {
        public string Filename { get; internal set; } = null;
        public string BlockToDisplay { get; internal set; }
        public OutputMode OutputMode { get; internal set; } = OutputMode.Console;
        public bool ListOnly { get; internal set; } = false;
        public bool Register { get; internal set; } = false;
        public bool DisplayHelp { get; internal set; } = false;
        public bool IsTreeForGit { get; internal set; } = false;
        public bool ProjectOnly { get; internal set; } = false;
        public bool DebugFlag { get; internal set; } = false;
        public bool DisplayMetrics { get; internal set; } = false;
        public bool UpdateProjectSortABC { get; internal set; } = false;

        /// <summary>
        /// Print usage
        /// </summary>
        static public void PrintHelp()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(@"
Usage:  Ev3TreeVis EV3-FILE [options] [blockname]");
            Console.ForegroundColor = ConsoleColor.Gray;

            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location);
            Console.WriteLine(
@"EV3 Tree visualization for EV3 Mindstorm programs for console. " + $"{fvi.FileVersion}, {fvi.ProductVersion}" + @"

Options:
    -register         Register EV3TreeVis for windows shell integration for .ev3 files.
    -list             List only programs in the file.
                      This option will skip all contents.
    -metrics          Display program metrics for each program
    -html             Create html file using the flat tree list format.
    -json             JSON format output instead of plain text beside the ev3 file
    -git              Simplify tree display for git friendly format.
    -updatesortabc    Update EV3Project sorting programs, myblocks, media files and variables alphabetically.
    -? | -h | --help  Show help information..

Arguments:
    <blockname>       When specified only the selected block is processed.
                      Extension ev3p can be ommitted.
                      When no block is specified all programs gets displayed.

    CONFIGURING GIT TextConv differences
        git config -e
        [diff ""ev3""]
            textconv = <path>/ev3treevis.exe -git
        :qw

        C:\Program Files(x86)\...\gitattributes
        *.ev3   diff = ev3

    EXAMPLES for Git Diff with textconv
        git diff --textconv HEAD~ HEAD");
        }

        /// <summary>
        /// Create a CommandOptions instance
        /// </summary>
        /// <param name="args"></param>
        public CommandOptions(string[] args)
        {
            if (args.Length == 0)
            {
                // no args given, display help
                this.DisplayHelp = true;
            }

            Dictionary<string, Action> actionsForOptions = new Dictionary<string, Action>()
            {
                ["-json"] = () => this.OutputMode = OutputMode.JSON,
                ["-html"] = () => this.OutputMode = OutputMode.HTML,
                ["-dot"] = () => this.OutputMode = OutputMode.DOT,
                ["-list"] = () =>
                {
                    this.OutputMode = OutputMode.Console;
                    this.ListOnly = true;
                },
                ["-projectonly"] = () => this.ProjectOnly = true,
                ["-updatesortabc"] = () => this.UpdateProjectSortABC = true,                
                ["-metrics"] = () => this.DisplayMetrics = true,
                ["-git"] = () => this.IsTreeForGit = true,
                ["-register"] = () => this.Register = true,
                ["-debug"] = () => this.DebugFlag = true,
                ["--help"] = () => this.DisplayHelp = true,
                ["-help"] = () => this.DisplayHelp = true,
                ["-h"] = () => this.DisplayHelp = true,
                ["-?"] = () => this.DisplayHelp = true,
                ["/?"] = () => this.DisplayHelp = true,
            };

            // process arguments
            foreach (string arg in args)
            {
                if (actionsForOptions.ContainsKey(arg)) actionsForOptions[arg].Invoke();
                else
                {
                    if (arg.StartsWith("-"))
                    {
                        string closestoption = ClosestWord(arg, actionsForOptions.Keys.ToArray());
                        Console.WriteLine($"Option {arg} not found. Perhaps you meant {closestoption}.");
                    }

                    if (this.Filename == null)
                        this.Filename = arg;
                    else
                        this.BlockToDisplay = arg;
                }
            }
        }

        private static string ClosestWord(string word, string[] terms)
        {
            string term = word.ToLower();
            List<string> list = terms.ToList();
            if (list.Contains(term))
                return list.Find(t => t.ToLower() == term);
            else
            {
                int[] counter = new int[terms.Length];
                for (int i = 0; i < terms.Length; i++)
                {
                    for (int x = 0; x < Math.Min(term.Length, terms[i].Length); x++)
                    {
                        int difference = Math.Abs(term[x] - terms[i][x]);
                        counter[i] += difference;
                    }
                }

                int min = counter.Min();
                int index = counter.ToList().FindIndex(t => t == min);
                return terms[index];
            }
        }
    }
}
