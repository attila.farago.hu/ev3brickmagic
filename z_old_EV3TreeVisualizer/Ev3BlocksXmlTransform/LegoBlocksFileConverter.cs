﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Linq;
using Newtonsoft.Json;
using Wmhelp.XPath2;

namespace Ev3BlocksXmlTransform
{
    public class LegoBlocksFileConverter
    {
        public struct LegoBlockRecord
        {
            public string ShortName { get; set; }
            public string Reference { get; set; }
            public string IconName { get; set; }
            public string BlockFamily { get; set; }
        }

        private List<LegoBlockRecord> blockRecords;
        public void ConvertBlocks()
        {
            string filename = @"c:\Program Files (x86)\LEGO Software\LEGO MINDSTORMS Edu EV3\Resources\Blocks\LEGO\blocks.xml";

            blockRecords = new List<LegoBlockRecord>();

            //Processor processor = new Processor();
            //DocumentBuilder docuBuilder = processor.NewDocumentBuilder();
            //XdmNode input = docuBuilder.Build(new Uri(filename));
            XDocument xdoc = XDocument.Load(filename);
            //xpath = processor.NewXPathCompiler();
            //xpath.Caching = true;

            var aPolyGroups = xdoc.XPath2SelectElements("//*:PolyGroup");
            foreach (XElement aPolyGroup in aPolyGroups)
            {
                string sGroupName = aPolyGroup.Attribute("Name").Value;
                string sBlockFamily = aPolyGroup.Attribute("BlockFamily").Value;
                ConvertGroup(aPolyGroup, sGroupName, sGroupName, sBlockFamily);

                var aCategories = aPolyGroup.XPath2SelectElements("*:Category");
                foreach (XElement aCategory in aCategories)
                {
                    var sCategoryName = aCategory.Attribute("Name").Value;
                    ConvertGroup(aCategory, sGroupName, sGroupName + "." + sCategoryName, sBlockFamily);
                }
            }

            StringBuilder sb = new StringBuilder();
            sb.AppendLine(@"
using System;
using System.Collections.Generic;
using System.Linq;

namespace EV3TreeVisLib
{
    public static partial class BlockNameConverter
    {
        public class LegoBlockRecord
        {
            public string ShortName { get; set; }
            public string Reference { get; set; }
            public string IconName { get; set; }
            public string BlockFamily { get; set; }
        }
".Trim());
            sb.Append(@"
        public static Dictionary<string, LegoBlockRecord> BlockTranslateMap = new Dictionary<string, LegoBlockRecord> 
        {");
            foreach (var record in blockRecords)
            {
                sb.Append(string.Format(@"
            [""{1}""] = new LegoBlockRecord 
            {{
                ShortName = ""{0}"",
                Reference = ""{1}"",
                IconName = ""{2}"",
                BlockFamily = ""{3}""
            }},
            ", record.ShortName, record.Reference, record.IconName, record.BlockFamily).TrimEnd());
            }
            sb.AppendLine(@"
        };
    }
}");
            Console.WriteLine(sb.ToString());

            System.IO.File.WriteAllText(@"..\..\..\Ev3TreeVisLib\BlockNameConverter_BlockTranslateMap_Generated.cs",
                sb.ToString());
            //Console.WriteLine(new JavaScriptSerializer().Serialize(blockRecords));
        }

        public void ConvertGroup(XElement aGroup, string sPolyGroup, string sLabel, string sBlockFamily)
        {
            var aBlocks = aGroup.XPath2SelectElements("*:Block").ToList();
            foreach (XElement aBlock in aBlocks)
            {
                var aHardwarePageTreatment = aBlock.XPath2SelectElements("*:hardwarepagetreatment");
                if (aHardwarePageTreatment.Count() > 0) continue;

                string sReference = aBlock.XPath2SelectOne("string(*:Reference/@Name)").ToString();
                string sMode = aBlock.XPath2SelectOne("string(*:Mode)").ToString();
                //"c:\Program Files (x86)\LEGO Software\LEGO MINDSTORMS Edu EV3\Resources\Blocks\LEGO\images"
                //PolyGroup_Math_Mode_Divide_Diagram.png

                if (sReference == "") continue;

                string sShortname = (aBlocks.Count > 1) ? sLabel + "." + sMode : sLabel;
                string sIconName = (aBlocks.Count > 1) ?
                    string.Format("PolyGroup_{0}_Mode_{1}_Diagram.png", sPolyGroup, sMode) :
                    string.Format("PolyGroup_{0}_Diagram.png", sPolyGroup);
                //RedundantDisplayName //TODO:
                //Dictionary<string, string> element = new Dictionary<string, string>
                //{
                //    { "ShortName", sShortname},
                //    { "Reference", sReference},
                //    { "IconName", sIconName},
                //    { "BlockFamily", sBlockFamily}
                //};
                LegoBlockRecord element2 = new LegoBlockRecord()
                {
                    ShortName = sShortname,
                    Reference = sReference,
                    IconName = sIconName,
                    BlockFamily = sBlockFamily
                };
                //Tuple<string, string, string, string> element1 =
                //    Tuple.Create<string, string, string, string>(sShortname, sReference, sIconName, sBlockFamily);
                blockRecords.Add(element2);

                //Console.WriteLine("{{\"{2}\", new Dictionary<string,string>{{ {{\"ShortName\", \"{0}.{1}\"}}, {{\"Name\", \"{5}\"}}, {{\"BlockFamily\",\"{3}\"}}, {{\"IconName\", \"{4}\"}} }} }},",
                //    sLabel, sMode, sReference, sBlockFamily, sIconName, sReference);
            }
        }
    }
}
