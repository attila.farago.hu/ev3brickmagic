﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ev3BlocksXmlTransform
{
    class Program
    {
        static void Main(string[] args)
        {
            new LegoBlocksFileConverter().ConvertBlocks();
        }
    }
}
