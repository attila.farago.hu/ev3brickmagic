﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoBrick.EV3
{
    public class SensorInfo
    {
        public SensorType Type;
        public SensorMode Mode;
        public byte Figures;
        public byte Decimales;
        public float Minvalue;
        public float Maxvalue;
        public byte Connection;
        public byte Datasets;
        public SensorInfoFormat Format;
        public byte Modes;
        public byte Modesview;
        //public string Name;
        public string Symbol;
    }

    public enum SensorInfoFormat
    {
        F8Bit = 0, F16Bit = 1, F32Bit = 2, FFloat = 3
    };
}
