using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace MonoBrick.EV3
{
    public interface IBrickItem
    {
        string FullName { get; }
        string Name { get; }
        string Path { get; }
    }

    /// <summary>
    /// Class for accessing a file on the EV3
    /// </summary>
    public class BrickFile : IBrickItem, IBrickFile, IComparable<BrickFile>
    {
        public const string EXT_RBF_PROGRAM = ".rbf";
        public const string EXT_RSF_SOUND = ".rsf";
        public const string EXT_RGF_GRAPHICS = ".rgf";
        public const string EXT_RDF_DATALOG = ".rdf";
        public const string EXT_RTF_DATAFILE = ".rtf";
        private string fileName;
        private string extension;
        private UInt32 fileSize;
        private FileType ev3FileType;
        public static Dictionary<FileType, string> EXTENSIONS = new Dictionary<FileType, string>
        {
            [FileType.Program] = EXT_RBF_PROGRAM,
            [FileType.Sound] = EXT_RSF_SOUND,
            [FileType.Graphics] = EXT_RGF_GRAPHICS,
            [FileType.Datalog] = EXT_RDF_DATALOG,
            [FileType.Program] = EXT_RBF_PROGRAM,
            [FileType.DataFile] = EXT_RTF_DATAFILE
        };

        public static string GetExtensionByFileType(FileType typ)
        {
            if (EXTENSIONS.ContainsKey(typ)) return EXTENSIONS[typ];
            else return string.Empty;
        }

        private FileType StringToFileType(string extenstion)
        {
            FileType type;
            switch (extenstion.ToLower())
            {
                /*case "":
					type = MonoBrick.FileType.Firmware;
				break;
				case "":
					type = MonoBrick.FileType.OnBrickProgram;
				break;
				case "":
					type = MonoBrick.FileType.TryMeProgram;
				break;*/
                case EXT_RBF_PROGRAM:
                    type = MonoBrick.FileType.Program;
                    break;
                case EXT_RSF_SOUND:
                    type = MonoBrick.FileType.Sound;
                    break;
                case EXT_RGF_GRAPHICS:
                    type = MonoBrick.FileType.Graphics;
                    break;
                case EXT_RDF_DATALOG:
                    type = MonoBrick.FileType.Datalog;
                    break;
                case EXT_RTF_DATAFILE:
                    type = MonoBrick.FileType.DataFile;
                    break;
                default:
                    type = MonoBrick.FileType.Unknown;
                    break;
            }
            return type;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MonoBrick.EV3.BrickFile"/> class.
        /// </summary>
        /// <param name="name">Name of the file.</param>
        /// <param name="size">Size of the file in bytes.</param>
        /// <param name="path">Path to the location of the file.</param>
        public BrickFile(string name, UInt32 size, string path)
        {
            fileName = System.IO.Path.GetFileName(name);
            fileSize = size;
            extension = System.IO.Path.GetExtension(name).ToLower();
            ev3FileType = StringToFileType(extension);
            Path = path;
        }

        /// <summary>
        /// Gets the full name of the file e.i. path+filename
        /// </summary>
        /// <value>The full name.</value>
        public string FullName
        {
            get
            {
                string final = this.Path;
                if (!final.EndsWith("/"))
                {
                    final = final + "/";
                }
                return final + this.Name;
            }

        }

        /// <summary>
        /// Gets the name of the file
        /// </summary>
        /// <value>
        /// The name of the file
        /// </value>
        public string Name
        {
            get { return fileName; }
        }

        /// <summary>
        /// Gets the size
        /// </summary>
        /// <value>
        /// The size in bytes
        /// </value>
        public UInt32 Size
        {
            get { return fileSize; }
        }

        /// <summary>
        /// Gets a value indicating whether the file is empty
        /// </summary>
        /// <value>
        /// <c>true</c> if the file is empty; otherwise, <c>false</c>.
        /// </value>
        public bool IsEmpty
        {
            get { return !Convert.ToBoolean(fileSize); }
        }

        /// <summary>
        /// Gets the file extension.
        /// </summary>
        /// <value>
        /// The file extension.
        /// </value>
        public string Extension
        {
            get { return extension; }
        }

        /// <summary>
        /// Gets the file type
        /// </summary>
        /// <value>
        /// The file type
        /// </value>
        public FileType FileType
        {
            get { return ev3FileType; }
        }

        /// <summary>
        /// Gets the path where the file is located
        /// </summary>
        /// <value>The path.</value>
        public string Path { get; private set; }

        /// <summary>
        /// Compares to.
        /// </summary>
        /// <returns>The to.</returns>
        /// <param name="other">Other.</param>
        public int CompareTo(BrickFile other)
        {
            return this.Name.CompareTo(other.Name);
        }
    }

    public class BrickFolder : IBrickItem, IComparable<BrickFolder>
    {
        public BrickFolder(string name, string path)
        {
            Name = name;
            Path = path;
        }

        /// <summary>
        /// Gets the full name of the file e.i. path+filename
        /// </summary>
        /// <value>The full name.</value>
        public string FullName
        {
            get
            {
                return this.Path != "/" ? this.Path + this.Name : this.Name;
            }

        }

        public string Name { get; private set; }
        public string Path { get; private set; }

        /// <summary>
        /// Compares to.
        /// </summary>
        /// <returns>The to.</returns>
        /// <param name="other">Other.</param>
        public int CompareTo(BrickFolder other)
        {
            return this.Name.CompareTo(other.Name);
        }

    }

    /// <summary>
    /// Class that holds information about a EV3 folder and it's subfolders.
    /// </summary>
    public class FolderStructure : IComparable<FolderStructure>
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="MonoBrick.EV3.FolderStructure"/> class.
        /// </summary>
        /// <param name="path">Path of the folder.</param>
        /// <param name="isBrowsable">If set to <c>true</c> the folder is browsable.</param>
        public FolderStructure(string path, bool isBrowsable)
        {
            Path = path;
            FileList = new List<BrickFile>();
            FolderList = new List<FolderStructure>();
            IsBrowsable = isBrowsable;
        }

        /// <summary>
        /// Runs recursively through the folders structure.
        /// </summary>
        /// <returns>The next (sub)folder in the structure.</returns>
        public IEnumerable<FolderStructure> RunThroughFolders()
        {
            return RecursiveRunThrough(this);
        }

        /// <summary>
        /// Recursives run through the folder structure.
        /// </summary>
        /// <returns>A structure enumeable</returns>
        /// <param name="structure">Structure.</param>
        private IEnumerable<FolderStructure> RecursiveRunThrough(FolderStructure structure)
        {
            yield return structure;
            foreach (var child in structure.FolderList)
            {
                foreach (var f in RecursiveRunThrough(child))
                {
                    yield return f;
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether folder is browsable.
        /// </summary>
        /// <value><c>true</c> if this folder is readable; otherwise, <c>false</c>.</value>
        public bool IsBrowsable { get; private set; }

        /// <summary>
        /// Gets the path.
        /// </summary>
        /// <value>The path.</value>
        public string Path { get; private set; }

        /// <summary>
        /// Gets the file list.
        /// </summary>
        /// <value>The file list.</value>
        public List<BrickFile> FileList { get; private set; }

        /// <summary>
        /// Gets the sub folders.
        /// </summary>
        /// <value>The sub folders.</value>
        public List<FolderStructure> FolderList { get; private set; }

        /// <Docs>To be added.</Docs>
        /// <para>Returns the sort order of the current instance compared to the specified object.</para>
        /// <summary>
        /// Compares to.
        /// </summary>
        /// <returns>The to.</returns>
        /// <param name="other">Other.</param>
        public int CompareTo(FolderStructure other)
        {
            return this.Path.CompareTo(other.Path);
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents the current <see cref="MonoBrick.EV3.FolderStructure"/>.
        /// </summary>
        /// <returns>A the path as a string.</returns>
        public override string ToString()
        {
            return this.Path;
        }

    }

    /// <summary>
    /// Fil system for the EV3 brick
    /// </summary>
    public class FilSystem
    {
        public const string BRICK_PROJECTS_FOLDER0 = "../prjs";
        public const string BRICK_PROJECTS_FOLDER = BRICK_PROJECTS_FOLDER0 + "/";

        private const int MaxBytesToWrite = 50;
        private const int MaxBytestoRead = 100;
        //private const UInt32 MaxBytesInFileList = 1000;
        private Connection<Command, Reply> connection;
        internal Connection<Command, Reply> Connection
        {
            get { return connection; }
            set { connection = value; }
        }

        /// <summary>
        /// Begins the download from you to the EV3
        /// </summary>
        /// <returns>The download.</returns>
        /// <param name="fileLength">File length.</param>
        /// <param name="filename">Filename relative to /home/root/lms2012/sys</param>
        private byte BeginWrite(UInt32 fileLength, string filename)
        {
            var command = new Command(SystemCommand.BeginDownload, 100, true);
            command.Append(fileLength);
            command.Append(filename);
            var reply = connection.SendAndReceive(command);
            Error.CheckForError(reply, 6, 100);
            return reply.GetByte(5);
        }

        /// <summary>
        /// Continues writing files.
        /// </summary>
        /// <param name="handle">Handle.</param>
        /// <param name="data">Data.</param>
        /// <param name="startIdx">Start index.</param>
        /// <param name="length">Length.</param>
        private void ContinueWrite(byte handle, byte[] data, int startIdx, int length)
        {
            var command = new Command(SystemCommand.ContinueDownload, 101, true);
            command.Append(handle);
            command.Append(data, startIdx, length);
            var reply = connection.SendAndReceive(command);
            if (reply.HasError)
            {
                Error.ThrowException(reply);
            }

        }

        private void WriteCompleteArray(string fileName, byte[] data)
        {
            byte handle = 0;
            int bytesWritten = 0;
            int bytesToWrite = data.Length;
            try
            {
                handle = BeginWrite((UInt32)data.Length, fileName);
                while (bytesWritten < bytesToWrite)
                {
                    if ((bytesToWrite - bytesWritten) >= MaxBytesToWrite)
                    {
                        ContinueWrite(handle, data, bytesWritten, MaxBytesToWrite);
                        bytesWritten += MaxBytesToWrite;
                    }
                    else
                    {
                        ContinueWrite(handle, data, bytesWritten, (bytesToWrite - bytesWritten));
                        bytesWritten = bytesToWrite + (bytesToWrite - bytesWritten);
                    }
                }
            }
            catch (BrickException)
            {
                //CloseFile (handle);
                throw;
            }
            // closefile seems to be needed // Added by Attila
            //if (handle > 0) CloseFile(handle);
        }

        private byte[] BeginRead(UInt16 bytesToRead, string filePath, out byte handle, out UInt32 fileSize)
        {
            var command = new Command(SystemCommand.BeginUpload, 102, true);
            command.Append(bytesToRead);
            command.Append(filePath);
            var reply = connection.SendAndReceive(command);
            Error.CheckForError(reply, 102);
            fileSize = reply.GetUInt32(5);
            handle = reply.GetByte(9);
            return reply.GetData(10);
        }

        private byte[] ContinueRead(byte handle, UInt16 bytesToRead)
        {
            var command = new Command(SystemCommand.ContinueUpload, 103, true);
            command.Append(handle);
            command.Append(bytesToRead);
            var reply = connection.SendAndReceive(command);
            Error.CheckForError(reply, bytesToRead + 6, 103);
            if (handle != reply[5])
            {
                throw new BrickException(BrickError.UnknownHandle);
            }
            return reply.GetData(6);
        }

        private byte[] ReadWholeFile(string fileName)
        {
            UInt32 bytesRead = 0;
            byte[] fileData = null;
            byte[] dataRead = null;
            byte handle = 0;
            UInt32 fileSize = 0;
            dataRead = BeginRead(MaxBytestoRead, fileName, out handle, out fileSize);
            fileData = new byte[fileSize];
            Array.Copy(dataRead, fileData, dataRead.Length);
            bytesRead = (UInt32)dataRead.Length;
            while (bytesRead < fileSize)
            {
                if ((fileSize - bytesRead) >= MaxBytestoRead)
                {
                    dataRead = ContinueRead(handle, MaxBytestoRead);
                    Array.Copy(dataRead, 0, fileData, bytesRead, dataRead.Length);
                    bytesRead = bytesRead + MaxBytestoRead;
                }
                else
                {
                    dataRead = ContinueRead(handle, (UInt16)(fileSize - bytesRead));
                    Array.Copy(dataRead, 0, fileData, bytesRead, dataRead.Length);
                    bytesRead = bytesRead + (fileSize - bytesRead);
                }
            }
            return fileData;
        }

        //private byte[] BeginGetFile(UInt16 bytesToRead, string filePath, out byte handle, out UInt32 fileSize)
        //{
        //    var command = new Command(SystemCommand.BeginGetFile, 102, true);
        //    command.Append(bytesToRead);
        //    command.Append(filePath);
        //    var reply = connection.SendAndReceive(command);
        //    Error.CheckForError(reply, 102);
        //    fileSize = reply.GetUInt32(5);
        //    handle = reply.GetByte(9);
        //    return reply.GetData(10);
        //}

        //private byte[] ContinueGetFile(byte handle, UInt16 bytesToRead)
        //{
        //    var command = new Command(SystemCommand.ContinueUpload, 103, true);
        //    command.Append(handle);
        //    command.Append(bytesToRead);
        //    var reply = connection.SendAndReceive(command);
        //    Error.CheckForError(reply, bytesToRead + 6, 103);
        //    var handle_this = reply[9];
        //    if (handle != handle_this)
        //    {
        //        throw new BrickException(BrickError.UnknownHandle);
        //    }
        //    var fileSize = reply.GetUInt32(5);
        //    return reply.GetData(10);
        //}

        //private byte[] GetWholeFile(string fileName)
        //{
        //    UInt32 bytesRead = 0;
        //    byte[] fileData = null;
        //    byte[] dataRead = null;
        //    byte handle = 0;
        //    UInt32 fileSize = 0;
        //    dataRead = BeginGetFile(MaxBytestoRead, fileName, out handle, out fileSize);
        //    fileData = new byte[fileSize];
        //    Array.Copy(dataRead, fileData, dataRead.Length);
        //    bytesRead = (UInt32)dataRead.Length;
        //    while (bytesRead < fileSize)
        //    {
        //        if ((fileSize - bytesRead) >= MaxBytestoRead)
        //        {
        //            dataRead = ContinueGetFile(handle, MaxBytestoRead);
        //            Array.Copy(dataRead, 0, fileData, bytesRead, dataRead.Length);
        //            bytesRead = bytesRead + MaxBytestoRead;
        //        }
        //        else
        //        {
        //            dataRead = ContinueGetFile(handle, (UInt16)(fileSize - bytesRead));
        //            Array.Copy(dataRead, 0, fileData, bytesRead, dataRead.Length);
        //            bytesRead = bytesRead + (fileSize - bytesRead);
        //        }
        //    }
        //    return fileData;
        //}

        private void CloseFile(byte handle)
        {
            var command = new Command(SystemCommand.CloseFileHandle, 104, true);
            command.Append(handle);
            var reply = connection.SendAndReceive(command);
            Error.CheckForError(reply, 6, 104);
        }

        private byte[] OpenHandles()
        {
            var reply = connection.SendAndReceive(new Command(SystemCommand.ListOpenHandles, 108, true));
            Error.CheckForError(reply, 108);
            return reply.GetData(5);
        }

        private string ListFiles(string path)
        {
            try
            {
                UInt16 maxlength = 1000;
                // LIST_FILES work as long as list does not exceed 1014 bytes. 
                var command = new Command(SystemCommand.ListFiles, 109, true);
                command.Append((UInt16)maxlength);
                command.Append(path);
                connection.Send(command);
                var reply = connection.Receive();
                Error.CheckForError(reply, command.SequenceNumber);

                /* returns:
                 * (Data32) SIZE: size of the directory data (in bytes)
                 * (Data8)HANDLE: file handle
                 * (DataX) DATA: first partition of data */
                Int32 ret_size = reply.GetInt32(5);
                byte ret_handle = reply.GetByte(5 + 4);
                string retval = reply.GetString(10);

                var part_size = Math.Min(maxlength, ret_size); //!!reply.Length; //??
                var rest = ret_size - part_size;
                BrickError berror = (BrickError)reply.GetByte(4);
                while (rest > 0)
                {
                    UInt16 maxlength2 = 1000;
                    var part2_size = Math.Min(maxlength2, rest);
                    var command2 = new Command(SystemCommand.ContinueListFiles, 209, true);
                    command2.Append(ret_handle);
                    command2.Append(part2_size);
                    connection.Send(command2);
                    var reply2 = connection.Receive();
                    Error.CheckForError(reply2, command2.SequenceNumber);

                    byte ret_handle2 = reply2.GetByte(5);
                    ret_handle = ret_handle2;
                    string retval2 = reply2.GetString(5 + 1);
                    BrickError berror2 = (BrickError)reply2.GetByte(4);

                    retval += retval2;
                    rest -= part2_size;
                }
                return retval;
            }
            catch (Exception ex)
            {
                connection.ResetOnError();
                throw; //re-throw
            }
        }

        /// <summary>
        /// Gets the folder info.
        /// </summary>
        /// <param name="path">Path to folder to read</param>
        /// <param name="files">Files in folder</param>
        /// <param name="subFoldersNames">Sub folders names</param>
        public void GetFolderInfo(string path, out BrickFolder[] subFoldersNames, out BrickFile[] files)
        {
            path = AlignProjectFilename(path);
            //Console.WriteLine("Get folder info at " +  path);
            var fileList = new List<BrickFile>();
            var folderList = new List<BrickFolder>();
            string list = ListFiles(path); //, (UInt16)MaxBytesInFileList);
            //Console.Write("Raw string: " + list);
            string[] element = list.Split(Environment.NewLine.ToCharArray());
            foreach (string s in element)
            {
                //Console.WriteLine("Element:" + s);
                if (s.EndsWith("/"))
                {
                    try
                    {
                        if (s != "../" && s != "./" && s != "/../" && s != "/./")
                        {
                            //folderList.Add("/" + s.Remove(s.Length - 1));
                            folderList.Add(new BrickFolder("/" + s.Remove(s.Length - 1), path));
                            //Console.WriteLine("Adding folder: "  + "/" + s.Remove(s.Length-1));
                        }
                    }
                    catch
                    {
                        //Console.WriteLine("Failed to add folder");	
                    }
                }
                else
                {
                    string[] fileInfo = s.Split(' ');
                    if (fileInfo.Length > 1)
                    {
                        try
                        {
                            byte[] sizeBytes = System.Text.Encoding.ASCII.GetBytes(fileInfo[1]);
                            UInt32 fileSize = BitConverter.ToUInt32(sizeBytes, 0);
                            fileList.Add(new BrickFile(string.Join(" ", fileInfo.Skip(2).ToArray()), fileSize, path));
                            //Console.WriteLine("Adding file: "  + fileInfo[2] +  " with size " + fileSize);
                        }
                        catch
                        {
                            //Console.WriteLine("Failed to file");			
                        }
                    }
                }
            }
            files = fileList.ToArray();
            subFoldersNames = folderList.ToArray();
        }

        public (BrickFolder[], BrickFile[]) GetFolderInfoHandleError(string path)
        {
            try
            {
                GetFolderInfo(path, out BrickFolder[] subFoldersNames, out BrickFile[] files);
                return (subFoldersNames, files);
            }
            catch (BrickException)
            {
                return (null, null);
            }
        }

        /// <summary>
        /// Get a complete folder structure
        /// </summary>
        /// <returns>The folder structure</returns>
        /// <param name="path">Path where you want the folder structure to start</param>
        public FolderStructure GetFolderStructure(string path)
        {
            path = AlignProjectFilename(path);
            FolderStructure folderStructure = null;
            try
            {
                BrickFile[] files;
                BrickFolder[] folders;
                GetFolderInfo(path, out folders, out files);
                folderStructure = new FolderStructure(path, true);
                foreach (var file in files)
                {
                    folderStructure.FileList.Add(file);
                }
                for (int i = 0; i < folders.Length; i++)
                {
                    string newPath = "";
                    if (path.EndsWith("/"))
                    {
                        newPath = path.Remove(path.Length - 1) + folders[i].Name;
                    }
                    else
                    {
                        newPath = path + folders[i].Name;
                    }
                    folderStructure.FolderList.Add(GetFolderStructure(newPath));
                }
            }
            catch (Exception /*ex*/)
            {
                folderStructure = new FolderStructure(path, false);
            }
            if (folderStructure.FolderList.Count > 0)
                folderStructure.FolderList.Sort();
            if (folderStructure.FileList.Count > 0)
                folderStructure.FileList.Sort();
            return folderStructure;
        }

        /// <summary>
        /// Read a file from the EV3
        /// </summary>
        /// <param name="file">File to download.</param>
        /// <param name="destinationFileName">Destination file name.</param>
        public void ReadFile(BrickFile file, string destinationFileName)
        {
            ReadFile(file.Name, destinationFileName);
        }

        /// <summary>
        /// Read a file from the EV3
        /// </summary>
        /// <param name="brickFileName">Brick file name.</param>
        /// <param name="destinationFileName">Destination file name.</param>
        public void ReadFile(string brickFileName, string destinationFileName)
        {
            brickFileName = AlignProjectFilename(brickFileName);
            byte[] fileData = ReadWholeFile(brickFileName);
            using (FileStream fileStream = new FileStream(destinationFileName, System.IO.FileMode.Create, FileAccess.Write))
            {
                fileStream.Write(fileData, 0, fileData.Length);
                fileStream.Close();
            }
        }
        //public void GetFile(string brickFileName, string destinationFileName)
        //{
        //    brickFileName = AlignProjectFilename(brickFileName);
        //    byte[] fileData = GetWholeFile(brickFileName);
        //    using (FileStream fileStream = new FileStream(destinationFileName, System.IO.FileMode.Create, FileAccess.Write))
        //    {
        //        fileStream.Write(fileData, 0, fileData.Length);
        //        fileStream.Close();
        //    }
        //}

        public string ReadFileToString(string brickFileName)
        {
            brickFileName = AlignProjectFilename(brickFileName);
            byte[] fileData = ReadWholeFile(brickFileName);
            return System.Text.Encoding.ASCII.GetString(fileData);
        }

        public byte[] ReadFileToByteArray(string brickFileName)
        {
            brickFileName = AlignProjectFilename(brickFileName);
            byte[] fileData = ReadWholeFile(brickFileName);
            return fileData;
        }

        ///<summary>
        /// Write a file to the EV3
        /// </summary>
        /// <param name='fileToWrite'>
        /// PC file to write to brick
        /// </param>
        /// <param name='brickFileName'>
        /// File name that the file should have on the brick including file path example: /home/root/lms2012/prjs/someFileName.txt
        /// </param>
        public void WriteFile(string sourceFile, string brickFileName)
        {
            using (FileStream fileStream = new FileStream(sourceFile, System.IO.FileMode.Open, FileAccess.Read))
            {
                WriteFile(fileStream, brickFileName, (int)fileStream.Length);
                fileStream.Close();
            }
        }

        ///<summary>
        /// Write a file to the EV3
        /// </summarysourceStream
        /// <param name='fileToWrite'>
        /// Stream to write to the brick
        /// </param>
        /// <param name='brickFileName'>
        /// File name that the file should have on the brick including file path example: /home/root/lms2012/prjs/someFileName.txt
        /// </param>
        public void WriteFile(Stream sourceStream, string brickFileName, int streamSize)
        {
            brickFileName = AlignProjectFilename(brickFileName);
            byte[] fileData = null;

            fileData = new byte[streamSize];
            sourceStream.Read(fileData, 0, (int)streamSize);

            WriteCompleteArray(brickFileName, fileData);
        }

        ///<summary>
        /// Write a file to the EV3
        /// </summary>
        /// <param name='fileToWrite'>
        /// PC file to write to brick
        /// </param>
        /// <param name='brickFileName'>
        /// File name that the file should have on the brick including file path example: /home/root/lms2012/prjs/someFileName.txt
        /// </param>
        public void WriteFileFromString(string data, string brickFileName)
        {
            brickFileName = AlignProjectFilename(brickFileName);
            byte[] fileData = Encoding.ASCII.GetBytes(data);
            WriteCompleteArray(brickFileName, fileData);
        }

        /// <summary>
        /// Creates a directory.
        /// </summary>
        /// <param name="dir">Full path of directory to create.</param>
        public void CreateDirectory(string dir)
        {
            var command = new Command(SystemCommand.CreateDir, 105, true);
            command.Append(dir);
            var reply = connection.SendAndReceive(command);
            Error.CheckForError(reply, 5, 105);
        }

        /// <summary>
        /// Deletes a file on the EV3.
        /// </summary>
        /// <param name="file">File to delete.</param>
        public void DeleteFile(BrickFile file)
        {
            DeleteFile(file.FullName);
        }

        /// <summary>
        /// Deletes a file on the EV3.
        /// </summary>
        /// <param name="fileName">File name to delete.</param>
        public void DeleteFile(string fileName)
        {
            var command = new Command(SystemCommand.DeleteFile, 106, true);
            command.Append(AlignProjectFilename(fileName));
            var reply = connection.SendAndReceive(command);
            Error.CheckForError(reply, 5, 106);
        }

        public static string AlignProjectFilename(string filename)
        {
            if (!(filename.StartsWith("..") || filename.StartsWith("/")))
                return (BRICK_PROJECTS_FOLDER + $"{filename}");
            else
                return filename;
        }

        public bool FileExistsOld(string filename)
        {
            //opFileNameTotalSize = 50705, // 0x0000C611
            BrickFile[] files;
            GetFolderInfo(Path.GetDirectoryName(filename).Replace('\\', '/'), out _, out files);
            return files
                .Select(elem => elem.Name)
                .Contains(Path.GetFileName(filename));
        }
        public (uint numberOfFiles, uint totalFolderSize) TotalSizeOfDir(string dirname)
        {
            var command = new Command(4 * 2, 0, 700, true);
            command.Append(ByteCodes.FILENAME);
            command.Append(ArrayFileNameSubcodes.TOTALSIZE);
            command.Append(dirname, ConstantParameterType.Value);
            command.Append((byte)0, VariableScope.Global); //Return at Global Var offset 0. Offset encoded as single byte
            command.Append((byte)4, VariableScope.Global); //Return at Global Var offset 0. Offset encoded as single byte
            connection.Send(command);
            var brickReply = connection.Receive();
            Error.CheckForError(brickReply, command.SequenceNumber);
            var numberOfFiles = brickReply.GetUInt32(3);
            var totalFolderSize = brickReply.GetUInt32(3);
            return (numberOfFiles, totalFolderSize);
        }
        public bool FileExists(string filename)
        {
            filename = AlignProjectFilename(filename);
            var command = new Command(1, 0, 701, true);
            command.Append(ByteCodes.FILENAME);
            command.Append(ArrayFileNameSubcodes.EXIST);
            command.Append(filename, ConstantParameterType.Value);
            command.Append((byte)0, VariableScope.Global); //Return at Global Var offset 0. Offset encoded as single byte
            connection.Send(command);
            var brickReply = connection.Receive();
            Error.CheckForError(brickReply, command.SequenceNumber);
            var b = brickReply.GetByte(3);
            return b == 1;
        }
        //public bool GetCurrentFolderName()
        //{
        //    var destfile = "../prjs/test1/alma.rtf";

        //    const int FILENAMESIZE = 120;
        //    var maxlen = FILENAMESIZE;
        //    var command = new Command(maxlen, 0, 603, true);
        //    command.Append(ByteCodes.FILENAME);
        //    command.Append(ArrayFileNameSubcodes.GET_FOLDERNAME);
        //    command.Append((byte)maxlen, ConstantParameterType.Value);
        //    //command.Append((byte)maxlen, ParameterFormat.Short); //Arguments: (Data8) LENGTH � Max length of returned string
        //    command.Append((byte)0, VariableScope.Global); //Return at Global Var offset 0. Offset encoded as single byte
        //    connection.Send(command);
        //    var brickReply = connection.Receive();
        //    Error.CheckForError(brickReply, command.SequenceNumber);
        //    var b = brickReply.GetString(3, (byte)maxlen);

        //    return true;
        //}
    }
}

