using MonoBrick;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace MonoBrick.EV3
{
    /// <summary>
    /// Encoded Parameter format
    /// </summary>
    [Flags]
    public enum ParameterFormat : byte
    {
#pragma warning disable
        Short = 0x00,
        Long = 0x80
#pragma warning restore
    }

    /// <summary>
    /// Encoded Parameter type
    /// </summary>
    [Flags]
    public enum ParameterType : byte
    {
#pragma warning disable
        Constant = 0x00,
        Variable = 0x40
#pragma warning restore
    }

    /// <summary>
    /// Encoded Parameter sign when using short constant format
    /// </summary>
    [Flags]
    enum ShortSign : byte
    {
#pragma warning disable
        Positive = 0x00,
        Negative = 0x20
#pragma warning restore
    }


    /// <summary>
    /// Encoded Parameter type when using long constant format
    /// </summary>
    [Flags]
    public enum ConstantParameterType : byte
    {
#pragma warning disable
        Value = 0x00,
        Label = 0x20
#pragma warning restore
    }

    /// <summary>
    /// Encoded Parameter scope when using long variable format
    /// </summary>
    [Flags]
    public enum VariableScope : byte
    {
#pragma warning disable
        Local = 0x00,
        Global = 0x20,
#pragma warning restore
    }

    /// <summary>
    /// Encoded Parameter type when using long variable format
    /// </summary>
    [Flags]
    public enum VariableType : byte
    {
#pragma warning disable
        Value = 0x00,
        Handle = 0x10
#pragma warning restore
    }

    /// <summary>
    /// Encoded Parameter following when using long format
    /// </summary>
    [Flags]
    enum FollowType : byte
    {
#pragma warning disable
        OneByte = 0x01,
        TwoBytes = 0x02,
        FourBytes = 0x03,
        TerminatedString = 0x00,
        TerminatedString2 = 0x04
#pragma warning restore
    }

    /// <summary>
    /// Program slots used by the EV3
    /// </summary>
    public enum ProgramSlots
    {
        /// <summary>
        /// Program slot reserved for executing the user interface
        /// </summary>
        Gui = 0,

        /// <summary>
        /// Program slot used to execute user projects, apps and tools
        /// </summary>
        User = 1,

        /// <summary>
        /// Program slot used for direct commands coming from c_com
        /// </summary>
        Cmd = 2,

        /// <summary>
        /// Program slot used for direct commands coming from c_ui
        /// </summary>
        Term = 3,

        /// <summary>
        /// Program slot used to run the debug ui
        /// </summary>
        Debug = 4,

        /// <summary>
        /// ONLY VALID IN opPROGRAM_STOP
        /// </summary>
        Current = -1
    }

    /// <summary>
    /// The daisychain layer
    /// </summary>
    public enum DaisyChainLayer
    {
        /// <summary>
        /// The EV3
        /// </summary>
        EV3 = 0,

        /// <summary>
        /// First EV3 in the Daisychain
        /// </summary>
        First = 1,

        /// <summary>
        /// Second EV3 in the Daisychain
        /// </summary>
        Second = 2,

        /// <summary>
        /// Third EV3 in the Daisychain
        /// </summary>
        Third = 3,
    }




    /// <summary>
    /// EV3 command type.
    /// </summary>
    public enum CommandType
    {
        /// <summary>
        /// Direct command
        /// </summary>
        DirectCommand = 0x00,

        /// <summary>
        /// System command.
        /// </summary>
        SystemCommand = 0x01,

        /// <summary>
        /// Direct command reply.
        /// </summary>
        DirectReply = 0x02,

        /// <summary>
        /// System command reply.
        /// </summary>
        SystemReply = 0x03,

        /// <summary>
        /// Direct reply with error.
        /// </summary>
        DirectReplyWithError = 0x04,

        /// <summary>
        /// System reply with error.
        /// </summary>
        SystemReplyWithError = 0x05
    }

    /// <summary>
    /// EV3 system commands
    /// </summary>
    public enum SystemCommand
    {
#pragma warning disable
        None = 0x00,
        BeginDownload = 0x92,
        ContinueDownload = 0x93,
        BeginUpload = 0x94,
        ContinueUpload = 0x95,
        BeginGetFile = 0x96,
        ContinueGetFile = 0x97,
        CloseFileHandle = 0x98,
        ListFiles = 0x99,
        ContinueListFiles = 0x9a,
        CreateDir = 0x9b,
        DeleteFile = 0x9c,
        ListOpenHandles = 0x9d,
        WriteMailbox = 0x9e,
        BluetoothPin = 0x9f,
        EnterFirmwareUpdate = 0xa0
#pragma warning restore
    }

    /// <summary>
    /// EV3 byte codes
    /// </summary>
    public enum ByteCodes
    {
#pragma warning disable
        // VM
        //                                    0000....
        ERROR = 0x00, //       0000
        NOP = 0x01, //       0001
        PROGRAM_STOP = 0x02, //       0010
        PROGRAM_START = 0x03, //       0011
        OBJECT_STOP = 0x04, //       0100
        OBJECT_START = 0x05, //       0101
        OBJECT_TRIG = 0x06, //       0110
        OBJECT_WAIT = 0x07, //       0111
        RETURN = 0x08, //       1000
        CALL = 0x09, //       1001
        OBJECT_END = 0x0A, //       1010
        SLEEP = 0x0B, //       1011
        PROGRAM_INFO = 0x0C, //       1100
        LABEL = 0x0D, //       1101
        PROBE = 0x0E, //       1110
        DO = 0x0F, //       1111

        // cMath "MATH"
        //                                    0001....
        //                    ADD                 00..
        ADD8 = 0x10, //         00
        ADD16 = 0x11, //         01
        ADD32 = 0x12, //         10
        ADDF = 0x13, //         11
                     //                    SUB                 01..
        SUB8 = 0x14, //         00
        SUB16 = 0x15, //         01
        SUB32 = 0x16, //         10
        SUBF = 0x17, //         11
                     //                    MUL                 10..
        MUL8 = 0x18, //         00
        MUL16 = 0x19, //         01
        MUL32 = 0x1A, //         10
        MULF = 0x1B, //         11
                     //                    DIV                 11..
        DIV8 = 0x1C, //         00
        DIV16 = 0x1D, //         01
        DIV32 = 0x1E, //         10
        DIVF = 0x1F, //         11

        // Logic "LOGIC"
        //        LOGIC                       0010....
        //                    OR                  00..
        OR8 = 0x20, //         00
        OR16 = 0x21, //         01
        OR32 = 0x22, //         10

        //                    AND                 01..
        AND8 = 0x24, //         00
        AND16 = 0x25, //         01
        AND32 = 0x26, //         10

        //                    XOR                 10..
        XOR8 = 0x28, //         00
        XOR16 = 0x29, //         01
        XOR32 = 0x2A, //         10

        //                    RL                  11..
        RL8 = 0x2C, //         00
        RL16 = 0x2D, //         01
        RL32 = 0x2E, //         10

        // cMove "MOVE"
        INIT_BYTES = 0x2F, //       1111
                           //        MOVE                        0011....
                           //                    MOVE8_              00..
        MOVE8_8 = 0x30, //         00
        MOVE8_16 = 0x31, //         01
        MOVE8_32 = 0x32, //         10
        MOVE8_F = 0x33, //         11
                        //                    MOVE16_             01..
        MOVE16_8 = 0x34, //         00
        MOVE16_16 = 0x35, //         01
        MOVE16_32 = 0x36, //         10
        MOVE16_F = 0x37, //         11
                         //                    MOVE32_             10..
        MOVE32_8 = 0x38, //         00
        MOVE32_16 = 0x39, //         01
        MOVE32_32 = 0x3A, //         10
        MOVE32_F = 0x3B, //         11
                         //                    MOVEF_              11..
        MOVEF_8 = 0x3C, //         00
        MOVEF_16 = 0x3D, //         01
        MOVEF_32 = 0x3E, //         10
        MOVEF_F = 0x3F, //         11

        // cBranch "BRANCH"
        //        BRANCH                      010000..
        JR = 0x40, //         00
        JR_FALSE = 0x41, //         01
        JR_TRUE = 0x42, //         10
        JR_NAN = 0x43, //         11

        // cCompare "COMPARE"
        //        COMPARE                     010.....
        //                    CP_LT              001..
        CP_LT8 = 0x44, //         00
        CP_LT16 = 0x45, //         01
        CP_LT32 = 0x46, //         10
        CP_LTF = 0x47, //         11
                       //                    CP_GT              010..
        CP_GT8 = 0x48, //         00
        CP_GT16 = 0x49, //         01
        CP_GT32 = 0x4A, //         10
        CP_GTF = 0x4B, //         11
                       //                    CP_EQ              011..
        CP_EQ8 = 0x4C, //         00
        CP_EQ16 = 0x4D, //         01
        CP_EQ32 = 0x4E, //         10
        CP_EQF = 0x4F, //         11
                       //                    CP_NEQ             100..
        CP_NEQ8 = 0x50, //         00
        CP_NEQ16 = 0x51, //         01
        CP_NEQ32 = 0x52, //         10
        CP_NEQF = 0x53, //         11
                        //                    CP_LTEQ            101..
        CP_LTEQ8 = 0x54, //         00
        CP_LTEQ16 = 0x55, //         01
        CP_LTEQ32 = 0x56, //         10
        CP_LTEQF = 0x57, //         11
                         //                    CP_GTEQ            110..
        CP_GTEQ8 = 0x58, //         00
        CP_GTEQ16 = 0x59, //         01
        CP_GTEQ32 = 0x5A, //         10
        CP_GTEQF = 0x5B, //         11

        // Select "SELECT"
        //        SELECT                      010111..
        SELECT8 = 0x5C, //         00
        SELECT16 = 0x5D, //         01
        SELECT32 = 0x5E, //         10
        SELECTF = 0x5F, //         11


        // VM
        SYSTEM = 0x60,
        PORT_CNV_OUTPUT = 0x61,
        PORT_CNV_INPUT = 0x62,
        NOTE_TO_FREQ = 0x63,

        // cBranch "BRANCH"
        // BRANCH                      011000..
        //?       00
        //?       01
        //?       10
        //?       11
        // JR_LT              001..
        JR_LT8 = 0x64, //         00
        JR_LT16 = 0x65, //         01
        JR_LT32 = 0x66, //         10
        JR_LTF = 0x67, //         11
                       //                    JR_GT              010..
        JR_GT8 = 0x68, //         00
        JR_GT16 = 0x69, //         01
        JR_GT32 = 0x6A, //         10
        JR_GTF = 0x6B, //         11
                       //                    JR_EQ              011..
        JR_EQ8 = 0x6C, //         00
        JR_EQ16 = 0x6D, //         01
        JR_EQ32 = 0x6E, //         10
        JR_EQF = 0x6F, //         11
                       //                    JR_NEQ             100..
        JR_NEQ8 = 0x70, //         00
        JR_NEQ16 = 0x71, //         01
        JR_NEQ32 = 0x72, //         10
        JR_NEQF = 0x73, //         11
                        //                    JR_LTEQ            101..
        JR_LTEQ8 = 0x74, //         00
        JR_LTEQ16 = 0x75, //         01
        JR_LTEQ32 = 0x76, //         10
        JR_LTEQF = 0x77, //         11
                         //                    JR_GTEQ            110..
        JR_GTEQ8 = 0x78, //         00
        JR_GTEQ16 = 0x79, //         01
        JR_GTEQ32 = 0x7A, //         10
        JR_GTEQF = 0x7B, //         11

        // VM
        INFO = 0x7C, //   01111100
        STRINGS = 0x7D, //   01111101
        MEMORY_WRITE = 0x7E, //   01111110
        MEMORY_READ = 0x7F, //   01111111

        //        SYSTEM                      1.......

        // cUi "UI"
        //        UI                          100000..
        UI_FLUSH = 0x80, //         00
        UI_READ = 0x81, //         01
        UI_WRITE = 0x82, //         10
        UI_BUTTON = 0x83, //         11
        UI_DRAW = 0x84, //   10000100

        // cTimer "TIMER"
        TIMER_WAIT = 0x85, //   10000101
        TIMER_READY = 0x86, //   10000110
        TIMER_READ = 0x87, //   10000111

        // VM
        //        BREAKPOINT                  10001...
        BP0 = 0x88, //        000
        BP1 = 0x89, //        001
        BP2 = 0x8A, //        010
        BP3 = 0x8B, //        011
        BP_SET = 0x8C, //   10001100
        MATH = 0x8D, //   10001101
        RANDOM = 0x8E, //   10001110

        // cTimer "TIMER"
        TIMER_READ_US = 0x8F, //   10001111

        // cUi "UI"
        KEEP_ALIVE = 0x90, //   10010000

        // cCom "COM"
        //                                      100100
        COM_READ = 0x91, //         01
        COM_WRITE = 0x92, //         10

        // cSound "SOUND"
        //                                      100101
        SOUND = 0x94, //         00
        SOUND_TEST = 0x95, //         01
        SOUND_READY = 0x96, //         10

        // cInput "INPUT"
        //
        INPUT_SAMPLE = 0x97, //   10010111

        //                                    10011...
        INPUT_DEVICE_LIST = 0x98, //        000
        INPUT_DEVICE = 0x99, //        001
        INPUT_READ = 0x9A, //        010
        INPUT_TEST = 0x9B, //        011
        INPUT_READY = 0x9C, //        100
        INPUT_READSI = 0x9D, //        101
        INPUT_READEXT = 0x9E, //        110
        INPUT_WRITE = 0x9F, //        111
                            // cOutput "OUTPUT"
                            //                                    101.....
        OUTPUT_GET_TYPE = 0xA0, //      00000
        OUTPUT_SET_TYPE = 0xA1, //      00001
        OUTPUT_RESET = 0xA2, //      00010
        OUTPUT_STOP = 0xA3, //      00011
        OUTPUT_POWER = 0xA4, //      00100
        OUTPUT_SPEED = 0xA5, //      00101
        OUTPUT_START = 0xA6, //      00110
        OUTPUT_POLARITY = 0xA7, //      00111
        OUTPUT_READ = 0xA8, //      01000
        OUTPUT_TEST = 0xA9, //      01001
        OUTPUT_READY = 0xAA, //      01010
        OUTPUT_POSITION = 0xAB, //      01011
        OUTPUT_STEP_POWER = 0xAC, //      01100
        OUTPUT_TIME_POWER = 0xAD, //      01101
        OUTPUT_STEP_SPEED = 0xAE, //      01110
        OUTPUT_TIME_SPEED = 0xAF, //      01111

        OUTPUT_STEP_SYNC = 0xB0, //      10000
        OUTPUT_TIME_SYNC = 0xB1, //      10001
        OUTPUT_CLR_COUNT = 0xB2, //      10010
        OUTPUT_GET_COUNT = 0xB3, //      10011

        OUTPUT_PRG_STOP = 0xB4, //      10100

        // cMemory "MEMORY"
        //                                    11000...
        FILE = 0xC0, //        000
        ARRAY = 0xC1, //        001
        ARRAY_WRITE = 0xC2, //        010
        ARRAY_READ = 0xC3, //        011
        ARRAY_APPEND = 0xC4, //        100
        MEMORY_USAGE = 0xC5, //        101
        FILENAME = 0xC6, //        110

        // cMove "READ"
        //                                    110010..
        READ8 = 0xC8, //         00
        READ16 = 0xC9, //         01
        READ32 = 0xCA, //         10
        READF = 0xCB, //         11

        // cMove "WRITE"
        //                                    110011..
        WRITE8 = 0xCC, //         00
        WRITE16 = 0xCD, //         01
        WRITE32 = 0xCE, //         10
        WRITEF = 0xCF, //         11

        // cCom "COM"
        //                                    11010...
        COM_READY = 0xD0, //        000
        COM_READDATA = 0xD1, //        001
        COM_WRITEDATA = 0xD2, //        010
        COM_GET = 0xD3, //        011
        COM_SET = 0xD4, //        100
        COM_TEST = 0xD5, //        101
        COM_REMOVE = 0xD6, //        110
        COM_WRITEFILE = 0xD7, //        111

        //                                    11011...
        MAILBOX_OPEN = 0xD8, //        000
        MAILBOX_WRITE = 0xD9, //        001
        MAILBOX_READ = 0xDA, //        010
        MAILBOX_TEST = 0xDB, //        011
        MAILBOX_READY = 0xDC, //        100
        MAILBOX_CLOSE = 0xDD, //        101

        //        SPARE                       111.....

        // TST
        TST = 0xFF, //  11111111
#pragma warning restore
    }


    /// <summary>
    /// EV3 sound sub codes
    /// </summary>
    public enum SoundSubCodes
    {
#pragma warning disable
        Break = 0,
        Tone = 1,
        Play = 2,
        Repeat = 3,
        Service = 4
#pragma warning restore
    }

    /// <summary>
    /// EV3 input sub codes.
    /// </summary>
    public enum InputSubCodes
    {
#pragma warning disable
        GetFormat = 2,
        CalMinMax = 3,
        CalDefault = 4,
        GetTypeMode = 5,
        GetSymbol = 6,
        CalMin = 7,
        CalMax = 8,
        Setup = 9,
        ClearAll = 10,
        GetRaw = 11,
        GetConnection = 12,
        StopAll = 13,
        GetName = 21,
        GetModeName = 22,
        SetRaw = 23,
        GetFigures = 24,
        GetChanges = 25,
        ClrChanges = 26,
        ReadyPCT = 27,
        ReadyRaw = 28,
        ReadySI = 29,
        GetMinMax = 30,
        GetBumps = 31
#pragma warning disable
    }

    /// <summary>
    /// EV3 file sub codes.
    /// </summary>
    public enum FileSubCodes
    {
#pragma warning disable
        OpenAppend = 0,
        OpenRead = 1,
        OpenWrite = 2,
        ReadValue = 3,
        WriteValue = 4,
        ReadText = 5,
        WriteText = 6,
        Close = 7,
        LoadImage = 8,
        GetHandle = 9,
        LoadPicture = 10,
        GetPool = 11,
        Unload = 12,
        GetFolders = 13,
        GetIcon = 14,
        GetSubfolderName = 15,
        WriteLog = 16,
        CLoseLog = 17,
        GetImage = 18,
        GetItem = 19,
        GetCacheFiles = 20,
        PutCacheFile = 21, // MRU
        GetCacheFile = 22,
        DelCacheFile = 23,
        DelSubfolder = 24,
        GetLogName = 25,
        GetCacheName = 26,
        OpenLog = 27,
        ReadBytes = 28,
        WriteBytes = 29,
        Remove = 30,
        Move = 31,
#pragma warning restore
    }

    /// <summary>
    /// Memory sub codes
    /// </summary>
    public enum MemorySubCodes
    {
#pragma warning disable
        Delete = 0,
        Create8 = 1,
        Create16 = 2,
        Create32 = 3,
        CreateTEF = 4,
        Resize = 5,
        Fill = 6,
        Copy = 7,
        Init8 = 8,
        Init16 = 9,
        Init32 = 10,
        InitF = 11,
        Size = 12,
#pragma warning restore
    }

    public enum UIReadSubCodes
    {
        GET_VBATT = 0x01,
        GET_IBATT = 0x02,
        GET_OS_VERS = 0x03,
        GET_TBATT = 0x05,
        GET_IINT = 0x06,
        GET_HW_VERS = 0x09,
        GET_FW_VERS = 0x0A,
        GET_FW_BUILD = 0x0B,
        GET_OS_BUILD = 0x0C,
        GET_ADDRESS = 0x0D,
        GET_LBATT = 18,
        GET_VERSION = 26,
        GET_IP = 27,
        GET_POWER = 29
    }

    internal enum UiWriteSubcode
    {
        WRITE_FLUSH = 1,
        FLOATVALUE = 2,
        STAMP = 3,
        PUT_STRING = 8,
        VALUE8 = 9,
        VALUE16 = 10,
        VALUE32 = 11,
        VALUEF = 12,
        ADDRESS = 13,
        CODE = 14,
        DOWNLOAD_END = 15,
        SCREEN_BLOCK = 16,
        ALLOW_PULSE = 17,
        SET_PULSE = 18,
        TEXTBOX_APPEND = 21,
        SET_BUSY = 22,
        SET_TESTPIN = 24,
        INIT_RUN = 25,
        UPDATE_RUN = 26,
        LED = 27,
        POWER = 29,
        GRAPH_SAMPLE = 30,
        TERMINAL = 31,
    }

    internal enum UiDrawSubcode
    {
        UPDATE = 0,
        CLEAN = 1,
        PIXEL = 2,
        LINE = 3,
        CIRCLE = 4,
        TEXT = 5,
        ICON = 6,
        PICTURE = 7,
        VALUE = 8,
        FILLRECT = 9,
        RECT = 10,
        NOTIFICATION = 11,
        QUESTION = 12,
        KEYBOARD = 13,
        BROWSE = 14,
        VERTBAR = 15,
        INVERSERECT = 16,
        SELECT_FONT = 17,
        TOPLINE = 18,
        FILLWINDOW = 19,
        SCROLL = 20,
        DOTLINE = 21,
        VIEW_VALUE = 22,
        VIEW_UNIT = 23,
        FILLCIRCLE = 24,
        STORE = 25,
        RESTORE = 26,
        ICON_QUESTION = 27,
        BMPFILE = 28,
        POPUP = 29,
        GRAPH_SETUP = 30,
        GRAPH_DRAW = 31,
        TEXTBOX = 32,
    }

    public enum ProgramInfoSubcodes //PROGRAM_INFO
    {
        OBJ_STOP = 0,
        OBJ_START = 4,
        GET_STATUS = 22,
        GET_SPEED = 23,
        GET_PRGRESULT = 24, // 0x00000C18
        SET_INSTR = 25,
        GET_PRGNAME = 26, //UNOFFICIAL //0x00000C1A
    }

    public enum ArrayFileNameSubcodes // array and filename subcodes
    {
        DELETE = 0,
        CREATE8 = 1,
        CREATE16 = 2,
        CREATE32 = 3,
        CREATEF = 4,
        RESIZE = 5,
        FILL = 6,
        COPY = 7,
        INIT8 = 8,
        INIT16 = 9,
        INIT32 = 10,
        INITF = 11,
        SIZE = 12,
        READ_CONTENT = 13,
        WRITE_CONTENT = 14,
        READ_SIZE = 15,
        // # File name subcodes
        EXIST = 16, //0x10
        TOTALSIZE = 17,
        SPLIT = 18,
        MERGE = 19,
        CHECK = 20,
        PACK = 21,
        UNPACK = 22,
        GET_FOLDERNAME = 23,
    }

    public enum ComGetSubcode
    {
        GET_ON_OFF = 1,
        GET_VISIBLE = 2,
        GET_RESULT = 4,
        GET_PIN = 5,
        SEARCH_ITEMS = 8,
        SEARCH_ITEM = 9,
        FAVOUR_ITEMS = 10,
        FAVOUR_ITEM = 11,
        GET_ID = 12,
        GET_BRICKNAME = 13,
        GET_NETWORK = 14,
        GET_PRESENT = 15,
        GET_ENCRYPT = 16,
        CONNEC_ITEMS = 17,
        CONNEC_ITEM = 18,
        GET_INCOMING = 19,
        GET_MODE2 = 20,

    }

    public enum ResultCodes
    {
        OK = 0,
        BUSY = 1,
        FAIL = 2,
        STOP = 4,
        START = 8,
    }

    public enum ObjectStatusCodes
    {
        RUNNING = 0x0010,
        WAITING = 0x0020,
        STOPPED = 0x0040,
        HALTED = 0x0080
    }

    internal enum LedPattern
    {
        LED_BLACK = 0,
        LED_GREEN = 1,
        LED_RED = 2,
        LED_ORANGE = 3,
        LED_GREEN_FLASH = 4,
        LED_RED_FLASH = 5,
        LED_ORANGE_FLASH = 6,
        LED_GREEN_PULSE = 7,
        LED_RED_PULSE = 8,
        LED_ORANGE_PULSE = 9
    }

    internal enum HWTYPE
    {
        HW_USB = 1,
        HW_BT = 2,
        HW_WIFI = 3,
    }

    /// <summary>
    /// Class for creating a EV3 system command.
    /// </summary>
    public class Command : BrickCommand
    {
        private SystemCommand systemCommand;
        private CommandType commandType;
        private UInt16 sequenceNumber;
        /// <summary>
        /// The short value maximum size
        /// </summary>
        public const sbyte ShortValueMax = 31;

        /// <summary>
        /// The short value minimum size
        /// </summary>
        public const sbyte ShortValueMin = -32;

        /// <summary>
        /// Initializes a new instance of the <see cref="MonoBrick.EV3.Command"/> class.
        /// </summary>
        /// <param name="data">Data.</param>
        public Command(byte[] data)
        {
            if (data.Length < 4)
            {
                throw new System.ArgumentException("Invalid EV3 Command");
            }
            for (int i = 0; i < data.Length; i++)
            {
                dataArr.Add(data[i]);
            }
            this.sequenceNumber = (UInt16)(0x0000 | dataArr[0] | (dataArr[1] << 8)); //!! was << 2
            try
            {
                commandType = (CommandType)(data[2] & 0x7f);
                if (commandType == CommandType.SystemCommand)
                {
                    systemCommand = (SystemCommand)data[3];
                }
                else
                {
                    systemCommand = SystemCommand.None;
                }

            }
            catch (BrickException)
            {
                throw new System.ArgumentException("Invalid EV3 Command");
            }
            replyRequired = !Convert.ToBoolean(data[2] & 0x80);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MonoBrick.EV3.Command"/> class as a system command
        /// </summary>
        /// <param name="systemCommand">System command.</param>
        /// <param name="sequenceNumber">Sequence number.</param>
        /// <param name="reply">If set to <c>true</c> reply will be send from brick</param>
        public Command(SystemCommand systemCommand, UInt16 sequenceNumber, bool reply)
        {
            this.systemCommand = systemCommand;
            this.commandType = CommandType.SystemCommand;
            this.sequenceNumber = sequenceNumber;
            this.Append(sequenceNumber);

            if (reply)
            {
                replyRequired = true;
                dataArr.Add((byte)commandType);
            }
            else
            {
                replyRequired = false;
                dataArr.Add((byte)((byte)commandType | 0x80));
            }
            dataArr.Add((byte)systemCommand);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MonoBrick.EV3.Command"/> class as a direct command
        /// </summary>
        /// <param name="byteCode">Bytecode to use for the direct command</param>
        /// <param name="globalVariables">Global variables.</param>
        /// <param name="localVariables">Number of global variables</param>
        /// <param name="sequenceNumber">Number of local variables</param>
        /// <param name="reply">If set to <c>true</c> reply will be send from the brick</param>
        public Command(ByteCodes byteCode, int globalVariables, int localVariables, UInt16 sequenceNumber, bool reply) : this(globalVariables, localVariables, sequenceNumber, reply)
        {
            this.Append(byteCode);
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="MonoBrick.EV3.Command"/> as a direct command
        /// </summary>
        /// <param name="globalVariables">Global bytes.</param>
        /// <param name="localVariables">Number of global variables</param>
        /// <param name="sequenceNumber">Number of local variables</param>
        /// <param name="reply">If set to <c>true</c> reply will be send from brick</param>
        public Command(int globalVariables, int localVariables, UInt16 sequenceNumber, bool reply)
        {
            this.systemCommand = SystemCommand.None;
            this.commandType = CommandType.DirectCommand;
            this.sequenceNumber = sequenceNumber;
            this.Append(sequenceNumber);
            if (reply)
            {
                replyRequired = true;
                dataArr.Add((byte)commandType);
            }
            else
            {
                replyRequired = false;
                dataArr.Add((byte)((byte)commandType | 0x80));
            }
            byte firstByte = (byte)(globalVariables & 0xFF);
            byte secondByte = (byte)((localVariables << 2) | (globalVariables >> 8));
            this.Append(firstByte);
            this.Append(secondByte);
        }

        /// <summary>
        /// Gets the EV3 system command.
        /// </summary>
        /// <value>The system command.</value>
        public SystemCommand SystemCommandType
        {
            get { return systemCommand; }
        }
        /// <summary>
        /// Gets the EV3 command type
        /// </summary>
        /// <value>The type of the command.</value>
        public CommandType CommandType
        {
            get { return commandType; }
        }

        /// <summary>
        /// Gets the sequence number
        /// </summary>
        /// <value>The sequence number.</value>
        public UInt16 SequenceNumber
        {
            get { return sequenceNumber; }
        }

        /// <summary>
        /// Append a sensor type value
        /// </summary>
        /// <param name="type">Sensor type to append</param>
        public void Append(SensorType type)
        {
            Append((byte)type, ParameterFormat.Short);
        }

        /// <summary>
        /// Append a sensor mode value
        /// </summary>
        /// <param name="mode">Sensor mode to append</param>
        public void Append(SensorMode mode)
        {
            Append((byte)mode, ParameterFormat.Short);
        }

        /// <summary>
        /// Append a byte code value
        /// </summary>
        /// <param name="byteCode">Byte code to append</param>
        public void Append(ByteCodes byteCode)
        {
            Append((byte)byteCode);
        }

        /// <summary>
        /// Append a file sub code
        /// </summary>
        /// <param name="code">Code to append.</param>
        public void Append(FileSubCodes code)
        {
            Append((sbyte)code, ParameterFormat.Short);
        }
        public void Append(ArrayFileNameSubcodes code)
        {
            Append((sbyte)code, ParameterFormat.Short);
        }

        /// <summary>
        /// Append a file sub code
        /// </summary>
        /// <param name="code">Code to append.</param>
        public void Append(SoundSubCodes code)
        {
            Append((sbyte)code, ParameterFormat.Short);
        }

        /// <summary>
        /// Append a daisy chain layer 
        /// </summary>
        /// <param name="chain">Daisy chain layer to append</param>
        public void Append(DaisyChainLayer chain)
        {
            Append((sbyte)chain, ParameterFormat.Short);
        }

        /// <summary>
        /// Append a input sub code
        /// </summary>
        /// <param name="subCode">Sub code to append</param>
        public void Append(InputSubCodes subCode)
        {
            Append((sbyte)subCode, ParameterFormat.Short);
        }

        /// <summary>
        /// Append a memory sub code
        /// </summary>
        /// <param name="subCode">Sub code to append</param>
        public void Append(MemorySubCodes subCode)
        {
            Append((sbyte)subCode, ParameterFormat.Short);
        }

        /// <summary>
        /// Append a sensor port
        /// </summary>
        /// <param name="port">Sensor port to append</param>
        public void Append(SensorPort port)
        {
            Append((sbyte)port, ParameterFormat.Short);
        }

        /// <summary>
        /// Append a motor port
        /// </summary>
        /// <param name="port">Motor port to append</param>
        public void Append(MotorPort port)
        {
            Append((sbyte)port, ParameterFormat.Short);
        }

        /// <summary>
        /// Append a output bit field
        /// </summary>
        /// <param name="bitField">Bit field to append</param>
        public void Append(OutputBitfield bitField)
        {
            Append((sbyte)bitField, ParameterFormat.Short);
        }


        /// <summary>
        /// Append a constant parameter encoded byte in either short or long format. Note that if format is long parameter constant type will be a value
        /// </summary>
        /// <param name="value">Value to append</param>
        /// <param name="format">Use either short or long format</param>
        public void Append(byte value, ParameterFormat format)
        {
            if (format == ParameterFormat.Short)
            {
                if (value > (byte)ShortValueMax)
                {
                    value = (byte)ShortValueMax;
                }
                byte b = (byte)((byte)format | (byte)ParameterType.Constant | (byte)(value & ((byte)0x1f)) | (byte)ShortSign.Positive);
                Append(b);
            }
            else
            {
                byte b = (byte)((byte)format | (byte)ParameterType.Constant | (byte)ConstantParameterType.Value | (byte)FollowType.OneByte);
                Append(b);
                Append(value);
            }
        }

        /// <summary>
        /// Append a constant parameter encoded byte in either short or long format. Note that if format is long parameter constant type will be a value
        /// </summary>
        /// <param name="value">Value to append</param>
        /// <param name="format">Use either short or long format</param>
        public void Append(sbyte value, ParameterFormat format)
        {
            if (format == ParameterFormat.Short)
            {
                byte b = 0x00;
                if (value < 0)
                {
                    if (value < ShortValueMin)
                    {
                        value = ShortValueMin;
                    }
                    b = (byte)((byte)format | (byte)ParameterType.Constant | (byte)(value & ((byte)0x1f)));
                    b = (byte)((byte)ShortSign.Negative | b);
                }
                else
                {
                    if (value > ShortValueMax)
                    {
                        value = ShortValueMax;
                    }
                    b = (byte)((byte)format | (byte)ParameterType.Constant | (byte)(value & ((byte)0x1f)));
                    b = (byte)((byte)ShortSign.Positive | b);
                }
                Append(b);

            }
            else
            {
                byte b = (byte)((byte)format | (byte)ParameterType.Constant | (byte)ConstantParameterType.Value | (byte)FollowType.OneByte);
                Append(b);
                Append(value);
            }
        }

        /// <summary>
        /// Append a constant parameter encoded
        /// </summary>
        /// <param name="value">byte to append</param>
        /// <param name="type">User either value or lable type</param>
        public void Append(sbyte value, ConstantParameterType type)
        {
            Append(type, FollowType.OneByte);
            Append(value);
        }


        /// <summary>
        /// Append a constant parameter encoded
        /// </summary>
        /// <param name="value">byte to append</param>
        /// <param name="type">User either value or lable type</param>
        public void Append(byte value, ConstantParameterType type)
        {
            Append(type, FollowType.OneByte);
            Append(value);
        }

        /// <summary>
        /// Append a constant parameter encoded
        /// </summary>
        /// <param name="value">Int16 to append</param>
        /// <param name="type">User either value or lable type</param>
        public void Append(Int16 value, ConstantParameterType type)
        {
            Append(type, FollowType.TwoBytes);
            Append(value);
        }

        /// <summary>
        /// Append a constant parameter encoded
        /// </summary>
        /// <param name="value">Int32 to append</param>
        /// <param name="type">User either value or lable type</param>
        public void Append(Int32 value, ConstantParameterType type)
        {
            Append(type, FollowType.FourBytes);
            Append(value);
        }

        /// <summary>
        /// Append a constant parameter encoded
        /// </summary>
        /// <param name="value">UInt32 to append</param>
        /// <param name="type">User either value or lable type</param>
        public void Append(UInt32 value, ConstantParameterType type)
        {
            Append(type, FollowType.FourBytes);
            Append(value);
        }

        /// <summary>
        /// Append a constant parameter encoded
        /// </summary>
        /// <param name="value">Float to append</param>
        /// <param name="type">User either value or lable type</param>
        public void Append(float value, ConstantParameterType type)
        {
            Append(type, FollowType.FourBytes);
            Append(value);
        }



        /// <summary>
        /// Append a constant parameter encoded
        /// </summary>
        /// <param name="s">String to append</param>
        /// <param name="type">User either value or lable type</param>
        public void Append(string s, ConstantParameterType type)
        {
            Append(type, FollowType.TerminatedString2);
            Append(s);
        }

        /// <summary>
        /// Append a variable parameter encoded byte in short format 
        /// </summary>
        /// <param name="value">Value to append</param>
        /// <param name="scope">Select either global or local scope</param>
        public void Append(byte value, VariableScope scope)
        {
            byte b = (byte)((byte)ParameterFormat.Short | (byte)ParameterType.Variable | (byte)scope | (byte)(value & ((byte)0x1f)));
            Append(b);
        }

        private void Append(VariableScope scopeType, VariableType variableType, FollowType followType)
        {
            byte b = (byte)((byte)ParameterFormat.Long | (byte)ParameterType.Variable | (byte)scopeType | (byte)variableType | (byte)followType);
            Append(b);
        }

        /// <summary>
        /// Append a variable parameter encoded byte in long format 
        /// </summary>
        /// <param name="value">Value to append</param>
        /// <param name="scope">Select either global or local scope</param>
        /// <param name="type">Select either value or handle scope</param>
        public void Append(byte value, VariableScope scope, VariableType type)
        {
            Append(scope, type, FollowType.OneByte);
            Append(value);
        }

        /// <summary>
        /// Append a variable parameter encoded Int16
        /// </summary>
        /// <param name="value">Value to append</param>
        /// <param name="scope">Select either global or local scope</param>
        /// <param name="type">Select either value or handle scope</param>
        public void Append(Int16 value, VariableScope scope, VariableType type)
        {
            Append(scope, type, FollowType.TwoBytes);
            Append(value);
        }

        /// <summary>
        /// Append a variable parameter encoded Int32
        /// </summary>
        /// <param name="value">Value to append</param>
        /// <param name="scope">Select either global or local scope</param>
        /// <param name="type">Select either value or handle scope</param>
        public void Append(Int32 value, VariableScope scope, VariableType type)
        {
            Append(scope, type, FollowType.FourBytes);
            Append(value);
        }


        /// <summary>
        /// Append a variable parameter encoded string
        /// </summary>
        /// <param name="s">String to append</param>
        /// <param name="scope">Select either global or local scope</param>
        /// <param name="type">Select either value or handle scope</param>
        public void Append(string s, VariableScope scope, VariableType type)
        {
            Append(scope, type, FollowType.TerminatedString2);
            Append(s);
        }

        /// <summary>
        /// Append the specified longType and followType.
        /// </summary>
        /// <param name="longType">Long type.</param>
        /// <param name="followType">Follow type.</param>
        private void Append(ConstantParameterType longType, FollowType followType)
        {
            byte b = (byte)((byte)ParameterFormat.Long | (byte)ParameterType.Constant | (byte)longType | (byte)followType);
            Append(b);
        }

        internal void Print()
        {
            byte[] arr = Data;
            for (int i = 0; i < Length; i++)
            {
                Console.WriteLine("Command[" + i + "]: " + arr[i].ToString("X"));
            }
        }

    }


    /// <summary>
    /// Class for creating a EV3 reply
    /// </summary>
    [DebuggerDisplay("{CommandType}")]
    public class Reply : BrickReply
    {
        /// <summary>
        /// Gets a value indicating whether this instance has error.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has error; otherwise, <c>false</c>.
        /// </value>
        public bool HasError
        {
            get
            {
                CommandType type = (CommandType)dataArray[2];
                if (type == CommandType.DirectReply || type == CommandType.SystemReply)
                {
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        /// Gets the type of error.
        /// </summary>
        /// <value>
        /// The type of error
        /// </value>
        internal ErrorType ErrorType
        {
            get
            {
                return Error.ToErrorType(ErrorCode);
            }
        }

        /// <summary>
        /// Gets the error code.
        /// </summary>
        /// <value>
        /// The error code
        /// </value>
        public byte ErrorCode
        {
            get
            {
                if (HasError)
                {
                    if (CommandType == CommandType.SystemReplyWithError)
                    {
                        if (dataArray.Length >= 5)
                        {
                            byte error = dataArray[4];
                            if (Enum.IsDefined(typeof(EV3.BrickError), (int)error))
                            {
                                return error;
                            }
                        }
                    }
                    return (byte)BrickError.UnknownError;
                }
                return 0;//no error
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MonoBrick.EV3.Reply"/> class.
        /// </summary>
        public Reply()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MonoBrick.EV3.Reply"/> class.
        /// </summary>
        /// <param name='data'>
        /// The byte array to be used for the reply
        /// </param>
        public Reply(byte[] data)
        {
            dataArray = data;
            if (data.Length < 4)
            {
                throw new System.ArgumentException("Invalid EV3 Reply");
            }
            if (!Enum.IsDefined(typeof(CommandType), data[2]))
            {
                throw new System.ArgumentException("Invalid EV3 Reply");
            }
            CommandType type = (CommandType)data[2];
            if (type == CommandType.SystemReply)
            {
                if (!Enum.IsDefined(typeof(SystemCommand), data[3]))
                {
                    throw new System.ArgumentException("Invalid EV3 Reply");
                }
            }
            if (type == CommandType.SystemCommand || type == CommandType.SystemCommand)
            {
                throw new System.ArgumentException("Invalid EV3 Reply");
            }
        }


        /// <summary>
        /// Gets the EV3 system command.
        /// </summary>
        /// <value>The system command.</value>
        public SystemCommand SystemCommandType
        {
            get
            {
                if (CommandType == CommandType.SystemReply)
                {
                    return (SystemCommand)dataArray[3];
                }
                return SystemCommand.None;
            }
        }
        /// <summary>
        /// Gets the EV3 command type
        /// </summary>
        /// <value>The type of the command.</value>
        public CommandType CommandType
        {
            get { return (CommandType)dataArray[2]; }
        }

        /// <summary>
        /// Gets the sequence number.
        /// </summary>
        /// <value>The sequence number.</value>
        public UInt16 SequenceNumber
        {
            get { return (UInt16)(0x0000 | dataArray[0] | (dataArray[1] << 8)); } //!! was << 2
        }

        /// <summary>
        /// Gets the command byte as string.
        /// </summary>
        /// <value>
        /// The command byte as string
        /// </value>
        public string CommandTypeAsString
        {
            get { return BrickCommand.AddSpacesToString(CommandType.ToString()); }
        }

        internal void print()
        {
            Console.WriteLine("Command: " + CommandType.ToString());
            Console.WriteLine("Length: " + Length);
            Console.WriteLine("Errorcode: " + ErrorCode);
            for (int i = 0; i < Length; i++)
            {
                Console.WriteLine("Reply[" + i + "]: " + dataArray[i].ToString("X"));
            }

        }
    }

}

