﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoBrick.EV3
{
    /// <summary>
    /// Sensor mode when using a touch sensor
    /// </summary>
    public enum TouchMode
    {
        /// <summary>
        /// On or off mode
        /// </summary>
        Boolean = SensorMode.Mode0,
        /// <summary>
        /// Bump mode
        /// </summary>
        Count = SensorMode.Mode1
    };

    /// <summary>
    /// Colors that can be read from the  EV3 color sensor
    /// </summary>
    public enum EV3Color
    {
#pragma warning disable
        None = 0, Black = 1, Blue = 2, Green = 3,
        Yellow = 4, Red = 5, White = 6, Brown = 7
#pragma warning restore
    };

    public enum NXTColor
    {
#pragma warning disable
        Black = 0x01, Blue = 0x02, Green = 0x03,
        Yellow = 0x04, Red = 0x05, White = 0x06
#pragma warning restore
    };

    /// <summary>
    /// Sensor mode when using a EV3 color sensor
    /// </summary>
    public enum ColorMode
    {
        /// <summary>
        /// Use the color sensor to read reflected light
        /// </summary>
        Reflect = SensorMode.Mode0, // value0 : Reflected light intensity (0 to 100)

        /// <summary>
        /// Use the color sensor to detect the light intensity
        /// </summary>
        Ambient = SensorMode.Mode1, // value0 : Ambient light intensity (0 to 100)

        /// <summary>
        /// Use the color sensor to distinguish between eight different colors
        /// </summary>
        Color = SensorMode.Mode2, // value0 : Detected color (0 to 7)

        /// <summary>
        /// Read the raw value of the reflected light
        /// </summary>
        RawReflected = SensorMode.Mode3, // value0 : ??? (0 to 1020???) value1 : ??? (0 to 1020???)

        ///// <summary>
        ///// Activate the green light on the color sensor. Only works with the NXT Color sensor 
        ///// </summary>
        //NXTGreen = SensorMode.Mode3,

        ///// <summary>
        ///// Activate the green blue on the color sensor. Only works with the NXT Color sensor 
        ///// </summary>
        //NXTBlue = SensorMode.Mode4,

        //Raw = SensorMode.Mode5

        RGBRaw = SensorMode.Mode4, // value0 : Red (0 to 1020???), value1 : Green (0 to 1020???), value2 : Blue (0 to 1020???)

        ColorCal = SensorMode.Mode5, // Calibration ??? - sets LED color to red, flashing every 4 seconds, then goes continuous, 4 values ?? ?? ?? ??
                                     // This mode is not usable. When in COL-CAL mode, the color sensor does not respond to the keep-alive
                                     // sent from the EV3 brick. As a result, the sensor will time out and reset.
    };


    /// <summary>
    /// Sensor mode when using a EV3 IR Sensor
    /// </summary>
    public enum IRMode
    {
        /// <summary>
        /// Use the IR sensor as a distance sensor
        /// </summary>
        Proximity = SensorMode.Mode0,

        /// <summary>
        /// Use the IR sensor to detect the location of the IR Remote
        /// </summary>
        Seek = SensorMode.Mode1,

        /// <summary>
        /// Use the IR sensor to detect wich Buttons where pressed on the IR Remote
        /// </summary>
        Remote = SensorMode.Mode2,

    };

    /// <summary>
    /// Sensor mode when using a NXT light sensor
    /// </summary>
    public enum NXTLightMode
    {
        /// <summary>
        /// Use the lgith sensor to read reflected light
        /// </summary>
        Relection = SensorMode.Mode0,

        /// <summary>
        /// Use the light sensor to detect the light intensity
        /// </summary>
        Ambient = SensorMode.Mode1,
    };


    /// <summary>
    /// Sensor mode when using a sound sensor
    /// </summary>
    public enum SoundMode
    {
        /// <summary>
        /// The sound level is measured in A-weighting decibel
        /// </summary>
        SoundDBA = SensorMode.Mode1,
        /// <summary>
        /// The sound level is measured in decibel 
        /// </summary>
        SoundDB = SensorMode.Mode0
    };

    /// <summary>
    /// Sensor modes when using a ultrasonic sensor
    /// </summary>
    public enum UltrasonicMode
    {
#pragma warning disable
        /// <summary>
        /// Result will be in centimeter
        /// </summary>
        Centimeter = SensorMode.Mode0,
        /// <summary>
        /// Result will be in inch
        /// </summary>
        Inch = SensorMode.Mode1,

        /// <summary>
        /// Sensor is in listen mode
        /// </summary>
        Listen = SensorMode.Mode2
#pragma warning restore
    };

    /// <summary>
    /// Sensor modes when using a Temperature sensor
    /// </summary>
    public enum NXTTemperatureMode
    {
#pragma warning disable

        /// <summary>
        /// Result will be in celcius
        /// </summary>
        Celcius = SensorMode.Mode0,


        /// <summary>
        /// Result will be in fahrenheit 
        /// </summary>
        Fahrenheit = SensorMode.Mode1,
#pragma warning restore
    };

    /// <summary>
    /// Sensor modes when using a Gyro sensor
    /// </summary>
    public enum GyroMode
    {
#pragma warning disable
        /// <summary>
        /// Result will be in degrees
        /// </summary>
        Angle = SensorMode.Mode0,
        /// <summary>
        /// Result will be in degrees per second
        /// </summary>
        RotaSpeed = SensorMode.Mode1,
        RotaSpeedFast = SensorMode.Mode2,
        AngleAndRotaSpeed = SensorMode.Mode3,
        Calibration = SensorMode.Mode4,

        // http://docs.ev3dev.org/projects/lego-linux-drivers/en/ev3dev-stretch/sensor_data.html#lego-ev3-gyro-mode0-value0-direction
        TiltRotaSpeed = SensorMode.Mode5,
        TiltAngle = SensorMode.Mode6,
#pragma warning restore
    };

}

