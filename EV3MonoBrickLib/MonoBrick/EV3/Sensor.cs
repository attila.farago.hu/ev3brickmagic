using MonoBrick;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MonoBrick.EV3
{
    /// <summary>
    /// Sensor ports
    /// </summary>
    public enum SensorPort
    {
#pragma warning disable
        In1 = 0, In2 = 1, In3 = 2, In4 = 3,
        OutA = 0x10, OutB = 0x11, OutC = 0x12, OutD = 0x13
#pragma warning restore
    };

    /// <summary>
    /// Device types
    /// </summary>
    public enum SensorType
    {
#pragma warning disable
        Keep = 0, //!< Type value that won't change type in byte codes
        NXTTouch = 1, NXTLight = 2, NXTSound = 3, NXTColor = 4, NXTUltraSonic = 5, NXTTemperature = 6,
        LMotor = 7, MMotor = 8,
        //TYPE_NEWTACHO                 =   9,  //!< Device is a new tacho motor
        Touch = 16, Test = 21,
        Color = 29, UltraSonic = 30, Gyro = 32, IR = 33,
        Energy = 99,
        I2C = 100, NXTTest = 101,
        NXT_IIC = 123,  //!< Device is NXT IIC sensor
        Terminal = 124, //!< Port is connected to a terminal
        Unknown = 125, //!< Port not empty but type has not been determined
        None = 126,
        Error = 127,  //!< Port not empty and type is invalid
        OtherUnknown = 129,
#pragma warning restore
    };

    /// <summary>
    /// Sensor modes
    /// </summary>
    public enum SensorMode
    {
#pragma warning disable
        Mode0 = 0, Mode1 = 1, Mode2 = 2, Mode3 = 3, Mode4 = 4, Mode5 = 5, Mode6 = 6
#pragma warning restore
    };

    /// <summary>
    /// Class for creating a sensor 
    /// </summary>
    public class Sensor : ISensor
    {
        public SensorInfo SensorInfo
        {
            get
            {
                if (m_SensorInfo == null)
                    m_SensorInfo = this.GetSensorInfo();
                return m_SensorInfo;
            }
        }
        private SensorInfo m_SensorInfo;

        private Dictionary<SensorType, Type> DictSensorModes = new Dictionary<SensorType, Type>()
        {
            [SensorType.Color] = typeof(ColorMode),
            [SensorType.Gyro] = typeof(GyroMode),
            [SensorType.IR] = typeof(IRMode),
            [SensorType.UltraSonic] = typeof(UltrasonicMode),
            [SensorType.NXTTemperature] = typeof(NXTTemperatureMode),
            [SensorType.NXTLight] = typeof(NXTLightMode),
            [SensorType.Touch] = typeof(TouchMode),
        };

        public String GetSensorModeString()
        {
            if (DictSensorModes.ContainsKey(SensorType))
            {
                var tSensorMode = DictSensorModes[SensorType];
                var sSensorMode = Enum.GetName(tSensorMode, mode);
                return sSensorMode ?? mode.ToString();
            }
            else
            {
                return mode.ToString();
            }
        }

        internal SensorPort Port { get; set; }

        internal Connection<Command, Reply> Connection { get; set; }

        /// <summary>
        /// Gets or sets the daisy chain layer.
        /// </summary>
        /// <value>The daisy chain layer.</value>
        public DaisyChainLayer DaisyChainLayer { get; set; }

        public SensorType SensorType
        {
            get
            {
                return m_SensorType ?? GetSensorType();
            }
        }
        private SensorType? m_SensorType = null;
        public SensorMode SensorMode
        {
            get
            {
                return m_SensorMode ?? GetSensorMode();
            }
            set
            {
                m_SensorMode = value;
            }
        }
        private SensorMode? m_SensorMode = null;

        /// <summary>
        /// Gets the sensor type
        /// </summary>
        /// <returns>The type.</returns>
        public SensorType GetSensorType()
        {
            //if (!isInitialized)
            //    Initialize();
            SensorType type;
            SensorMode mode;
            GetTypeAndMode(out type, out mode);
            return type;
        }

        /// <summary>
        /// Gets the sensor mode.
        /// </summary>
        /// <returns>The mode.</returns>
        public SensorMode GetSensorMode()
        {
            SensorType type;
            SensorMode mode;
            GetTypeAndMode(out type, out mode);
            return mode;
        }

        /// <summary>
        /// Gets the sensor type and mode.
        /// </summary>
        /// <param name="type">Type.</param>
        /// <param name="mode">Mode.</param>
        protected void GetTypeAndMode(out SensorType type, out SensorMode mode)
        {
            //if (!isInitialized)
            //    Initialize();
            var command = new Command(2, 0, 200, true);
            command.Append(ByteCodes.INPUT_DEVICE);
            command.Append(InputSubCodes.GetTypeMode);
            command.Append(this.DaisyChainLayer);
            command.Append(this.Port);
            command.Append((byte)0, VariableScope.Global);
            command.Append((byte)1, VariableScope.Global);
            var reply = Connection.SendAndReceive(command);
            Error.CheckForError(reply, 200);
            type = (SensorType)reply.GetByte(3);
            mode = (SensorMode)reply.GetByte(4);

            m_SensorType = type;
            m_SensorMode = mode;
        }

        /// <summary>
        /// Gets the name of the sensor
        /// </summary>
        /// <returns>The name.</returns>
        public virtual string GetName()
        {
            if (!isInitialized)
                Initialize();
            var command = new Command(Command.ShortValueMax, 0, 201, true);
            command.Append(ByteCodes.INPUT_DEVICE);
            command.Append(InputSubCodes.GetName);
            command.Append(this.DaisyChainLayer);
            command.Append(this.Port);
            command.Append(Command.ShortValueMax, ConstantParameterType.Value);
            command.Append((byte)0, VariableScope.Global);
            var reply = Connection.SendAndReceive(command);
            Error.CheckForError(reply, 201);
            return reply.GetString(3, (byte)Command.ShortValueMax);
        }

        /// <summary>
        /// Get device symbol
        /// </summary>
        /// <returns>The symbole.</returns>
        public virtual string GetSymbole()
        {
            if (!isInitialized)
                Initialize();
            var command = new Command(Command.ShortValueMax, 0, 201, true);
            command.Append(ByteCodes.INPUT_DEVICE);
            command.Append(InputSubCodes.GetSymbol);
            command.Append(this.DaisyChainLayer);
            command.Append(this.Port);
            command.Append(Command.ShortValueMax, ConstantParameterType.Value);
            command.Append((byte)0, VariableScope.Global);
            var reply = Connection.SendAndReceive(command);
            Error.CheckForError(reply, 201);
            return reply.GetString(3, (byte)Command.ShortValueMax);
        }

        /// <summary>
        /// Get sensor format
        /// </summary>
        /// <returns>The format.</returns>
        protected string GetFormat()
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Read the raw sensor value
        /// </summary>
        /// <returns>The raw sensor value.</returns>
        public Int32 GetRaw()
        {
            if (!isInitialized)
                Initialize();
            var command = new Command(4, 0, 201, true);
            command.Append(ByteCodes.INPUT_DEVICE);
            command.Append(InputSubCodes.GetRaw);
            command.Append(this.DaisyChainLayer);
            command.Append(this.Port);
            command.Append((byte)0, VariableScope.Global);
            var reply = Connection.SendAndReceive(command);
            Error.CheckForError(reply, 201);
            return reply.GetInt32(3);
        }

        /// <summary>
        /// Read the raw sensor value
        /// </summary>
        /// <returns>The raw sensor value.</returns>
        public Int32[] GetRaw(int numvalues)
        {
            if (!isInitialized)
                Initialize();
            var command = new Command(4 * numvalues, 0, 201, true);
            command.Append(ByteCodes.INPUT_DEVICE);
            command.Append(InputSubCodes.ReadyRaw);
            command.Append(this.DaisyChainLayer);
            command.Append(this.Port);
            command.Append(0, ConstantParameterType.Value); //Specify device type (0 = Don�t change type)
            command.Append(-1, ConstantParameterType.Value); //Device mode [0-7] (-1 = Don�t change mode) 
            command.Append((byte)numvalues, ConstantParameterType.Value); //VALUES � Number of return values
            for (int i = 0; i < numvalues; i++)
                command.Append((byte)(i * 4), VariableScope.Global);
            var reply = Connection.SendAndReceive(command);
            Error.CheckForError(reply, command.SequenceNumber);
            var retval = new Int32[numvalues];
            for (int i = 0; i < numvalues; i++)
                retval[i] = reply.GetInt32(3 + i * 4);
            return retval;
        }

        /// <summary>
        /// Get device mode name
        /// </summary>
        /// <returns>The device mode name.</returns>
        protected string GetModeName()
        {
            return GetModeName(this.mode);
        }

        /// <summary>
        /// Get device mode name
        /// </summary>
        /// <returns>The device mode name.</returns>
        /// <param name="mode">Mode to get name of.</param>
        protected string GetModeName(SensorMode mode)
        {
            if (!isInitialized)
                Initialize();
            var command = new Command(Command.ShortValueMax, 0, 201, true);
            command.Append(ByteCodes.INPUT_DEVICE);
            command.Append(InputSubCodes.GetModeName);
            command.Append(this.DaisyChainLayer);
            command.Append(this.Port);
            command.Append((byte)mode, ConstantParameterType.Value);

            command.Append(Command.ShortValueMax, ConstantParameterType.Value);
            command.Append((byte)0, VariableScope.Global);
            var reply = Connection.SendAndReceive(command);
            Error.CheckForError(reply, 201);
            return reply.GetString(3, (byte)Command.ShortValueMax);
        }

        /// <summary>
        /// Gets figure layout.
        /// </summary>
        /// <param name="figures">Figures.</param>
        /// <param name="decimals">Decimals.</param>
        protected void GetFigures(out byte figures, out byte decimals)
        {
            if (!isInitialized)
                Initialize();
            var command = new Command(2, 0, 201, true);
            command.Append(ByteCodes.INPUT_DEVICE);
            command.Append(InputSubCodes.GetFigures);
            command.Append(this.DaisyChainLayer);
            command.Append(this.Port);
            command.Append((byte)0, VariableScope.Global);
            command.Append((byte)1, VariableScope.Global);
            var reply = Connection.SendAndReceive(command);
            Error.CheckForError(reply, 201);
            figures = reply.GetByte(3);
            decimals = reply.GetByte(4);
        }

        /// <summary>
        /// Gets the min and max values that can be returned.
        /// </summary>
        /// <param name="min">Minimum.</param>
        /// <param name="max">Maxium.</param>
        protected void GetMinMax(out float min, out float max)
        {
            if (!isInitialized)
                Initialize();
            var command = new Command(8, 0, 201, true);
            command.Append(ByteCodes.INPUT_DEVICE);
            command.Append(InputSubCodes.GetMinMax);
            command.Append(this.DaisyChainLayer);
            command.Append(this.Port);
            command.Append((byte)0, VariableScope.Global);
            command.Append((byte)4, VariableScope.Global);
            var reply = Connection.SendAndReceive(command);
            Error.CheckForError(reply, 201);
            min = reply.GetFloat(3);
            max = reply.GetFloat(7);
        }

        /// <summary>
        /// </summary>
        protected byte ReadyPct()
        {
            if (!isInitialized)
                Initialize();
            var command = new Command(1, 0, 201, true);
            command.Append(ByteCodes.INPUT_DEVICE);
            command.Append(InputSubCodes.ReadyPCT);
            command.Append(this.DaisyChainLayer);
            command.Append(this.Port);
            command.Append(0, ConstantParameterType.Value);
            //command.Append(mode);
            command.Append(-1, ConstantParameterType.Value); //� Device mode [0-7] (-1 = Don�t change mode) 
            command.Append(1, ConstantParameterType.Value);
            command.Append((byte)0, VariableScope.Global);
            var reply = Connection.SendAndReceive(command);
            Error.CheckForError(reply, 201);
            return reply.GetByte(3);
        }

        /// <summary>
        /// </summary>
        protected Int32[] ReadyRaw(byte numvalues)
        {
            if (!isInitialized)
                Initialize();
            var command = new Command(4 * numvalues, 0, 201, true);
            command.Append(ByteCodes.INPUT_DEVICE);
            command.Append(InputSubCodes.ReadyRaw);
            command.Append(this.DaisyChainLayer);
            command.Append(this.Port);
            command.Append(0, ConstantParameterType.Value);
            //command.Append(mode);
            command.Append(-1, ConstantParameterType.Value); //� Device mode [0-7] (-1 = Don�t change mode) 
            command.Append(numvalues, ConstantParameterType.Value);
            for (int i = 0; i < numvalues; i++) command.Append((byte)0, VariableScope.Global);
            var reply = Connection.SendAndReceive(command);
            Error.CheckForError(reply, 201);
            Int32[] retval = new Int32[numvalues];
            for (int i = 0; i < numvalues; i++) retval[i] = reply.GetInt32(3 + i * 4);
            return retval;
        }

        /// <summary>
        /// </summary>
        protected float ReadySi()
        {
            if (!isInitialized)
                Initialize();
            var command = new Command(4, 0, 201, true);
            command.Append(ByteCodes.INPUT_DEVICE);
            command.Append(InputSubCodes.ReadySI);
            command.Append(this.DaisyChainLayer);
            command.Append(this.Port);
            command.Append(0, ConstantParameterType.Value);
            //command.Append(mode);
            command.Append(-1, ConstantParameterType.Value); //� Device mode [0-7] (-1 = Don�t change mode) 
            command.Append(1, ConstantParameterType.Value);
            command.Append((byte)0, VariableScope.Global);
            var reply = Connection.SendAndReceive(command);
            Error.CheckForError(reply, 201);
            return reply.GetFloat(3);
        }

        /// <summary>
        /// Get positive changes since last clear
        /// </summary>
        /// <returns>The changes.</returns>
        protected float GetChanges()
        {
            if (!isInitialized)
                Initialize();
            var command = new Command(4, 0, 201, true);
            command.Append(ByteCodes.INPUT_DEVICE);
            command.Append(InputSubCodes.GetChanges);
            command.Append(this.DaisyChainLayer);
            command.Append(this.Port);
            command.Append((byte)0, VariableScope.Global);
            var reply = Connection.SendAndReceive(command);
            Error.CheckForError(reply, 201);
            return reply.GetFloat(3);
        }

        /// <summary>
        /// Get the bolean count since the last clear
        /// </summary>
        /// <returns>The bumb count.</returns>
        protected float GetBumbs()
        {
            if (!isInitialized)
                Initialize();
            var command = new Command(4, 0, 201, true);
            command.Append(ByteCodes.INPUT_DEVICE);
            command.Append(InputSubCodes.GetBumps);
            command.Append(this.DaisyChainLayer);
            command.Append(this.Port);
            command.Append((byte)0, VariableScope.Global);
            var reply = Connection.SendAndReceive(command);
            Error.CheckForError(reply, 201);
            return reply.GetFloat(3);
        }

        /// <summary>
        /// Clear changes and bumps
        /// </summary>
        protected void ClearChanges()
        {
            if (!isInitialized)
                Initialize();
            var command = new Command(0, 0, 201, false);
            command.Append(ByteCodes.INPUT_DEVICE);
            command.Append(InputSubCodes.ClrChanges);
            command.Append(this.DaisyChainLayer);
            command.Append(this.Port);
            Connection.Send(command);
        }

        /// <summary>
        /// Apply new minimum and maximum raw value for device type to be used in scaling PCT and SI
        /// </summary>
        /// <param name="min">Minimum.</param>
        /// <param name="max">Max.</param>
        protected void CalcMinMax(UInt32 min, UInt32 max)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Apply new minimum raw value for device type to be used in scaling PCT and SI
        /// </summary>
        /// <param name="min">Minimum.</param>
        protected void CalcMin(UInt32 min)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Apply new maximum raw value for device type to be used in scaling PCT and SI
        /// </summary>
        /// <param name="max">Max.</param>
        protected void CalcMax(UInt32 max)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Apply the default minimum and maximum raw value for device type to be used in scaling PCT and SI
        /// </summary>
        protected void CalcDefault()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Generic setup/read IIC sensors
        /// </summary>
        /// <returns>DATA8 array (handle) to read into</returns>
        /// <param name="repeat">Repeat setup/read "REPEAT" times (0 = infinite)</param>
        /// <param name="reapeatTime">Time between repeats [10..1000mS] (0 = 10)</param>
        /// <param name="writeData">Byte array to write</param>
        protected byte SetUp(byte repeat, Int16 reapeatTime, byte[] writeData)
        {
            if (!isInitialized)
                Initialize();
            var command = new Command(4, 0, 201, true);
            command.Append(ByteCodes.INPUT_DEVICE);
            command.Append(InputSubCodes.Setup);
            command.Append(this.DaisyChainLayer);
            command.Append(this.Port);
            command.Append(repeat, ConstantParameterType.Value);
            command.Append(repeat, ConstantParameterType.Value);
            command.Append(writeData.Length, ConstantParameterType.Value);
            command.Append(1, ConstantParameterType.Value);
            command.Append((byte)0, VariableScope.Global);
            var reply = Connection.SendAndReceive(command);
            Error.CheckForError(reply, 201);
            return reply.GetByte(3);

        }

        /// <summary>
        /// Clear all devices (e.c. counters, angle, ...)
        /// </summary>
        protected void ClearAll()
        {
            if (!isInitialized)
                Initialize();
            var command = new Command(0, 0, 201, false);
            command.Append(ByteCodes.INPUT_DEVICE);
            command.Append(InputSubCodes.ClearAll);
            command.Append(this.DaisyChainLayer);
            Connection.Send(command);
        }
        /// <summary>
        /// Stop all devices (e.c. motors, ...)
        /// </summary>
        protected void StopAll()
        {
            if (!isInitialized)
                Initialize();
            var command = new Command(0, 0, 201, false);
            command.Append(ByteCodes.INPUT_DEVICE);
            command.Append(InputSubCodes.StopAll);
            command.Append(this.DaisyChainLayer);
            Connection.Send(command);
        }

        /// <summary>
        /// Read a sensor value
        /// </summary>
        /// <returns>The sensor value.</returns>
        protected byte GetReadPct()
        {
            if (!isInitialized)
                Initialize();
            var command = new Command(4, 0, 123, true);
            command.Append(ByteCodes.INPUT_READ);
            command.Append(this.DaisyChainLayer);
            command.Append(this.Port);
            command.Append(0, ConstantParameterType.Value); //Specify device type (0 = Don�t change type)
            //command.Append(mode);
            command.Append(-1, ConstantParameterType.Value); //� Device mode [0-7] (-1 = Don�t change mode) 
            command.Append((byte)0, VariableScope.Global);
            var reply = Connection.SendAndReceive(command);
            Error.CheckForError(reply, 123);
            //TODO: check for error read
            return reply[3];
        }

        /// <summary>
        /// Reads the si sensor value
        /// </summary>
        /// <returns>The si sensor value.</returns>
        protected float ReadSi()
        {
            if (!isInitialized)
                Initialize();
            var command = new Command(4, 0, 123, true);
            command.Append(ByteCodes.INPUT_READSI);
            command.Append(this.DaisyChainLayer);
            command.Append(this.Port);
            command.Append(0, ConstantParameterType.Value);
            //command.Append(mode);
            command.Append(-1, ConstantParameterType.Value); //� Device mode [0-7] (-1 = Don�t change mode) 
            command.Append((byte)0, VariableScope.Global);
            var reply = Connection.SendAndReceive(command);
            Error.CheckForError(reply, 123);
            return reply.GetFloat(3);
        }

        /// <summary>
        /// Sets the sensor mode
        /// </summary>
        /// <param name="mode">Mode to use.</param>
        public void SetMode(SensorMode mode)
        {
            isInitialized = true;
            this.mode = mode;
            var command = new Command(4, 0, 123, true);
            command.Append(ByteCodes.INPUT_READSI);
            command.Append(this.DaisyChainLayer);
            command.Append(this.Port);
            command.Append(0, ConstantParameterType.Value);
            command.Append(mode); //set mode
            command.Append((byte)0, VariableScope.Global);
            var reply = Connection.SendAndReceive(command);

            m_SensorInfo = GetSensorInfo();
            Error.CheckForError(reply, 123);
        }

        /// <summary>
        /// Gete detailed sensor base information
        /// </summary>
        /// <returns></returns>
        public SensorInfo GetSensorInfo()
        {
            var command = new Command(100, 0, 123, true); //TODO: 100 is surely enough to hold, should calc the exact number
            int globalsoffset = 0;

            command.Append(ByteCodes.INPUT_DEVICE);
            command.Append(InputSubCodes.GetTypeMode);
            command.Append(this.DaisyChainLayer);
            command.Append(this.Port);
            command.Append((byte)globalsoffset++, VariableScope.Global); //(Data8) TYPE � See device type list (Please reference section 0)
            command.Append((byte)globalsoffset++, VariableScope.Global); //(Data8) MODE � Device mode [0-7]

            command.Append(ByteCodes.INPUT_DEVICE);
            command.Append(InputSubCodes.GetFigures);
            command.Append(this.DaisyChainLayer);
            command.Append(this.Port);
            command.Append((byte)globalsoffset++, VariableScope.Global); //(Data8) FIGURES � Total number of figures (Inclusive decimal points and decimals)
            command.Append((byte)globalsoffset++, VariableScope.Global); //(Data8) DECIMALES � Number of decimals

            command.Append(ByteCodes.INPUT_DEVICE);
            command.Append(InputSubCodes.GetMinMax);
            command.Append(this.DaisyChainLayer);
            command.Append(this.Port);
            command.Append((byte)globalsoffset, VariableScope.Global); //(DataF) MIN � Minimum SI value
            globalsoffset += 4;
            command.Append((byte)globalsoffset, VariableScope.Global); //(DataF) MAX � Maximum SI value
            globalsoffset += 4;

            command.Append(ByteCodes.INPUT_DEVICE);
            command.Append(InputSubCodes.GetConnection);
            command.Append(this.DaisyChainLayer);
            command.Append(this.Port);
            command.Append((byte)globalsoffset++, VariableScope.Global); //(Data8)CONN � Connection type, see documentation below

            command.Append(ByteCodes.INPUT_DEVICE);
            command.Append(InputSubCodes.GetFormat);
            command.Append(this.DaisyChainLayer);
            command.Append(this.Port);
            command.Append((byte)globalsoffset++, VariableScope.Global); //(Data8) Datasets � Number of data sets
            command.Append((byte)globalsoffset++, VariableScope.Global); //(Data8) FORMAT � Format [0-3], (0: 8-bit, 1: 16-bit, 2: 32-bit, 3: Float point)
            command.Append((byte)globalsoffset++, VariableScope.Global); //(Data8) MODES � Number of modes [1-8]
            command.Append((byte)globalsoffset++, VariableScope.Global); //(Data8) VIEW � Number of modes visible within port view app [1-8]

            //command.Append(ByteCodes.INPUT_DEVICE);
            //command.Append(InputSubCodes.GetName);
            //command.Append(this.DaisyChainLayer);
            //command.Append(this.Port);
            //command.Append(Command.ShortValueMax, ConstantParameterType.Value);
            //command.Append((byte)globalvar, VariableScope.Global); //(Data8) DESTINATION � String variable or handle to string
            //globalvar += Command.ShortValueMax;
            //TODO: only one string is tolerated somehow

            command.Append(ByteCodes.INPUT_DEVICE);
            command.Append(InputSubCodes.GetSymbol);
            command.Append(this.DaisyChainLayer);
            command.Append(this.Port);
            command.Append(Command.ShortValueMax, ConstantParameterType.Value);
            command.Append((byte)globalsoffset, VariableScope.Global); //(Data8) DESTINATION � String variable or handle to string
            globalsoffset += Command.ShortValueMax;

            //command.Append(ByteCodes.INPUT_DEVICE);
            //command.Append(InputSubCodes.GetModeName);
            //command.Append(this.DaisyChainLayer);
            //command.Append(this.Port);
            //command.Append((byte)mode, ConstantParameterType.Value);

            var reply = Connection.SendAndReceive(command);
            Error.CheckForError(reply, 123);

            short offset = 3;

            //var devicename = reply.GetString(3+0, (byte)Command.ShortValueMax);
            var type = (SensorType)reply.GetByte(offset); offset++;
            var mode = (SensorMode)reply.GetByte(offset); offset++;
            byte figures = reply.GetByte(offset); offset++;
            byte decimales = reply.GetByte(offset); offset++;
            float minvalue = reply.GetFloat(offset); offset += 4;
            float maxvalue = reply.GetFloat(offset); offset += 4;
            byte connection = reply.GetByte(offset); offset++;
            byte datasets = reply.GetByte(offset); offset++;
            var format = (SensorInfoFormat)reply.GetByte(offset); offset++;
            byte modes = reply.GetByte(offset); offset++;
            byte modesview = reply.GetByte(offset); offset++;
            //string name = reply.GetString(offset, (byte)Command.ShortValueMax).TrimEnd(' ', '\0'); offset += Command.ShortValueMax;
            string symbol = reply.GetString(offset, (byte)Command.ShortValueMax).TrimEnd(' ', '\0'); offset += Command.ShortValueMax;
            var sensorInfo = new SensorInfo()
            {
                Type = type,
                Mode = mode,
                Figures = figures,
                Decimales = decimales,
                Minvalue = minvalue,
                Maxvalue = maxvalue,
                Connection = connection,
                Datasets = datasets,
                Format = format,
                Modes = modes,
                Modesview = modesview,
                Symbol = symbol
            };
            return sensorInfo;

            /* Connection type:
            0x6F : CONN_UNKNOW : Fake
            0x75 : CONN_DAISYCHAIN : Daisy chained
            0x76 : CONN_NXT_COLOR : NXT Color sensor
            0x77 : CONN_NXT_DUMB : NXT analog sensor
            0x78 : CONN_NXT_IIC : NXT IIC sensor
            0x79 : CONN_INPUT_DUMB : EV3 input device with ID resistor
            0x7A : CONN_INPUT_UART : EV3 input UART sensor
            0x7B: CONN_OUTPUT_DUMB : EV3 output device with ID resistor
            0x7C: CONN_OUTPUT_INTELLIGENT : EV3 output device with communication
            0x7D: CONN_OUTPUT_TACHO : EV3 Tacho motor with ID resistor
            0x7E: CONN_NONE : Port empty or not available
            0x7F: CONN_ERROR : Port not empty and type is invalid
             */
        }


        /// <summary>
        /// Wait for device ready (wait for valid data)
        /// </summary>
        /// <returns><c>true</c> if this instance is ready; otherwise, <c>false</c>.</returns>
        protected bool IsReady()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Write data to device (only UART devices)
        /// </summary>
        /// <param name="data">Data array to write.</param>
        protected void Write(byte[] data)
        {
            if (!isInitialized)
                Initialize();
            var command = new Command(0, 0, 100, false);
            command.Append(ByteCodes.INPUT_WRITE);
            command.Append(this.DaisyChainLayer);
            command.Append(this.Port);
            command.Append((byte)data.Length, ParameterFormat.Short);
            foreach (byte b in data)
                command.Append(b);
            Connection.Send(command);
        }
        /// <summary>
        /// If initialized has been called
        /// </summary>
        protected bool isInitialized = false;

        /// <summary>
        /// Holds the sensor mode that is used.
        /// </summary>
        protected SensorMode mode { get { return SensorMode; } set { SensorMode = value; } } //!!

        /// <summary>
        /// Initializes a new instance of the <see cref="MonoBrick.EV3.Sensor"/> class.
        /// </summary>
        public Sensor()
        {
            mode = SensorMode.Mode0;
            if (Connection != null && Connection.IsConnected)
                Initialize();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MonoBrick.EV3.Sensor"/> class.
        /// </summary>
        /// <param name="mode">Mode to use.</param>
        public Sensor(SensorMode mode)
        {
            this.mode = mode;
            if (Connection != null && Connection.IsConnected)
                Initialize();
        }

        #region ISensor

        /// <summary>
        /// Initialize the sensor
        /// </summary>
        public void Initialize()
        {
            isInitialized = true;
            m_SensorMode = null;
            m_SensorType = null;
            m_SensorInfo = null;
            SetMode(mode);
            ReadyRaw(1);
        }

        /// <summary>
        /// Reads the sensor value as a string.
        /// </summary>
        /// <returns>
        /// The value as a string
        /// </returns>
        public virtual string ReadAsString()
        {
            switch (SensorType)
            {
                case SensorType.Touch:
                    return Touch_ReadAsString();
                case SensorType.Color:
                case SensorType.NXTColor:
                    return Color_ReadAsString();
                case SensorType.Gyro:
                    return Gyro_ReadAsString();
                //case SensorType.IR:
                //    return IRSensor_ReadAsString();
                case SensorType.NXTLight:
                    return NXTLightSensor_ReadAsString();
                //case SensorType.NXTUltraSonic:
                //    return Ultrasonic_ReadAsString();
                case SensorType.NXTSound:
                    return Sound_ReadAsString();
                case SensorType.NXTTemperature:
                    return Temperature_ReadAsString();
                default:
                    string symbol = !string.IsNullOrEmpty(SensorInfo.Symbol) ? " " + SensorInfo.Symbol : null;
                    return ReadSi().ToString() + symbol;
            }
        }
        #endregion

        /// <summary>
        /// Reads the sensor value as a string.
        /// </summary>
        /// <returns>The value as a string</returns>
        public string Touch_ReadAsString()
        {
            string s = "";
            if (SensorMode == (SensorMode)TouchMode.Count)
            {
                s = Touch_Read() + " count";
            }
            if (SensorMode == (SensorMode)TouchMode.Boolean)
            {
                if (Convert.ToBoolean(ReadSi()))
                {
                    s = "On";
                }
                else
                {
                    s = "Off";
                }
            }
            return s;
        }

        /// <summary>
        /// Read the value. In bump mode this will return the count
        /// </summary>
        public int Touch_Read()
        {
            int value = 0;
            if (SensorMode == (SensorMode)TouchMode.Count)
            {
                value = (int)GetBumbs();
            }
            if (SensorMode == (SensorMode)TouchMode.Boolean)
            {
                if (GetReadPct() > 50)
                {
                    value = 1;
                }
                else
                {
                    value = 0;
                }
            }
            return value;
        }

        /// <summary>
        /// Reset the bumb count
        /// </summary>
        public void Touch_Reset()
        {
            this.ClearChanges();
        }

        /// <summary>
        /// Gets or sets the mode.
        /// </summary>
        /// <value>The mode.</value>
        public TouchMode TouchMode
        {
            get { return (TouchMode)this.SensorMode; }
            set { SetMode((SensorMode)value); }
        }

        /// <summary>
        /// Gets or sets the color mode.
        /// </summary>
        /// <value>The color mode.</value>
        public ColorMode ColorMode
        {
            get { return (ColorMode)this.mode; }
            set { SetMode((SensorMode)value); }
        }

        /// <summary>
        /// Reads the sensor value as a string.
        /// </summary>
        /// <returns>The value as a string</returns>
        public string Color_ReadAsString()
        {
            string symbol = !string.IsNullOrEmpty(SensorInfo.Symbol) ? " " + SensorInfo.Symbol : null;
            switch (SensorMode)
            {
                case (SensorMode)ColorMode.Color:
                    if (SensorType == SensorType.Color)
                    {
                        return Color_ReadColor().ToString();
                    }
                    else if (SensorType == SensorType.NXTColor)
                    {
                        int value = GetRaw();
                        return ((NXTColor)value).ToString();
                    }
                    else return GetReadPct().ToString();
                //break;
                //case (SensorMode)ColorMode.Ambient:
                //case (SensorMode)ColorMode.Reflect:
                //    return GetRead().ToString() + symbol;
                //case (SensorMode)ColorMode.RGBRaw:
                //    var val1 = ReadyRaw(3);
                //    return string.Join(",", val1);
                default:
                    if (SensorInfo.Datasets == 1)
                        return GetReadPct().ToString() + symbol;
                    else
                    {
                        //TODO: based on format do different things (raw/si/pct reading - not clear how this is indicated)
                        int[] data = GetRaw(SensorInfo.Datasets);
                        return string.Join(",", data.Select(elem => elem.ToString()).ToArray()) + symbol;
                    }
            };
        }

        /// <summary>
        /// Read the intensity of the reflected light
        /// </summary>
        public int Color_Read()
        {
            int value = 0;
            switch (mode)
            {
                case (SensorMode)ColorMode.Ambient:
                    value = GetReadPct();
                    break;
                case (SensorMode)ColorMode.Reflect:
                    value = GetReadPct();
                    break;
                case (SensorMode)ColorMode.RawReflected:
                    if (SensorType == SensorType.NXTColor)
                    {
                        if (((ColorMode)mode) == ColorMode.RawReflected)
                        {
                            SetMode(SensorMode.Mode5);
                        }
                    }
                    value = GetRaw();
                    break;
                case (SensorMode)ColorMode.Color: //fallback, not going to happen
                default:
                    value = GetRaw();
                    break;
            }
            return value;
        }

        /// <summary>
        /// Reads the color.
        /// </summary>
        /// <returns>The color.</returns>
        public EV3Color Color_ReadColor()
        {
            EV3Color color = EV3Color.None;
            if (SensorMode == (SensorMode)ColorMode.Color)
            {
                color = (EV3Color)GetRaw();
            }
            return color;
        }

        /// <summary>
        /// Gets or sets the IR mode.
        /// </summary>
        /// <value>The mode.</value>
        public IRMode IRSensor_Mode
        {
            get { return (IRMode)this.mode; }
            set { SetMode((SensorMode)value); }
        }

        /// <summary>
        /// Reads the sensor value as a string.
        /// </summary>
        /// <returns>The value as a string</returns>
        public string IRSensor_ReadAsString()
        {
            string s = "";
            switch (mode)
            {
                case (SensorMode)IRMode.Proximity:
                    s = IRSensor_Read().ToString();
                    break;
                case (SensorMode)IRMode.Remote:
                    s = IRSensor_Read().ToString();
                    break;
                case (SensorMode)IRMode.Seek:
                    s = IRSensor_Read().ToString();
                    break;
            }
            return s;
        }

        /// <summary>
        /// Read the value of the sensor. The result will vary depending on the mode
        /// </summary>
        public int IRSensor_Read()
        {
            int value = 0;
            switch (mode)
            {
                case (SensorMode)IRMode.Proximity:
                    value = GetReadPct();
                    break;
                case (SensorMode)IRMode.Remote:
                    value = GetRaw();
                    break;
                case (SensorMode)IRMode.Seek:
                    value = GetRaw();
                    break;
            }
            return value;
        }

        /*public void SetChannel (int i)
        {
            throw new NotImplementedException();
            //byte[] data = { 0x12, 0x01};
            //Write(data);
        }*/

        /// <summary>
        /// Gets or sets the light mode.
        /// </summary>
        /// <value>The mode.</value>
        public NXTLightMode NXTLightSensor_Mode
        {
            get { return (NXTLightMode)this.mode; }
            set { SetMode((SensorMode)value); }
        }

        /// <summary>
        /// Reads the sensor value as a string.
        /// </summary>
        /// <returns>The value as a string</returns>
        public string NXTLightSensor_ReadAsString()
        {
            string s = "";
            switch (mode)
            {
                case (SensorMode)NXTLightMode.Ambient:
                    s = NXTLightSensor_Read().ToString();
                    break;
                case (SensorMode)NXTLightMode.Relection:
                    s = NXTLightSensor_Read().ToString();
                    break;
            }
            return s;
        }

        /// <summary>
        /// Read this instance.
        /// </summary>
        public int NXTLightSensor_Read()
        {
            int value = 0;
            switch (mode)
            {
                case (SensorMode)NXTLightMode.Ambient:
                    value = GetReadPct();
                    break;
                case (SensorMode)NXTLightMode.Relection:
                    value = GetReadPct();
                    break;
            }
            return value;
        }

        /// <summary>
        /// Reads the sensor value as a string.
        /// </summary>
        /// <returns>The value as a string</returns>
        public string Sound_ReadAsString()
        {
            string s = "";
            switch (mode)
            {
                case (SensorMode)SoundMode.SoundDB:
                    s = Sound_Read().ToString();
                    break;
                case (SensorMode)SoundMode.SoundDBA:
                    s = Sound_Read().ToString();
                    break;
            }
            return s;
        }

        /// <summary>
        /// Read the sensor value
        /// </summary>
        public int Sound_Read()
        {
            int value = 0;
            switch (mode)
            {
                case (SensorMode)SoundMode.SoundDB:
                    value = GetReadPct();
                    break;
                case (SensorMode)SoundMode.SoundDBA:
                    value = GetReadPct();
                    break;
            }
            return value;
        }

        /// <summary>
        /// Gets or set the sound mode.
        /// </summary>
        /// <value>The mode.</value>
        public SoundMode Sound_Mode
        {
            get { return (SoundMode)this.mode; }
            set { SetMode((SensorMode)value); }
        }


        /// <summary>
        /// Gets or sets the ultrasonic mode.
        /// </summary>
        /// <value>The mode.</value>
        public UltrasonicMode Ultrasonic_Mode
        {
            get { return (UltrasonicMode)this.mode; }
            set { SetMode((SensorMode)value); }
        }

        ///// <summary>
        ///// Reads the sensor value as a string.
        ///// </summary>
        ///// <returns>The value as a string</returns>
        //public string Ultrasonic_ReadAsString()
        //{
        //    string s = "";
        //    switch (mode)
        //    {
        //        case (SensorMode)UltrasonicMode.Centimeter:
        //            s = Ultrasonic_Read().ToString() + " cm";
        //            break;
        //        case (SensorMode)UltrasonicMode.Inch:
        //            s = Ultrasonic_Read().ToString() + " inch";
        //            break;
        //        case (SensorMode)UltrasonicMode.Listen:
        //            s = Ultrasonic_Read().ToString();
        //            break;
        //    }
        //    return s;
        //}

        /// <summary>
        /// Read the sensor value. Result depends on the mode
        /// </summary>
        public float Ultrasonic_Read()
        {
            return ReadSi();
        }

        /// <summary>
        /// Gets or sets the temperature mode.
        /// </summary>
        /// <value>The mode.</value>
        public NXTTemperatureMode Temperature_Mode
        {
            get { return (NXTTemperatureMode)this.mode; }
            set { SetMode((SensorMode)value); }
        }

        /// <summary>
        /// Reads the sensor value as a string.
        /// </summary>
        /// <returns>The value as a string</returns>
        public string Temperature_ReadAsString()
        {
            string s = "";
            switch (mode)
            {
                case (SensorMode)NXTTemperatureMode.Celcius:
                    s = Temperature_ReadTemperature().ToString() + " C";
                    break;
                case (SensorMode)NXTTemperatureMode.Fahrenheit:
                    s = Temperature_ReadTemperature().ToString() + " F";
                    break;
            }
            return s;
        }

        /// <summary>
        /// Read the temperature.
        /// </summary>
        /// <returns>The temperature.</returns>
        public float Temperature_ReadTemperature()
        {
            return ReadSi();
        }

        /// <summary>
        /// Gets or sets the gyro mode.
        /// </summary>
        /// <value>The mode.</value>
        public GyroMode Gyro_Mode
        {
            get { return (GyroMode)this.mode; }
            set { SetMode((SensorMode)value); }
        }

        /// <summary>
        /// Reads the sensor value as a string.
        /// </summary>
        /// <returns>The value as a string</returns>
        public string Gyro_ReadAsString()
        {
            switch (mode)
            {
                case (SensorMode)GyroMode.Angle:
                    return Gyro_Read().ToString() + " degree";
                case (SensorMode)GyroMode.RotaSpeed:
                    return Gyro_Read().ToString() + " deg/sec";
                default:
                    return ReadSi().ToString();
            }
        }

        /// <summary>
        /// Read the sensor value. The result will depend on the mode
        /// </summary>
        public float Gyro_Read()
        {
            return ReadSi();
        }
    }
}

