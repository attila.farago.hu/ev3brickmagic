using EV3MonoBrickLib.Helper;
using MonoBrick.EV3;
using System;
using System.Collections.Generic;
using System.Threading;

namespace MonoBrick.EV3
{
    /// <summary>
    /// Class for creating a EV3 brick
    /// </summary>
    public class Brick<TSensor1, TSensor2, TSensor3, TSensor4>
        where TSensor1 : Sensor, new()
        where TSensor2 : Sensor, new()
        where TSensor3 : Sensor, new()
        where TSensor4 : Sensor, new()
    {
        #region wrapper for connection, filesystem, sensor and motor
        private Connection<Command, Reply> connection = null;
        private TSensor1 sensor1;
        private TSensor2 sensor2;
        private TSensor3 sensor3;
        private TSensor4 sensor4;
        private FilSystem fileSystem = new FilSystem();
        private Motor motorA = new Motor();
        private Motor motorB = new Motor();
        private Motor motorC = new Motor();
        private Motor motorD = new Motor();
        private Memory memory = new Memory();
        private MotorSync motorSync = new MotorSync();
        private Vehicle vehicle = new Vehicle(MotorPort.OutA, MotorPort.OutC);
        private Mailbox mailbox = new Mailbox();
        private Dictionary<SensorPort, Sensor> _Sensors;
        private Dictionary<MotorPort, Motor> _Motors;
        private void Init()
        {
            Sensor1 = new TSensor1();
            Sensor2 = new TSensor2();
            Sensor3 = new TSensor3();
            Sensor4 = new TSensor4();
            _Sensors = new Dictionary<SensorPort, Sensor>() {
                    { SensorPort.In1, Sensor1 },
                    { SensorPort.In2, Sensor2 },
                    { SensorPort.In3, Sensor3 },
                    { SensorPort.In4, Sensor4 },
                    };
            fileSystem.Connection = connection;
            motorA.Connection = connection;
            motorA.BitField = OutputBitfield.OutA;
            motorB.Connection = connection;
            motorB.BitField = OutputBitfield.OutB;
            motorC.Connection = connection;
            motorC.BitField = OutputBitfield.OutC;
            motorD.Connection = connection;
            motorD.BitField = OutputBitfield.OutD;
            motorSync.Connection = connection;
            motorSync.BitField = OutputBitfield.OutA | OutputBitfield.OutD;
            _Motors = new Dictionary<MotorPort, Motor>() {
                    { MotorA.Port, MotorA },
                    { MotorB.Port, MotorB },
                    { MotorC.Port, MotorC },
                    { MotorD.Port, MotorD },
                    };
            memory.Connection = connection;
            mailbox.Connection = connection;
            vehicle.Connection = connection;
        }

        /// <summary>
        /// Manipulate memory on the EV3
        /// </summary>
        /// <value>The memory.</value>
        /*public Memory Memory {
			get{return memory;}
		}
		*/

        /// <summary>
        /// Message system used to write and read data to/from the brick
        /// </summary>
        /// <value>
        /// The message system
        /// </value>
        public Mailbox Mailbox
        {
            get { return mailbox; }
        }

        /// <summary>
        /// Motor A
        /// </summary>
        /// <value>
        /// The motor connected to port A
        /// </value>
        public Motor MotorA
        {
            get { return motorA; }
        }

        /// <summary>
        /// Motor B
        /// </summary>
        /// <value>
        /// The motor connected to port B
        /// </value>
        public Motor MotorB
        {
            get { return motorB; }
        }

        /// <summary>
        /// Motor C
        /// </summary>
        /// <value>
        /// The motor connected to port C
        /// </value>
        public Motor MotorC
        {
            get { return motorC; }
        }

        /// <summary>
        /// Motor D
        /// </summary>
        /// <value>
        /// The motor connected to port D
        /// </value>
        public Motor MotorD
        {
            get { return motorD; }
        }

        public Dictionary<MotorPort, Motor> Motors
        {
            get
            {
                return _Motors;
            }
        }

        /// <summary>
        /// Synchronise two motors
        /// </summary>
        /// <value>The motor sync.</value>
        public MotorSync MotorSync
        {
            get { return motorSync; }
        }

        /// <summary>
        /// Use the brick as a vehicle
        /// </summary>
        /// <value>
        /// The vehicle
        /// </value>
        public Vehicle Vehicle
        {
            get { return vehicle; }
        }

        /// <summary>
        /// Gets or sets the sensor connected to port 1
        /// </summary>
        /// <value>
        /// The sensor connected to port 1
        /// </value>
        public TSensor1 Sensor1
        {
            get { return sensor1; }
            set
            {
                sensor1 = value;
                sensor1.Port = SensorPort.In1;
                sensor1.Connection = connection;
            }
        }

        /// <summary>
        /// Gets or sets the sensor connected to port 2
        /// </summary>
        /// <value>
        /// The sensor connected to port 2
        /// </value>
        public TSensor2 Sensor2
        {
            get { return sensor2; }
            set
            {
                sensor2 = value;
                sensor2.Port = SensorPort.In2;
                sensor2.Connection = connection;
            }
        }

        /// <summary>
        /// Gets or sets the sensor connected to port 3
        /// </summary>
        /// <value>
        /// The sensor connected to port 3
        /// </value>
        public TSensor3 Sensor3
        {
            get { return sensor3; }
            set
            {
                sensor3 = value;
                sensor3.Port = SensorPort.In3;
                sensor3.Connection = connection;
            }
        }

        /// <summary>
        /// Gets or sets the sensor connected to port 4
        /// </summary>
        /// <value>
        /// The sensor connected to port 4
        /// </value>
        public TSensor4 Sensor4
        {
            get { return sensor4; }
            set
            {
                sensor4 = value;
                sensor4.Port = SensorPort.In4;
                sensor4.Connection = connection;
            }
        }

        public Dictionary<SensorPort, Sensor> Sensors
        {
            get
            {
                return _Sensors;
            }
        }

        /// <summary>
        /// The file system 
        /// </summary>
        /// <value>
        /// The file system
        /// </value>
        public FilSystem FileSystem
        {
            get { return fileSystem; }
        }

        /// <summary>
        /// Gets the connection that the brick uses
        /// </summary>
        /// <value>
        /// The connection
        /// </value>
        public Connection<Command, Reply> Connection
        {
            get { return connection; }
        }


        /// <summary>
        /// Initializes a new instance of the Brick class.
        /// </summary>
        /// <param name='connection'>
        /// Connection to use
        /// </param>
        public Brick(Connection<Command, Reply> connection)
        {
            this.connection = connection;
            Init();
        }

        /// <summary>
        /// Initializes a new instance of the Brick class with bluetooth, usb or WiFi connection
        /// </summary>
        /// <param name='connection'>
        /// Can either be a serial port name for bluetooth connection or "usb" for usb connection and finally "wiFi" for WiFi connection
        /// </param>
        public Brick(string connection, string serialNumberUSBFilter = null)
        {
            switch (connection.ToLower())
            {
                case "usb":
                    this.connection = new USB<Command, Reply>(serialNumberUSBFilter);
                    break;
                case "wifi":
                    this.connection = new WiFiConnection<Command, Reply>(10000); //10 seconds timeout when connecting
                    break;
                case "loopback":
                    throw new NotImplementedException("Loopback connection has not been implemented for EV3");
                default:
                    this.connection = new Bluetooth<Command, Reply>(connection);
                    break;
            }
            Init();
        }

        /// <summary>
        /// Initializes a new instance of the Brick class with a tunnel connection
        /// </summary>
        /// <param name='ipAddress'>
        /// The IP address to use
        /// </param>
        /// <param name='port'>
        /// The port number to use
        /// </param>
        public Brick(string ipAddress, ushort port)
        {
            connection = new TunnelConnection<Command, Reply>(ipAddress, port);
            Init();
        }

        #endregion

        #region brick functions

        /// <summary>
        /// Starts a program on the brick
        /// </summary>
        /// <param name='name'>
        /// The of the program to start
        /// </param>
        public void StartProgram(string name)
        {
            name = FilSystem.AlignProjectFilename(name);
            var command = new Command(0, 8, 400, true);

            command.Append(ByteCodes.FILE);
            command.Append(FileSubCodes.LoadImage);
            command.Append((byte)ProgramSlots.User, ConstantParameterType.Value);
            command.Append(name, ConstantParameterType.Value);
            command.Append(0, VariableScope.Local);
            command.Append(4, VariableScope.Local);

            command.Append(ByteCodes.PROGRAM_START);
            command.Append((byte)ProgramSlots.User);
            command.Append(0, VariableScope.Local);
            command.Append(4, VariableScope.Local);

            command.Append(0, ParameterFormat.Short); //DEBUG: 0=NORMAL
            connection.Send(command);

            var brickReply = connection.Receive();
            Error.CheckForError(brickReply, command.SequenceNumber);
        }

        /// <summary>
        /// Stops all running programs
        /// </summary>
        public void StopProgram()
        {
            StopProgram(false);
        }

        /// <summary>
        /// Stops all running programs
        /// </summary>
        /// <param name='reply'>
        /// If set to <c>true</c> reply the brick will send a reply
        /// </param>
        public void StopProgram(bool reply)
        {
            var command = new Command(0, 0, 401, reply);
            command.Append(ByteCodes.PROGRAM_STOP);
            command.Append((byte)ProgramSlots.User, ConstantParameterType.Value);
            connection.Send(command);
            if (reply)
            {
                var brickReply = connection.Receive();
                Error.CheckForError(brickReply, 401);
            }
        }

        /// <summary>
        /// Control the led via led pattern
        /// </summary>
        /// <param name="ledpattern"></param>
        public void LEDControl(string ledpattern_str)
        {
            try
            {
                LedPattern ledpattern = (LedPattern)Enum.Parse(typeof(LedPattern), "LED_" + ledpattern_str);
                LEDControl((byte)ledpattern);
            }
            catch { }
        }

        /// <summary>
        /// Control the led via led pattern
        /// </summary>
        /// <param name="ledpattern"></param>
        public void LEDControl(byte ledpattern)
        {
            var command = new Command(0, 0, 500, false);

            command.Append(ByteCodes.UI_WRITE);
            command.Append((byte)UiWriteSubcode.LED, ConstantParameterType.Value);
            command.Append((byte)ledpattern, ConstantParameterType.Value);
            connection.Send(command);

            //var brickReply = connection.Receive();
        }

        /// <summary>
        /// Play a tone.
        /// </summary>
        /// <param name="volume">Volume.</param>
        /// <param name="frequency">Frequency of the tone</param>
        /// <param name="durationMs">Duration in ms.</param>
        public void PlayTone(byte volume, UInt16 frequency, UInt16 durationMs)
        {
            PlayTone(volume, frequency, durationMs, false);
        }
        public void PlayNote(byte volume, SoundHelper.Note note, UInt16 durationMs, bool bWaitSleep = false)
        {
            PlayTone(volume, note.Frequency, durationMs, false);
        }
        public void PlayNote(byte volume, double ev3note, UInt16 durationMs, bool bWaitSleep = false)
        {
            PlayTone(volume, SoundHelper._GetFrequecyEV3(ev3note), durationMs, false);
            if (bWaitSleep) Thread.Sleep(durationMs + 10);
        }
        public void PlayNote(byte volume, string ev3note, UInt16 durationMs, bool bWaitSleep = false)
        {
            PlayTone(volume, SoundHelper._GetFrequecyEV3(ev3note), durationMs, false);
            if (bWaitSleep) Thread.Sleep(durationMs + 10);
        }

        /// <summary>
        /// Play a tone.
        /// </summary>
        /// <param name="volume">Volume.</param>
        /// <param name="frequency">Frequency of the tone</param>
        /// <param name="durationMs">Duration in ms.</param>
        /// <param name="reply">If set to <c>true</c> reply from brick will be send</param>
        public void PlayTone(byte volume, UInt16 frequency, UInt16 durationMs, bool reply)
        {
            var command = new Command(0, 0, 123, reply);
            command.Append(ByteCodes.SOUND);
            command.Append(SoundSubCodes.Tone);
            command.Append(volume, ParameterFormat.Short);
            command.Append(frequency, ConstantParameterType.Value);
            command.Append(durationMs, ConstantParameterType.Value);
            connection.Send(command);
            if (reply)
            {
                var brickReply = connection.Receive();
                Error.CheckForError(brickReply, 123);
            }
        }

        /// <summary>
        /// Make the brick say beep
        /// </summary>
        /// <param name="volume">Volume of the beep</param>
        /// <param name="durationMs">Duration in ms.</param>
        public void Beep(byte volume, UInt16 durationMs)
        {
            Beep(volume, durationMs, false);
        }

        /// <summary>
        /// Make the brick say beep
        /// </summary>
        /// <param name="volume">Volume of the beep</param>
        /// <param name="durationMs">Duration in ms.</param>
        /// <param name="reply">If set to <c>true</c> reply from the brick will be send</param>
        public void Beep(byte volume, UInt16 durationMs, bool reply)
        {
            PlayTone(volume, 1000, durationMs, reply);
        }

        /// <summary>
        /// Play a sound file.
        /// </summary>
        /// <param name="name">Name the name of the file to play</param>
        /// <param name="volume">Volume.</param>
        /// <param name="repeat">If set to <c>true</c> the file will play in a loop</param>
        public void PlaySoundFile(string name, byte volume, bool repeat)
        {
            PlaySoundFile(name, volume, repeat, false);
        }

        /// <summary>
        /// Play a sound file.
        /// </summary>
        /// <param name="name">Name the name of the file to play</param>
        /// <param name="volume">Volume.</param>
        /// <param name="repeat">If set to <c>true</c> the file will play in a loop</param>
        /// <param name="reply">If set to <c>true</c> a reply from the brick will be send</param>
        public void PlaySoundFile(string name, byte volume, bool repeat, bool reply)
        {
            Command command = null;
            if (repeat)
            {
                command = new Command(0, 0, 200, reply);
                command.Append(ByteCodes.SOUND);
                command.Append(SoundSubCodes.Repeat);
                command.Append(volume, ConstantParameterType.Value);
                command.Append(name, ConstantParameterType.Value);
                command.Append(ByteCodes.SOUND_READY);//should this be here?
            }
            else
            {
                command = new Command(0, 0, 200, reply);
                command.Append(ByteCodes.SOUND);
                command.Append(SoundSubCodes.Play);
                command.Append(volume, ConstantParameterType.Value);
                command.Append(name, ConstantParameterType.Value);
                command.Append(ByteCodes.SOUND_READY);//should this be here?
            }
            connection.Send(command);
            if (reply)
            {
                var brickReply = connection.Receive();
                Error.CheckForError(brickReply, 200);
            }
        }

        /// <summary>
        /// Stops all sound playback.
        /// </summary>
        /// <param name="reply">If set to <c>true</c> reply from brick will be send</param>
        public void StopSoundPlayback(bool reply = false)
        {
            var command = new Command(0, 0, 123, reply);
            command.Append(ByteCodes.SOUND);
            command.Append(SoundSubCodes.Break);
            connection.Send(command);
            if (reply)
            {
                var brickReply = connection.Receive();
                Error.CheckForError(brickReply, 123);
            }
        }

        /// <summary>
        /// Gets the sensor types of all four sensors
        /// </summary>
        /// <returns>The sensor types.</returns>
        public (SensorType[], bool) GetSensorTypes()
        {
            const int numdevices = 4; //can be 32, motor ports start at 16
            var command = new Command(1 + numdevices, 0, 200, true);
            command.Append(ByteCodes.INPUT_DEVICE_LIST);
            command.Append((byte)numdevices, ParameterFormat.Short); // (Data8)LENGTH � Maximum number of devices types(Normally 32)
            command.Append((byte)0, VariableScope.Global);
            command.Append((byte)numdevices, VariableScope.Global);
            // RETURN:
            // (Data8) ARRAY � First element of data8 array of types (Normally 32) 
            // (Data8) CHANGED � Changed status
            var reply = Connection.SendAndReceive(command);
            var types = new SensorType[numdevices];
            for (int i = 0; i < numdevices; i++)
            {
                SensorType type;
                if (Enum.IsDefined(typeof(SensorType), (int)reply[3 + i]))
                {
                    type = (SensorType)reply[3 + i];
                }
                else
                {
                    type = SensorType.OtherUnknown;
                }
                types[i] = type;
            }
            bool changed = reply[3 + numdevices] != 0;
            return (types, changed);
        }

        public string GetFirwmareVersion()
        {
            const int maxlen = 0x10;
            var command = new Command(maxlen, 0, 201, true);
            command.Append(ByteCodes.UI_READ);
            command.Append((byte)UIReadSubCodes.GET_FW_VERS, ParameterFormat.Short);
            command.Append((byte)maxlen, ParameterFormat.Short);
            command.Append((byte)0, VariableScope.Global);
            var reply = Connection.SendAndReceive(command);
            string retval = System.Text.Encoding.ASCII.GetString(reply.Data, 3, maxlen);
            retval = retval.Substring(0, retval.IndexOf((char)0));
            return retval;
        }

        public string GetHardwareVersion()
        {
            const int maxlen = 0x10;
            var command = new Command(maxlen, 0, 202, true);
            command.Append(ByteCodes.UI_READ);
            command.Append((byte)UIReadSubCodes.GET_HW_VERS, ParameterFormat.Short);
            command.Append((byte)maxlen, ParameterFormat.Short);
            command.Append((byte)0, VariableScope.Global);
            var reply = Connection.SendAndReceive(command);
            string retval = System.Text.Encoding.ASCII.GetString(reply.Data, 3, maxlen);
            retval = retval.Substring(0, retval.IndexOf((char)0));
            return retval;
        }

        public string GetUIFWBuild()
        {
            const int maxlen = 0x10;
            var command = new Command(maxlen, 0, 203, true);
            command.Append(ByteCodes.UI_READ);
            command.Append((byte)UIReadSubCodes.GET_FW_BUILD, ParameterFormat.Short);
            command.Append((byte)maxlen, ParameterFormat.Short);
            command.Append((byte)0, VariableScope.Global);
            var reply = Connection.SendAndReceive(command);
            string retval = System.Text.Encoding.ASCII.GetString(reply.Data, 3, maxlen);
            retval = retval.Substring(0, retval.IndexOf((char)0));
            return retval;
        }

        public string GetID()
        {
            const int maxlen = 13 + 1;
            var command = new Command(maxlen, 0, 203, true);
            command.Append(ByteCodes.COM_GET);
            command.Append((byte)ComGetSubcode.GET_ID, ParameterFormat.Short);
            command.Append((byte)HWTYPE.HW_BT, ParameterFormat.Short);
            command.Append((byte)maxlen, ParameterFormat.Short);
            command.Append((byte)0, VariableScope.Global);
            var reply = Connection.SendAndReceive(command);
            Error.CheckForError(reply, command.SequenceNumber);
            string retval = System.Text.Encoding.ASCII.GetString(reply.Data, 3, maxlen);
            retval = retval.Substring(0, retval.IndexOf((char)0));
            return retval;
        }
        public (UInt32 total, UInt32 free) GetMemoryUsage()
        {
            var command = new Command(2 * 4, 0, 203, true);
            command.Append(ByteCodes.MEMORY_USAGE);
            command.Append((byte)0, VariableScope.Global);
            command.Append((byte)4, VariableScope.Global);
            var reply = Connection.SendAndReceive(command);
            Error.CheckForError(reply, command.SequenceNumber);
            UInt32 total = reply.GetUInt32(3);
            UInt32 free = reply.GetUInt32(3 + 4);
            return (total, free);
        }

        public (Single batteryVolts, Single batteryAmps, byte batteryPercentage) GetBatteryData()
        {
            UIReadSubCodes[] subcmds = {
                UIReadSubCodes.GET_VBATT,
                UIReadSubCodes.GET_IBATT
                };
            Dictionary<UIReadSubCodes, Single> values = new Dictionary<UIReadSubCodes, Single>();
            byte BATT_percentage;

            foreach (var subcmd in subcmds)
            {
                var command = new Command(0x04, 0, 204, true);
                command.Append(ByteCodes.UI_READ);
                command.Append((byte)subcmd); //OUT: Float (IEEE 754 single precision � 32 bits)
                command.Append((byte)0, VariableScope.Global);
                var reply = Connection.SendAndReceive(command);
                Error.CheckForError(reply, command.SequenceNumber);
                values[subcmd] = BitConverter.ToSingle(reply.Data, 3);
            }

            {
                var command = new Command(0x01, 0, 204, true);
                command.Append(ByteCodes.UI_READ);
                command.Append((byte)UIReadSubCodes.GET_LBATT); //OUT: (Data8) PCT � Battery level in percentage [0 - 100]
                command.Append((byte)0, VariableScope.Global);
                var reply = Connection.SendAndReceive(command);
                Error.CheckForError(reply, command.SequenceNumber);
                BATT_percentage = reply.GetByte(3);
            }

            return (values[UIReadSubCodes.GET_VBATT], values[UIReadSubCodes.GET_IBATT], BATT_percentage);
        }

        public string GetGetBrickName()
        {
            var command = new Command(0x20, 0, 205, true);
            command.Append(ByteCodes.COM_GET);
            command.Append((byte)ComGetSubcode.GET_BRICKNAME);
            command.Append((byte)0x20, ParameterFormat.Short); //Arguments: (Data8) LENGTH � Max length of returned string
            command.Append((byte)0, VariableScope.Global);
            var reply = Connection.SendAndReceive(command); //(Data8) NAME � First character in brick name (Zero terminated) 
            Error.CheckForError(reply, command.SequenceNumber);
            string retval = System.Text.Encoding.ASCII.GetString(reply.Data, 3, 16);
            retval = retval.Substring(0, retval.IndexOf((char)0));
            return retval;
        }

        /// <summary>
        /// Get the name of the program that is curently running
        /// </summary>
        /// <returns>
        /// The running program.
        /// </returns>
        public bool IsProgramRunning()
        {
            //Reply reply1, reply2, reply3;
            {
                var command = new Command(1, 0, 206, true);
                command.Append(ByteCodes.PROGRAM_INFO);
                command.Append((byte)ProgramInfoSubcodes.GET_PRGRESULT, ParameterFormat.Short);
                command.Append((byte)ProgramSlots.User, ConstantParameterType.Value);
                command.Append((byte)0, VariableScope.Global);
                var reply = Connection.SendAndReceive(command);
                Error.CheckForError(reply, command.SequenceNumber);
                //string retval = System.Text.Encoding.ASCII.GetString(reply.Data, 3, 16);
                // Program result [OK, BUSY, FAIL] //
                bool retval = reply[3] != (int)ResultCodes.OK;
                return retval;
            }
            //return retval;

            //{
            //    var command = new Command(1, 0, 206, true);
            //    command.Append(ByteCodes.Program_Info);
            //    command.Append((byte)ProgramInfoSubcodes.GET_STATUS, ParameterFormat.Short);
            //    command.Append((byte)ProgramSlots.User, ConstantParameterType.Value);
            //    command.Append((byte)0, VariableScope.Global);
            //    var reply = Connection.SendAndReceive(command);
            //    //string retval = System.Text.Encoding.ASCII.GetString(reply.Data, 3, 16);
            //    // Program result [OK, BUSY, FAIL]
            //    bool retval = reply[3] == (int)ObjectStatusCodes.STOPPED;
            //    reply2 = reply;
            //}
            ////return true;
            //{
            //    var command = new Command(120, 0, 206, true);
            //    command.Append(ByteCodes.Program_Info);
            //    command.Append((byte)ProgramInfoSubcodes.GET_PRGNAME, ParameterFormat.Short);
            //    command.Append((byte)ProgramSlots.User, ConstantParameterType.Value);
            //    command.Append((byte)0, VariableScope.Global);
            //    var reply = Connection.SendAndReceive(command);
            //    //string retval = System.Text.Encoding.ASCII.GetString(reply.Data, 3, 16);
            //    // Program result [OK, BUSY, FAIL]

            //    reply3 = reply;
            //    bool retval = reply[3] == (int)ObjectStatusCodes.STOPPED;
            //}
            //return true;
        }

        //public string GetProgramName()
        //{
        //    // does not work
        //    var command = new Command(Command.ShortValueMax, 0, 201, true);
        //    command.Append(ByteCodes.PROGRAM_INFO);
        //    command.Append((byte)ProgramInfoSubcodes.GET_PRGNAME, ParameterFormat.Short);
        //    command.Append((byte)ProgramSlots.User, ConstantParameterType.Value);
        //    command.Append((byte)Command.ShortValueMax, ParameterFormat.Short);
        //    command.Append((byte)0, VariableScope.Global);
        //    var reply = Connection.SendAndReceive(command);
        //    Error.CheckForError(reply, command.SequenceNumber);
        //    string retval = reply.GetString(3, Command.ShortValueMax);
        //    retval = retval.TrimEnd('\0', ' ');
        //    return retval;
        //}

        public Int32[] ReadGlobals(int offset = 0, int sizeof32 = -1)
        {
            if (sizeof32 <= 0) sizeof32 = 256;

            const int maxlenofchunk = 1023 / 4; //1024  1024-1 is max
            Int32[] data = new Int32[sizeof32];

            int dataleft = sizeof32;
            while (dataleft > 0)
            {
                int readlen = Math.Min(maxlenofchunk, dataleft);

                var command = new Command(readlen * 4, 0, 201, true);
                command.Append(ByteCodes.MEMORY_READ);
                command.Append((byte)ProgramSlots.User, ConstantParameterType.Value);
                command.Append(0, ConstantParameterType.Value); //Object id (zero means globals) PAR32 
                command.Append(offset, ConstantParameterType.Value); //Offset (start from) PAR32
                command.Append(readlen, ConstantParameterType.Value); //Size (length of dump) zero means all (max 1024) PAR32
                command.Append((byte)0, VariableScope.Global); //ARRAY - First element of DATA8 array to receive data 
                var reply = Connection.SendAndReceive(command);
                Error.CheckForError(reply, command.SequenceNumber);
                readlen = Math.Min((reply.Data.Length - 3) / 4, readlen);
                Buffer.BlockCopy(reply.Data, 3, data, sizeof32 - dataleft, readlen * 4);

                dataleft -= readlen;
                offset += readlen * 4;
            }
            return data;
        }

        /// <summary>
        /// PutCache - put file to most recent favourites list
        /// </summary>
        /// <param name="name"></param>
        /// <param name="reply"></param>
        public void PutCache(string name, bool reply = true)
        {
            name = FilSystem.AlignProjectFilename(name);
            var command = new Command(0, 8, 402, true);

            command.Append(ByteCodes.FILE);
            command.Append(FileSubCodes.PutCacheFile);
            command.Append(name, ConstantParameterType.Value);
            //command.Append(0, VariableScope.Local);
            //command.Append(4, VariableScope.Local);
            connection.Send(command);

            var brickReply = connection.Receive();
            Error.CheckForError(brickReply, command.SequenceNumber);
        }
        #endregion
    }
}

