using System;
using System.IO.Ports;
using System.Threading;

namespace MonoBrick
{
    /// <summary>
    /// Bluetooth connection for use on Windows and MAC
    /// </summary>
    public class Bluetooth<TBrickCommand, TBrickReply> : Connection<TBrickCommand, TBrickReply>
        where TBrickCommand : BrickCommand
        where TBrickReply : BrickReply, new()

    {
        private SerialPort comPort = null;
        string port;
        /// <summary>
        /// Initializes a new instance of the Bluetooth class.
        /// </summary>
        /// <param name="comport">he serial port to use</param>
        public Bluetooth(string comport)
        {
            port = comport;
            isConnected = false;
        }

        /// <summary>
        /// Send a command
        /// </summary>
        /// <param name='command'>
        /// The command to send
        /// </param>
        public override void Send(TBrickCommand command)
        {
            byte[] data = null;
            ushort length = (ushort)command.Length;
            data = new byte[length + 2];
            data[0] = (byte)(length & 0x00ff);
            data[1] = (byte)((length & 0xff00) >> 2);
            Array.Copy(command.Data, 0, data, 2, command.Length);
            CommandWasSend(command);
            try
            {
                comPort.Write(data, 0, data.Length);
            }
            catch (Exception e)
            {
                throw new ConnectionException(ConnectionError.WriteError, e);
            }

        }

        /// <summary>
        /// Receive a reply
        /// </summary>
        public override TBrickReply Receive()
        {
            byte[] data = new byte[2];
            byte[] payload;
            int expectedlength = 0;
            int replyLength = 0;
            try
            {
                expectedlength = 2;
                replyLength = comPort.Read(data, 0, 2);
                expectedlength = (ushort)(0x0000 | data[0] | (data[1] << 8)); //!! was << 2
                payload = new byte[expectedlength];
                replyLength = 0;
                //Thread.Sleep(10);
                replyLength = comPort.Read(payload, 0, expectedlength);
            }
            catch (TimeoutException tEx)
            {
                if (replyLength == 0)
                {
                    throw new ConnectionException(ConnectionError.NoReply, tEx);
                }
                else if (replyLength != expectedlength)
                {
                    //if (typeof(TBrickCommand) == typeof(NXT.Command))
                    //{
                    //    throw new NXT.BrickException(NXT.BrickError.WrongNumberOfBytes, tEx);
                    //}
                    //else
                    {
                        throw new EV3.BrickException(EV3.BrickError.WrongNumberOfBytes, tEx);
                    }
                }
                throw new ConnectionException(ConnectionError.ReadError, tEx);
            }
            catch (Exception e)
            {
                throw new ConnectionException(ConnectionError.ReadError, e);
            }
            TBrickReply reply = new TBrickReply();
            reply.SetData(payload);
            ReplyWasReceived(reply);
            return reply;
        }

        /// <summary>
        /// Open connection
        /// </summary>
        public override void Open()
        {
            try
            {
                void _openSerialPortOnThread(object obj)
                {
                    lock (obj)
                    {
                        try
                        {
                            SerialPort comPort = obj as SerialPort;
                            comPort.Open();
                        }
                        catch (Exception ex)
                        {
                            //Console.WriteLine(ex.Message);
                            comPort.Close();
                            comPort.Dispose(); //!!
                        }
                    }
                }

                comPort = new SerialPort(port); //, 115200); // 57600); // 115200);
                comPort.WriteTimeout = 5000;  // no send must take that long
                comPort.ReadTimeout = 5000;  // no reply must take that long

                //comPort.Open();
                Thread t = new Thread(new ParameterizedThreadStart(_openSerialPortOnThread));
                t.IsBackground = true;
                t.Start(comPort);
                //if (Monitor.TryEnter(obj, new TimeSpan(0, 0, 5)))
                //{
                //    //Thread.Sleep(5000); // wait and trink a tee for 5000 ms
                //    //wait for join
                //}
                bool threadJoined = t.Join(5000);
                if (!threadJoined)
                {
                    t.Interrupt(); t.Abort();
                    comPort = null;
                }
                GC.Collect();
                //t.ExecutionContext.Dispose();

                if (!comPort.IsOpen) throw new Exception("Could not open COM port");
            }
            catch (Exception e)
            {
                throw new ConnectionException(ConnectionError.OpenError, e);
            }
            isConnected = true;
            ConnectionWasOpened();
            //Thread.Sleep(1000);
        }

        /// <summary>
        /// Close connection
        /// </summary>
        public override void Close()
        {
            void _closeSerialPortOnThread()
            {
                try
                {
                    if (comPort != null && comPort.IsOpen)
                    {
                        while (!(comPort.BytesToRead == 0 && comPort.BytesToWrite == 0))
                        {
                            comPort.DiscardInBuffer();
                            comPort.DiscardOutBuffer();
                        }

                        comPort.Close();
                        //Thread.Sleep(100);
                        comPort.Dispose();
                        comPort = null;
                        //Console.WriteLine("COM Disposed");
                        GC.Collect();
                        //Thread.Sleep(100);
                    }
                    isConnected = false;
                }
                catch (Exception)
                {
                    comPort = null;
                }
            }
            Thread t = new Thread(new ThreadStart(_closeSerialPortOnThread));
            t.Start();
            t.IsBackground = true;
            t.Join(5000);
            ConnectionWasClosed();
        }

        /// <summary>
        /// Gets a list of available serial ports
        /// </summary>
        /// <returns>
        /// A list of available serial ports
        /// </returns>
        static public string[] GetPortNames()
        {
            return System.IO.Ports.SerialPort.GetPortNames();
        }

        public override void ResetOnError()
        {
            //!!
            comPort.DiscardInBuffer();
        }
    }
}

