﻿using MonoBrick.EV3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV3MonoBrickLib
{
    /// <summary>
    /// Main wrapper class around MonoBrick library
    /// </summary>
    static public class EV3
    {
        /// <summary>
        /// Lock object //TODO: ensure single thread access in a simpler way
        /// </summary>
        public static object lockBrick = new object();

        /// <summary>
        /// private member for brick instance
        /// </summary>
        static private Brick<Sensor, Sensor, Sensor, Sensor> mBrick = null;

        /// <summary>
        /// Connect to the brick
        /// </summary>
        /// <param name="port"></param>
        /// <returns></returns>
        static public bool Connect(string port, string serialNumberFilter = null)
        {
            if (IsConnected) Disconnect();

            mBrick = new Brick<Sensor, Sensor, Sensor, Sensor>(port, serialNumberFilter);
            mBrick.Connection.Open();

            return IsConnected;
        }

        /// <summary>
        /// Disconnect from the brick
        /// </summary>
        static public void Disconnect()
        {
            if (IsConnected) mBrick.Connection.Close();
            mBrick = null;
        }

        /// <summary>
        /// Is brick connected
        /// </summary>
        static public bool IsConnected
        {
            get
            {
                return mBrick != null && mBrick.Connection.IsConnected;
            }
        }

        /// <summary>
        /// Brick instance accessor
        /// </summary>
        static public Brick<Sensor, Sensor, Sensor, Sensor> Brick
        {
            get
            {
                return mBrick;
            }
        }
    }
}
