﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV3MonoBrickLib.Helper
{
    public class SoundHelper
    {
        public enum NoteKeys { C, CS, D, DS, E, F, FS, G, GS, A, AS, H };
        private static List<string> NoteKeysStrings = new List<string>(new[] { "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "H" });
        public static class Notes
        {
            public static readonly Note C5 = new Note() { NoteKey = NoteKeys.C, Octave = 5 };
            public static readonly Note CS5 = new Note() { NoteKey = NoteKeys.CS, Octave = 5 };
            public static readonly Note D5 = new Note() { NoteKey = NoteKeys.D, Octave = 5 };
            public static readonly Note DS5 = new Note() { NoteKey = NoteKeys.DS, Octave = 5 };
            public static readonly Note E5 = new Note() { NoteKey = NoteKeys.E, Octave = 5 };
            public static readonly Note F5 = new Note() { NoteKey = NoteKeys.F, Octave = 5 };
            public static readonly Note FS5 = new Note() { NoteKey = NoteKeys.FS, Octave = 5 };
            public static readonly Note G5 = new Note() { NoteKey = NoteKeys.G, Octave = 5 };
            public static readonly Note GS5 = new Note() { NoteKey = NoteKeys.GS, Octave = 5 };
            public static readonly Note A5 = new Note() { NoteKey = NoteKeys.A, Octave = 5 };
            public static readonly Note AS5 = new Note() { NoteKey = NoteKeys.AS, Octave = 5 };
            public static readonly Note H5 = new Note() { NoteKey = NoteKeys.H, Octave = 5 };
            public static readonly Note C6 = new Note() { NoteKey = NoteKeys.C, Octave = 6 };
            public static readonly Note CS6 = new Note() { NoteKey = NoteKeys.CS, Octave = 6 };
            public static readonly Note D6 = new Note() { NoteKey = NoteKeys.D, Octave = 6 };
            public static readonly Note DS6 = new Note() { NoteKey = NoteKeys.DS, Octave = 6 };
            public static readonly Note E6 = new Note() { NoteKey = NoteKeys.E, Octave = 6 };
            public static readonly Note F6 = new Note() { NoteKey = NoteKeys.F, Octave = 6 };
            public static readonly Note FS6 = new Note() { NoteKey = NoteKeys.FS, Octave = 6 };
            public static readonly Note G6 = new Note() { NoteKey = NoteKeys.G, Octave = 6 };
            public static readonly Note GS6 = new Note() { NoteKey = NoteKeys.GS, Octave = 6 };
            public static readonly Note A6 = new Note() { NoteKey = NoteKeys.A, Octave = 6 };
            public static readonly Note AS6 = new Note() { NoteKey = NoteKeys.AS, Octave = 6 };
            public static readonly Note H6 = new Note() { NoteKey = NoteKeys.H, Octave = 6 };
        }
        public class Note
        {
            public int Octave { get; set; } = 5;
            public NoteKeys NoteKey { get; set; } = NoteKeys.C;
            internal ushort Frequency
            {
                get
                {
                    return _GetFrequecyEV3(this);
                }
            }

            public override string ToString()
            {
                return $"{NoteKey}{Octave}";
            }
        }

        private static ushort _GetFrequecyPiano(int keyIndex)
        {
            // https://en.wikipedia.org/wiki/Piano_key_frequencies
            return (ushort)(Math.Pow(2, (keyIndex - 49) / 12.0) * 440);
        }
        public static ushort _GetFrequecyEV3(string noteString)
        {
            byte octave;
            int octaveOffset = 1;
            if (!byte.TryParse(noteString.Substring(noteString.Length - octaveOffset, octaveOffset), out octave)) { octaveOffset = 0; octave = 5; }
            int notesubidx = NoteKeysStrings.FindIndex(elem => elem == noteString.ToUpper().Substring(0, noteString.Length - octaveOffset));
            int noteIndex2 = 52 + notesubidx + (octave - 5) * 12;
            return _GetFrequecyPiano(noteIndex2);
        }
        public static Note _DoubleToNote(double noteIndex)
        {
            return new Note()
            {
                NoteKey = (NoteKeys)((noteIndex % 7 * 2) + (noteIndex % 7 >= 3 ? -1 : 0)), // note in octave
                Octave = ((int)noteIndex / 7) + 4 // octave 
            };
        }
        public static ushort _GetFrequecyEV3(double noteIndex)
        {
            // C 5 => 0
            // C=0, C#=0.5,D=1,D#=1.5,E=2,F=3,F#=3.5,G=4,G#=4.5,A=5,A#=5.5,H=6
            // keyoctave 52->63
            // C=0,C#=1,D=2,D#=3,E=4,F=5,F#=6,G=7,G#=8,A=9,A#=10,H=11
            var noteIndex2 =
                ((int)noteIndex / 7) * 12 + // octave 
                (noteIndex % 7 * 2) + (noteIndex % 7 >= 3 ? -1 : 0); // note in octave
            noteIndex2 += 52;
            return (ushort)_GetFrequecyPiano((int)noteIndex2);
        }
        public static ushort _GetFrequecyEV3(Note note)
        {
            var noteIndex2 = 52 + (note.Octave - 5) * 12 + (int)note.NoteKey;
            return _GetFrequecyPiano(noteIndex2);
        }

    }
}