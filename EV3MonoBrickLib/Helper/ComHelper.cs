﻿using EV3MonoBrickLib.Helper;
using InTheHand.Net.Sockets;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Management;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EV3MonoBrickLib
{
    /// <summary>
    /// COM device helper to list all known BT Serial ports
    /// </summary>
    public class ComHelper
    {
        /// <summary>
        /// Port info
        /// </summary>
        [DebuggerDisplay("{Port} {DeviceName}")]
        public struct PortInfo
        {
            public string Port { get; set; }
            public string DeviceName { get; set; }
            public string DeviceClass { get; set; }
            public DateTime? LastSeen { get; set; }
            public string DeviceSerial { get; set; }
        }


        #region Bluetooth detection
        /// <summary>
        /// internal - serial port PNPs
        /// </summary>
        /// <returns></returns>
        private static Dictionary<string, (string, string)> GetSerialPortsPNPs()
        {
            //var x1 = Win32DeviceMgmt.GetAllCOMPorts();

            Dictionary<string, (string, string)> retval = new Dictionary<string, (string, string)>();

            /*
            const string Win32_SerialPort = "Win32_SerialPort";
            SelectQuery q = new SelectQuery(Win32_SerialPort);
            //ManagementScope sc = new ManagementScope("root\\WMI");
            ManagementObjectSearcher s = new ManagementObjectSearcher(q);
            ManagementObjectCollection oc = s.Get();
            var oca = oc.OfType<ManagementObject>().ToArray();
            foreach (object cur in oca)
            {
                ManagementObject mo = (ManagementObject)cur;
                object id = mo.GetPropertyValue("DeviceID");
                object pnpId = mo.GetPropertyValue("PNPDeviceID");
                retval[id.ToString()] = pnpId.ToString();
                //Console.WriteLine("DeviceID:    {0} ", id);
                //Console.WriteLine("PNPDeviceID: {0} ", pnpId);
                //Console.WriteLine("");
                //Caption, Description, Name, "RS232 Serial Port", Status
                //https://docs.microsoft.com/en-us/windows/desktop/cimwin32prov/win32-serialport
            }//for
            */
            //{
            //    var serialports = System.IO.Ports.SerialPort.GetPortNames();
            //}
            
            {
                foreach (var dev in Bluetooth.GetBluetoothDevices())
                {
                    if (string.IsNullOrEmpty(dev.Port)) continue;
                    if (string.IsNullOrEmpty(dev.Name)) continue;
                    if (string.IsNullOrEmpty(dev.DeviceId)) continue;
                    retval[dev.Port] = (dev.DeviceId, dev.Name);
                }
            }

            //{
            //    // VERY SLOW using ManagementObjectSearcher - 2-3 sec
            //    DateTime dta1 = DateTime.Now;
            //    List<ManagementObject> collection;
            //    using (var searcher = new ManagementObjectSearcher("\\\\.\\root\\cimv2", @"Select * From Win32_PnPEntity WHERE PNPClass='Ports' AND DeviceID LIKE '%BTHENUM%' AND Caption LIKE '%COM%'"))
            //        collection = searcher.Get().Cast<ManagementObject>().ToList();
            //    var x = collection.Where(elem => elem.GetPropertyValue("DeviceID").ToString().Contains(@"BTHENUM")).ToList();
            //    foreach (ManagementObject mo in x)
            //    {
            //        //var xx = mo.Properties.Cast<PropertyData>().Select(elem => $"{elem?.Name}={elem?.Value}").ToArray(); Console.WriteLine(string.Join(", ",xx));

            //        //ManagementObject mo = (ManagementObject)cur;
            //        string deviceId = mo.GetPropertyValue("Caption").ToString();
            //        Console.WriteLine(deviceId);
            //        string pnpId = mo.GetPropertyValue("PNPDeviceID").ToString();

            //        // serial id
            //        string serialId = null;
            //        var match_s = Regex.Match(pnpId, ".*&(.*)_C00000000");
            //        if (match_s != null && match_s.Groups.Count >= 1) serialId = match_s.Groups[1].Value.ToLower();

            //        // com id
            //        Match match = new Regex(@"(COM\d+)").Match(deviceId);
            //        if (!match.Success) continue;
            //        string comport = match.Value;

            //        retval[comport] = (pnpId, serialId);
            //    }
            //    DateTime dta2 = DateTime.Now;
            //    Console.WriteLine($"a9 {dta2 - dta1}");
            //}
            return retval;
        }

        /// <summary>
        /// Retrieve all Serial (BT) ports
        /// </summary>
        /// <param name="bOnlyToyRobots"></param>
        /// <returns></returns>
        public static List<PortInfo> GetSerialPorts(bool bOnlyToyRobots = true)
        {
            BluetoothClient cli = new BluetoothClient();
            var toyDevices = cli.DiscoverDevices(255, true, true, false);
            if (bOnlyToyRobots) toyDevices = toyDevices.Where(dev => dev.ClassOfDevice.Device == InTheHand.Net.Bluetooth.DeviceClass.ToyRobot).ToArray();
            toyDevices = toyDevices.OrderByDescending(dev => dev.LastSeen).ToArray();

            //var firstToyDevice = toyDevices.First();
            var pnps = GetSerialPortsPNPs(); //TODO: this is slow, takes 2-3 sec
            //var deviceInfos = InTheHand.Devices.Enumeration.DeviceInformation.FindAll("*");
            //var selectedDevice = pnps.Where(elem => elem.Value.Contains(firstToyDevice.DeviceAddress.ToString())).First();

            var retval = pnps.Select(
                    pnp =>
                    {
                        var deviceMatching = toyDevices.FirstOrDefault(tdev => pnp.Value.Item1.Contains(tdev.DeviceAddress.ToString()));
                        var deviceName = deviceMatching?.DeviceName;
                        return new PortInfo()
                        {
                            Port = pnp.Key,
                            DeviceName = deviceName,
                            DeviceClass = deviceMatching?.ClassOfDevice.Device.ToString(),
                            DeviceSerial = pnp.Value.Item2,
                            LastSeen = deviceMatching?.LastSeen
                        };
                    }
                )
                .OrderByDescending(elem => elem.LastSeen)
                .ToList();

            return retval;
        }
        #endregion Bluetooth detection

        #region USB detection
        public static List<PortInfo> GetUSBPorts()
        {
            List<PortInfo> retval = new List<PortInfo>();

            string SEARCHSTR = @"USB\VID_0694&PID_0005\";
            using (var searcher = new ManagementObjectSearcher(@"Select * From Win32_PnPEntity WHERE DeviceID LIKE '%" + SEARCHSTR.Replace(@"\", @"\\") + "%'"))
            {
                using (ManagementObjectCollection collection = searcher.Get())
                {
                    //var usbev3s = collection.OfType<ManagementBaseObject>().Select(device => device.GetPropertyValue("DeviceID").ToString().Replace(SEARCHSTR, null));
                    //Console.WriteLine(string.Join("\n", usbev3s.ToArray()));

                    foreach (var device in collection)
                    {
                        string ev3serial = device.GetPropertyValue("DeviceID").ToString().Replace(SEARCHSTR, null).ToLower();
                        string ev3name = null;
                        using (var searcher1 = new ManagementObjectSearcher(@"Select Caption From Win32_PnPEntity WHERE DeviceID LIKE '%BTHENUM\\DEV_" + ev3serial + "%'"))
                        {
                            using (ManagementObjectCollection collection1 = searcher1.Get())
                            {
                                foreach (var device1 in collection1)
                                {
                                    ev3name = (string)device1.GetPropertyValue("Caption");
                                    break;
                                }
                            }
                        }
                        retval.Add(new PortInfo()
                        {
                            Port = "USB",
                            DeviceName = ev3name,
                            DeviceSerial = ev3serial
                        });
                    }
                }
            }

            return retval;
        }
        #endregion USB detection
    }
}
